
<?php require_once('header.php'); ?>

<!-- Top section -->
<section class="top-section">
    <div class="overlay"></div>
    <div class="bg-video">
        <video muted="muted" loop="loop" autoplay="autoplay">
            <source src="video/promo.mp4" type="video/mp4">
        </video>
    </div>
    <img class="message" src="img/top-section/message.png" alt="message">
    <div class="container">
        <div class="outer">
            <div class="date text-uppercase">
                <b>Бесплатный вебинар / </b>
            1 июня, четверг, в 13:00
            </div>
            <h1 class="page-title">
                Как настроить рекламу
                <br>
                в
                <img class="fb" src="img/fb.png" alt="fb">
                и
                <img class="in" src="img/in.png" alt="in">
            </h1>
            <div class="after-title">
            </div>
            <div class="share">
                Расскажите о мероприятии друзьям:
                <br>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                    <div class="vkontakte">Поделиться</div>
                    <div class="telegram">Отправить</div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center arrow wow slideInDown">
        <img src="img/icons/arrow-down.png" alt="arrow-down">
    </div>
</section>

<!-- You know section -->
<section class="you-know-section">
    <div class="container">
        <h2 class="section-title text-center wow fadeInDown">На вебинаре Вы узнаете</h2>
        <div class="row">
            <div class="col-md-6 hidden-sm hidden-xs text-center">
                <img class="wow fadeIn" src="img/you-know-section/thumb.png" alt="thumb">
            </div>
            <div class="col-md-6">
                <ul class="description">
                    <li class="wow fadeInDown">
                        <img src="img/you-know-section/marks/1.svg" alt="marks">
                        <span>Факты о профессии — SMM-таргетолог.</span>
                    </li>
                    <li class="wow fadeInDown">
                        <img src="img/you-know-section/marks/2.svg" alt="marks">
                        <span>Как работать в рекламном кабинете Facebook. Его инструменты и возможности.</span>
                    </li>
                    <li class="wow fadeInDown">
                        <img src="img/you-know-section/marks/3.svg" alt="marks">
                        <span>Что такое пиксель Facebook. Как правильно его использовать.</span>
                    </li>
                    <li class="wow fadeInDown">
                        <img src="img/you-know-section/marks/4.svg" alt="marks">
                        <span>Как создать эффективное рекламное объявление.</span>
                    </li>
                    <li class="wow fadeInDown">
                        <img src="img/you-know-section/marks/5.svg" alt="marks">
                        <span>Как оценить результативность рекламной кампании.</span>
                    </li>
                    <li class="wow fadeInDown">
                        <img src="img/you-know-section/marks/6.svg" alt="marks">
                        <span>3 инструмента для оптимизации и автоматизации рекламных кампаний.</span>
                    </li>
                </ul>
                <div class="fb-like" data-href="https://smmstudio.pro/webinar/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </div>
        </div>
    </div>
</section>

<!-- Who useful section -->
<section class="who-useful-section">
    <div class="text-center arrow wow slideInDown">
        <img src="img/icons/arrow-down-white.png" alt="arrow-down">
    </div>
    <div class="line"></div>
    <div class="container">
        <h2 class="section-title text-center wow fadeInDown">Кому будет полезен вебинар</h2>
        <div class="row">
            <div class="col-md-3 item text-center wow fadeInDown">
                <h4 class="title">Студенты</h4>
                <p class="description">
                    Хотите научиться настраивать таргетированную рекламу, но не знаете с чего начать?
                </p>
            </div>
            <div class="col-md-3 item text-center wow fadeInDown" data-wow-delay="0.25s">
                <h4 class="title">SMM-специалисты</h4>
                <p class="description">
                    Хотите зарабатывать больше, а работать меньше?
                </p>
            </div>
            <div class="col-md-3 item text-center wow fadeInDown" data-wow-delay="0.5s">
                <h4 class="title">Свитчеры</h4>
                <p class="description">
                    Вас не устраивает Ваша работа и зарплата? Хотите сменить вид деятельности?
                </p>
            </div>
            <div class="col-md-3 item text-center wow fadeInDown" data-wow-delay="0.75s">
                <h4 class="title">Предприниматели</h4>
                <p class="description">
                    Хотите больше клиентов в свой бизнес?
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Programs section -->
<section class="programs-section">
    <div class="container">
        <div class="outer clearfix wow fadeInUp">
            <div class="col-md-6 item bg">
                <img src="img/icons/docs.png" alt="docs">
                <h2 class="section-title">
                    Программа
                    <br class="hidden-sm">
                    вебинара
                </h2>                
            </div>
            <div class="col-md-6 item description">
                <ol>
                    <li>Понятие «SMM-таргетолог». Факты о профессии.</li>
                    <li>Доход SMM-таргетолога.</li>
                    <li>Как создать рекламный кабинет, для чего нужен бизнес-менеджер и как им пользоваться.</li>
                    <li>Виды рекламы.</li>
                    <li>Создание эффективных рекламных объявлений.</li>
                    <li>Для чего нужен пиксель Facebook и как правильно с ним работать.</li>
                    <li>Эффективные инструменты, которые облегчают настройку и оптимизацию рекламной кампании.</li>
                    <li>Оценка результативности рекламной кампании.</li>
                    <li>Как найти первых клиентов на биржах фриланса, если у Вас нет кейсов.</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Speaker section -->
<section class="speaker-section">
    <div class="container">
        <div class="row">
            <div class="thumb wow fadeInUp">
                <img src="img/speaker-section/thumb.png" alt="thumb">
            </div>
            <div class="col-md-6 col-md-offset-6 description wow fadeInUp">
                <h2 class="section-title">Спикер вебинара</h2>
                <span class="name v-center">Михаил Фёдоров</span>
                <a href="https://www.facebook.com/fedorov1991" target="_blank" class="social v-center">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a href="https://vk.com/fedorovzp" target="_blank" class="social v-center">
                    <i class="fa fa-vk" aria-hidden="true"></i>
                </a>
                <ul>
                    <li>CEO SMMSTUDIO, основатель холдинга IT NATION</li>
                    <li>Международный опыт в SMM — upwork.com (<a href="https://www.upwork.com/o/profiles/users/_~01a2951fd28545c8c0/">ссылка на профиль</a>)</li>
                    <li>Организатор профильных образовательных мероприятий по SMM</li>
                    <li>Спикер IT FORUM (2016, 2017)</li>
                    <li>Практикующий маркетолог</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Why section -->
<section class="why-section">
    <div class="figure t-l">
        <img src="img/why-section/1.svg" alt="figure">
    </div>
    <div class="figure t-c">
        <img src="img/why-section/2.svg" alt="figure">
    </div>
    <div class="figure b-r">
        <img src="img/why-section/3.svg" alt="figure">
    </div>
    <div class="container">
        <h2 class="section-title text-center wow fadeInDown">
            Почему нужно уметь настраивать рекламу
            <br class="hidden-sm">
            в Facebook и Instagram?
        </h2>
        <div class="row">
            <div class="col-md-offset-1 col-md-4 item wow fadeInDown">
                Умение настраивать рекламу поможет привлечь клиентов в Ваш бизнес и увеличит Ваш доход,
                как специалиста.
            </div>
            <div class="col-md-offset-2 col-md-4 item wow fadeInDown">
                Порог входа в профессию низкий, а оплата
                высокая.
            </div>
            <div class="clearfix"></div>
            <div class="col-md-offset-1 col-md-4 item wow fadeInDown">
                Навык настройки рекламы сейчас востребован на рынке вакансий.
            </div>
            <div class="col-md-offset-2 col-md-4 item wow fadeInDown">
                Основные бюджеты крупных международных
                брендов перетекают в сторону Facebook Ads.
            </div>
        </div>
    </div>
    <div class="text-center arrow wow fadeInDown">
        <img src="img/icons/arrow-down.png" alt="arrow-down">
    </div>
</section>

<!-- Top section -->
<section id="subscribe-section" class="top-section">
    <div class="overlay"></div>
    <div class="bg-video">
        <video muted="muted" loop="loop" autoplay="autoplay">
            <source src="video/promo.mp4" type="video/mp4">
        </video>
    </div>
    <img class="message" src="img/top-section/message.png" alt="message">
    <div class="container">
        <div class="outer">
            <div class="date text-uppercase">
                <b>Бесплатный вебинар / </b>
                1 июня, четверг, в 13:00
            </div>
            <h1 class="page-title">
                Как настроить рекламу
                <br>
                в
                <img class="fb" src="img/fb.png" alt="fb">
                и
                <img class="in" src="img/in.png" alt="in">
            </h1>
            <div class="after-title">
            </div>
            <div class="share">
                Расскажите о мероприятии друзьям:
                <br>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                    <div class="vkontakte">Поделиться</div>
                    <div class="telegram">Отправить</div>
                </div>
            </div>
        </div>
    </div>

</section>

<?php require_once('footer.php'); ?>
