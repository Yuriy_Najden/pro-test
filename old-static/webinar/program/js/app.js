$(document).ready(function () {

    new WOW().init();

    var listCountries = $.masksSort($.masksLoad('bower_components/inputmask-multi/data/phone-codes.json'), ['#'], /[0-9]|#/, 'mask');

    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: '[0-9]',
                    cardinality: 1
                }
            },
            showMaskOnHover: false,
            autoUnmask: false,
            clearMaskOnLostFocus: true
        },
        match: /[0-9]/,
        replace: '#',
        listKey: 'mask'
    };

    $('input[type=tel]').inputmasks($.extend(true, {}, maskOpts, {
        list: listCountries
    }));

    var subscribeForm = $('.subscribe-form');

    subscribeForm.find('.name').val(localStorage.getItem('userName'));
    subscribeForm.find('.email').val(localStorage.getItem('userEmail'));
    subscribeForm.find('.phone').val(localStorage.getItem('userPhone'));

    subscribeForm.on('submit', function () {
        var name = $(this).find('.name').val();
        var email = $(this).find('.email').val();
        var phone = $(this).find('.phone').val();

        $.ajax({
            method: 'POST',
            url: 'send.php',
            data: {
                name: name,
                email: email,
                phone: phone
            }
        });

        localStorage.setItem('userName', name);
        localStorage.setItem('userEmail', email);
        localStorage.setItem('userPhone', phone);
    });

});

$(document).on('click', '.scroll-to', function (e) {
    e.preventDefault();

    var href = $(this).attr('href');

    $('html, body').animate({
        scrollTop: $(href).offset().top
    }, 500);
});

$(window).on('load', function () {
    $('.loader').fadeOut('fast');
});