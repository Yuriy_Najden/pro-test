<form class="subscribe-form pull-right" action="//app.mailerlite.com/webforms/submit/t5f9q1" data-id="415617" data-code="t5f9q1" method="post">
    <h3 class="title text-center">
        Регистрируйтесь на вебинар —
        <small>это бесплатно и полезно.</small>
    </h3>
    <div class="form-group">
        <div class="label v-center">
            <label>Имя</label>
        </div>
        <div class="input v-center">
            <input type="text" name="fields[name]" class="name form-control" placeholder="Иван" required>
        </div>
    </div>
    <div class="form-group">
        <div class="label v-center">
            <label>Email</label>
        </div>
        <div class="input v-center">
            <input type="email" name="fields[email]" class="email form-control" placeholder="ivan@gmail.com" required>
        </div>
    </div>
    <div class="form-group">
        <div class="label v-center">
            <label>Телефон</label>
        </div>
        <div class="input v-center">
            <input type="tel" name="fields[phone]" class="phone form-control" placeholder="+380(93)312-72-02" value="+380" required>
        </div>
    </div>
    <input type="hidden" name="ml-submit" value="1" />
    <button type="submit" class="btn btn-success">Принять участие</button>
    <div class="count text-center">
        Количество свободных мест
        <span class="number">50</span>
    </div>
</form>