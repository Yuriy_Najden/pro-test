<form class="subscribe-form pull-right" action="//app.mailerlite.com/webforms/submit/r9e0c8" data-id="528449" data-code="r9e0c8" method="post">
    <h3 class="title text-center">
        Регистрируйтесь на вебинар —
        <small>это бесплатно и полезно.</small>
    </h3>
    <div class="form-group input-field has-feedback">
        <input id="input_name" name="fields[name]" placeholder="Имя" class="name form-input" type="text" data-minlength="2" data-required-error="Введите своё имя (не менее 2 символов)" required>

    </div>

    <div class="form-group input-field has-feedback">
        <input id="input_mail" name="fields[email]" placeholder="Email" class="email form-input" type="email" required>

    </div>

    <div class="form-group input-field has-feedback">
        <input id="input_phon"  name="fields[phone]"  class="phone input-form-mask form-input" type="tel" data-minlength="6" maxlength="13" placeholder="Телефон" required>

    </div>

    <input type="hidden" name="ml-submit" value="1" />
    <button type="submit" class="btn btn-success">Принять участие</button>

</form>