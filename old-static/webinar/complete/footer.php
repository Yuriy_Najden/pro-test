        <!-- Before footer -->
        <div class="before-footer">
            <div class="container">
                <div class="row">
                    <div class="content col-md-12">
                        <div class="footer-logo">
                            <div class="logo">
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsmmstudio.com.ua&tabs&width=280&height=220&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=1892932684290317" width="280" height="220" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true">
                                </iframe>
                            </div>
                        </div>
                        <div class="address">
                            <p><span>1</span> Украина, Запорожье, пр. Соборный, 192, офис 225</p>
                            <p><span>2</span> Уайтхолл, Вестминстер, Лондон SW1A 2EU, Великобритания</p>
                        </div>
                        <div class="phon-footer">
                            <p class="phon">
                                <img src="img/voip.svg" alt="" >
                            </p>
                            <ul>
                                <li><img src="img/ua_li.svg" alt=""><a href="tel:0800754754">0 800 754 754</a></li>
                                <li><img src="img/ru_li.svg" alt=""><a href="tel:78123095790">7 812 309 57 90</a></li>
                                <li><img src="img/kz_li.svg" alt=""><a href="tel:77172696230">77 1726 96 230</a></li>
                                <li><img src="img/gb_li.svg" alt=""><a href="tel:448000698473">44 800 069 84 73</a></li>
                                <li><p class="schedule">Пн-пт, 9:00 — 18:00 по Киеву</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="privacy pull-left">
                    <a href="#" data-toggle="modal" data-target="#privacy-modal">Политика конфиденциальности</a>
                </div>
                <div class="copyright pull-right">
                    © 2017 SMMSTUDIO
                </div>
            </div>
        </footer>

        <!-- Privacy modal -->
        <div id="privacy-modal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Политика конфиденциальности</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Мы уважаем информацию личного характера, касающуюся посетителей нашего сайта.
                            В настоящей Политике конфиденциальности разъясняются некоторые из мер, которые мы
                            предпринимаем для защиты Вашей частной жизни.
                        </p>
                        <h4>Какие контактные данные нам необходимы?</h4>
                        <p>
                            Мы регулярно выкладываем на сайте ценные информационные продукты (как платные, так и бесплатные).
                            Вы можете подписаться на любой из них  — в форму нужно ввести имя и email.
                        </p>
                        <p>
                            Иногда, в частности при регистрации на онлайн-мероприятия, мастер-классы или обучающие программы,
                            в форме может появиться обязательное поле для ввода номера мобильного телефона.
                        </p>
                        <h4>Зачем это нам нужно?</h4>
                        <p>
                            Когда Вы регистрируетесь на онлайн-мероприятия или приобретаете один из наших платных продуктов,
                            мы делаем информационную SMS-рассылку, с целью донесения важной информации о них, также, в
                            случае необходимости, связываемся с Вами по телефону для более детального информирования.
                        </p>
                        <p>
                            Как мы используем Вашу личную информацию (имя, email):
                        </p>
                        <ul>
                            <li>информируем Вас о выходе наших новых продуктов, которые могут быть Вам интересны;</li>
                            <li>предоставляем доступы к продуктам и материалам на email;</li>
                            <li>
                                осуществляем рассылку новостей и рекламной информации о продуктах, услугах, специальных
                                предложениях SMMSTUDIO / SMMSTUDIO.PRO, если Вы дали своё согласие, оставив нам свои контакты;
                            </li>
                            <li>
                                отправляем служебные сообщения (например, если Вы забыли пароль к продукту, или в других
                                случаях, когда Вам требуется помощь службы поддержки).
                            </li>
                        </ul>
                        <p>Важно: мы не рассылаем спам и не передаём Вашу личную информацию третьим лицам.</p>
                        <h4>Защита информации</h4>
                        <p>
                            Мы делаем всё возможное для того, чтобы обезопасить наш сайт и наших пользователей от
                            несанкционированных попыток доступа, изменения, раскрытия или уничтожения хранящихся у нас
                            данных. Для этого мы постоянно совершенствуем способы сбора, хранения и обработки данных для
                            противодействия несанкционированному доступу к нашим системам.
                        </p>
                        <h4>Можно ли отказаться от подписки?</h4>
                        <p>
                            Если наш сайт перестал приносить Вам пользу, или Вы не хотите больше получать наши рассылки,
                            перейдите по ссылке, указанной в нижней части любого нашего письма. После этого подписка будет
                            отменена.
                        </p>
                        <h4>Связь с нами</h4>
                        <p>
                            Если у Вас возникли какие-либо вопросы или предложения по поводу нашей Политики
                            конфиденциальности, пожалуйста, свяжитесь с нами по следующему адресу:
                            <a href="mailto:mail@smmstudio.pro">mail@smmstudio.pro</a>
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <!-- Javascript -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
        <script src="bower_components/wow/dist/wow.min.js"></script>
        <script src="bower_components/Likely/release/likely.js"></script>
        <script src="js/intlTelInput.min.js"></script>
        <script src="js/app.js"></script>

        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/11d37fcbb0a54bf55409c9f0d5b99d58_1.js" async></script>

        <!-- ManyChat -->
        <script src="//widget.manychat.com/1528621110733038.js" async="async">
        </script>




        </body>
</html>