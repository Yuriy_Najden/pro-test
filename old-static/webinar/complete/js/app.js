$(document).ready(function () {

    new WOW().init();


    var subscribeForm = $('.subscribe-form');

    subscribeForm.find('.name').val(localStorage.getItem('userName'));
    subscribeForm.find('.email').val(localStorage.getItem('userEmail'));
    subscribeForm.find('.phone').val(localStorage.getItem('userPhone'));

    subscribeForm.on('submit', function () {
        var name = $(this).find('.name').val();
        var email = $(this).find('.email').val();
        var phone = $(this).find('.phone').val();

        $.ajax({
            method: 'POST',
            url: 'send.php',
            data: {
                name: name,
                email: email,
                phone: phone
            }
        });

        localStorage.setItem('userName', name);
        localStorage.setItem('userEmail', email);
        localStorage.setItem('userPhone', phone);
    });

    $('header .phon').click(function(){
        $('.phone-list').slideDown(400);
    });

    $('.phone-list .close').click(function(){
        $('.phone-list').slideUp(400);
    });





});

$(document).on('click', '.scroll-to', function (e) {
    e.preventDefault();

    var href = $(this).attr('href');

    $('html, body').animate({
        scrollTop: $(href).offset().top
    }, 500);
});

$(window).on('load', function () {
    $('.loader').fadeOut('fast');
});



$('.input-form-mask').intlTelInput();

$('.input-form-mask').on('focus', function () {
    $('.input-form-mask').val($('.input-form-mask').intlTelInput("getSelectedCountryData").dialCode);
});

$('.input-form-mask').on('countrychange', function(e, countryData) {
    $(this).val(countryData.dialCode);
});

$.fn.forceNumbericOnly = function() {
    return this.each(function() {
        $(this).keydown(function(e) {
            var key = e.charCode || e.keyCode || 0;
            return (key == 8 || key == 9 || key == 46 || (key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 107 || key == 109 || key == 173 || key == 61);
        });
    });
};
$('.input-form-mask').forceNumbericOnly();


