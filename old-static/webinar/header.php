<?php
    session_start();

    $_SESSION['utm_source'] = !empty($_GET['utm_source']) ? $_GET['utm_source'] : null;
    $_SESSION['utm_medium'] = !empty($_GET['utm_medium']) ? $_GET['utm_medium'] : null;
    $_SESSION['utm_campaign'] = !empty($_GET['utm_campaign']) ? $_GET['utm_campaign'] : null;
    $_SESSION['utm_term'] = !empty($_GET['utm_term']) ? $_GET['utm_term'] : null;
    $_SESSION['utm_content'] = !empty($_GET['utm_content']) ? $_GET['utm_content'] : null;
?>
<!doctype html>
<html lang="ru">
    <head>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5RSJG8L');</script>
        <!-- End Google Tag Manager -->

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <meta property="og:title" content="Бесплатный вебинар «Как применить рекламу в Facebook и Instagram в своем бизнесе для увеличения продаж»" />
        <meta property="og:description" content="Зарегистрируйтесь прямо сейчас и получите PDF-инструкцию — «Как автоматизировать общение с клиентами с помощью чат-бота в Facebook»" />
        <meta property="og:type" content="video.movie" />
        <meta property="og:url" content="https://smmstudio.com.ua/webinar/" />
        <meta property="og:image" content="https://smmstudio.com.ua/webinar/img/og.png" />

        <title>Бесплатный вебинар «Как применить рекламу в Facebook и Instagram в своем бизнесе для увеличения продаж»</title>

        <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="img/favicon/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="img/favicon/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="img/favicon/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="img/favicon/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="img/favicon/manifest.json">
        <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="img/favicon/favicon.ico">

        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png">
        <meta name="msapplication-config" content="img/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">

        <!-- Styles -->
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="bower_components/Likely/release/likely.css">
        <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="bower_components/components-font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/intlTelInput.css">
        <link rel="stylesheet" href="css/style.css">

        <script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/11d37fcbb0a54bf55409c9f0d5b99d58_1.js" async></script>
        <!-- ManyChat -->
        <script src="//widget.manychat.com/1528621110733038.js" async="async">
        </script>


    </head>
    <body>

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQCL598" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        
        <!-- Loader -->
        <div class="loader"></div>

        <div id="fb-root"></div>

        <!-- Main header -->
        <header class="main-header">
            <div class="container">
                <div class="logo">
                    <img src="img/logo.svg" alt="logo">
                </div>
                <div class="contacts">
                    <div class="phon">
                        <p><img src="img/voip.svg" alt=""><span>0 800 754 754</span></p>
                        <a href="#" class="open-phone-list"><img src="img/arrowsdown.svg" alt=""></a>
                    </div>
                    <div class="phone-list">
                        <p class="phon"><img src="img/voip.svg" alt="" ></p>
                        <a href="#" class="close"><img src="img/arrows.svg" alt=""></a>
                        <ul>
                            <li><img src="img/ua.svg" alt=""><a href="tel:0800754754">0 800 754 754</a></li>
                            <li><img src="img/ru.svg" alt=""><a href="tel:78123095790">7 812 309 57 90</a></li>
                            <li><img src="img/kz.svg" alt=""><a href="tel:77172696230">77 1726 96 230</a></li>
                            <li><img src="img/gb.svg" alt=""><a href="tel:448000698473">44 800 069 84 73</a></li>
                            <li><p class="schedule">Пн-пт, 9:00 — 18:00 по Киеву</p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>