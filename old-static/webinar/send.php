<?php
session_start();

require_once('classes/AmoCrm.php');

function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name = clearData($_POST['name']);
$phone = clearData($_POST['phone']);
$email = clearData($_POST['email']);

if(!empty($name) && !empty($email) && !empty($phone)) {

    // Store in AmoCrm.
    $amoCrm = new AmoCrm();

    $lead = $amoCrm->storeLead(15990352);
    $leadId = (int)$lead['response']['leads']['add'][0]['id'];

    $contact = $amoCrm->storeContact($name, $email, $phone, $leadId);
}