<?php

class AmoCrm {
    private $subdomain = 'smmstudiopro';

    private function auth()
    {
        $user = array(
            'USER_LOGIN'    => 'admin@smmstudio.pro',
            'USER_HASH'     => '9f05bf766d20329c48a13c2910f00642'
        );

        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/auth.php?type=json';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($user));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);

        curl_close($curl);
    }

    public function __construct()
    {
        $this->auth();
    }

    /**
     * Store lead.
     *
     * @param $statusId
     * @return object
     */
    public function storeLead($statusId)
    {
        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/leads/set';

        $leads['request']['leads']['add'] = array(
            array(
                'name'          => 'Оформил заявку',
                'date_create'   => time(),
                'status_id'     => $statusId,
                'price'         => 50,
            )
        );

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($leads));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $response = json_decode($out, true);

        return $response;
    }

    /**
     * Store contact.
     *
     * @param $name
     * @param $email
     * @param $phone
     * @param $leadId
     */
    public function storeContact($name, $email, $phone, $leadId)
    {
        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/contacts/set';

        $contacts['request']['contacts']['add'] = array(
            array(
                'name'              => $name,
                'last_modified'     => time(),
                'linked_leads_id'   => array(
                    $leadId
                ),
                'custom_fields'     => array(
                    array(
                        // Phones
                        'id'        => 1660084,
                        'values'    => array(
                            array(
                                'value' => $phone,
                                'enum'  => 'OTHER'
                            )
                        )
                    ),
                    array(
                        // Emails
                        'id'        => 1660086,
                        'values'    => array(
                            array(
                                'value' => $email,
                                'enum'  => 'PRIV',
                            )
                        )
                    )
                )
            )
        );

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($contacts));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    }

    public function getContacts($email)
    {
        $link='https://'. $this->subdomain .'.amocrm.ru/private/api/v2/json/contacts/list?email=alex-test@gmail.com';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
    }
}