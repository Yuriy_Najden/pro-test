<?php require_once('header-paginal.php'); ?>

<!-- Status section -->
<section class="status-section">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="outer text-center success">
                    <img src="https://smmstudio.pro/webinar/instagram/img/icons/success.png" alt="success">
                    <h2 class="title">
                        Спасибо!
                        <br>
                        Ваш доступ к курсу уже у Вас на почте!
                    </h2>
                    <div class="after-title">Смотрите вебинар с друзьями. Расскажите им о нашем мероприятии:</div>
                    <div class="likely">
                        <div class="facebook">Поделиться</div>
                        <div class="vkontakte">Поделиться</div>
                        <div class="telegram">Отправить</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('footer.php'); ?>