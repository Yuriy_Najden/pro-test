<?php require_once('header-paginal.php'); ?>

<!-- Status section -->
<section class="status-section">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="outer text-center error">
                    <img src="https://smmstudio.pro/webinar/instagram/img/icons/error.png" alt="error">
                    <h2 class="title">Ошибка! Оплата не прошла!</h2>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('footer.php'); ?>
