        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="privacy pull-left">
                    <a href="#" data-toggle="modal" data-target="#privacy-modal">Политика конфиденциальности</a>
                </div>
                <div class="copyright pull-right">
                    © 2017 SMMSTUDIO.PRO
                </div>
            </div>
        </footer>

        <!-- Privacy modal -->
        <div id="privacy-modal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Политика конфиденциальности</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Мы уважаем информацию личного характера, касающуюся посетителей нашего сайта.
                            В настоящей Политике конфиденциальности разъясняются некоторые из мер, которые мы
                            предпринимаем для защиты Вашей частной жизни.
                        </p>
                        <h4>Какие контактные данные нам необходимы?</h4>
                        <p>
                            Мы регулярно выкладываем на сайте ценные информационные продукты (как платные, так и бесплатные).
                            Вы можете подписаться на любой из них  — в форму нужно ввести имя и email.
                        </p>
                        <p>
                            Иногда, в частности при регистрации на онлайн-мероприятия, мастер-классы или обучающие программы,
                            в форме может появиться обязательное поле для ввода номера мобильного телефона.
                        </p>
                        <h4>Зачем это нам нужно?</h4>
                        <p>
                            Когда Вы регистрируетесь на онлайн-мероприятия или приобретаете один из наших платных продуктов,
                            мы делаем информационную SMS-рассылку, с целью донесения важной информации о них, также, в
                            случае необходимости, связываемся с Вами по телефону для более детального информирования.
                        </p>
                        <p>
                            Как мы используем Вашу личную информацию (имя, email):
                        </p>
                        <ul>
                            <li>информируем Вас о выходе наших новых продуктов, которые могут быть Вам интересны;</li>
                            <li>предоставляем доступы к продуктам и материалам на email;</li>
                            <li>
                                осуществляем рассылку новостей и рекламной информации о продуктах, услугах, специальных
                                предложениях SMMSTUDIO / SMMSTUDIO.PRO, если Вы дали своё согласие, оставив нам свои контакты;
                            </li>
                            <li>
                                отправляем служебные сообщения (например, если Вы забыли пароль к продукту, или в других
                                случаях, когда Вам требуется помощь службы поддержки).
                            </li>
                        </ul>
                        <p>Важно: мы не рассылаем спам и не передаём Вашу личную информацию третьим лицам.</p>
                        <h4>Защита информации</h4>
                        <p>
                            Мы делаем всё возможное для того, чтобы обезопасить наш сайт и наших пользователей от
                            несанкционированных попыток доступа, изменения, раскрытия или уничтожения хранящихся у нас
                            данных. Для этого мы постоянно совершенствуем способы сбора, хранения и обработки данных для
                            противодействия несанкционированному доступу к нашим системам.
                        </p>
                        <h4>Можно ли отказаться от подписки?</h4>
                        <p>
                            Если наш сайт перестал приносить Вам пользу, или Вы не хотите больше получать наши рассылки,
                            перейдите по ссылке, указанной в нижней части любого нашего письма. После этого подписка будет
                            отменена.
                        </p>
                        <h4>Связь с нами</h4>
                        <p>
                            Если у Вас возникли какие-либо вопросы или предложения по поводу нашей Политики
                            конфиденциальности, пожалуйста, свяжитесь с нами по следующему адресу:
                            <a href="mailto:mail@smmstudio.pro">mail@smmstudio.pro</a>
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <!-- Javascript -->
        <script src="https://smmstudio.pro/webinar/instagram/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="https://smmstudio.pro/webinar/instagram/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="https://smmstudio.pro/webinar/instagram/bower_components/Likely/release/likely.js"></script>
        <script src="https://smmstudio.pro/webinar/instagram/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
        <script src="https://smmstudio.pro/webinar/instagram/bower_components/wow/dist/wow.min.js"></script>
        <script src="https://smmstudio.pro/webinar/instagram/js/app.js"></script>

    </body>
</html>