$(document).ready(function () {

    new WOW().init();

    $('input[type=tel]').mask('+38 (999) 999-99-99');

    var buyForm = $('#buy-form');

    buyForm.find('[name=name]').val(localStorage.getItem('userName'));
    buyForm.find('[name=email]').val(localStorage.getItem('userEmail'));
    buyForm.find('[name=phone]').val(localStorage.getItem('userPhone'));

});

$('#buy-form').on('submit', function () {
    var name = $(this).find('[name=name]').val();
    var email = $(this).find('[name=email]').val();
    var phone = $(this).find('[name=phone]').val();

    $.ajax({
        method: 'POST',
        url: '../send.php',
        data: {
            name: name,
            email: email,
            phone: phone
        }
    });

    localStorage.setItem('userName', name);
    localStorage.setItem('userEmail', email);
    localStorage.setItem('userPhone', phone);
});

$(window).on('load', function () {
    $('.loader').fadeOut('fast');
});