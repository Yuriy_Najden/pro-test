<?php require_once('header.php'); ?>

<!-- Top section -->
<section class="top-section">
    <div class="container">
        <div class="text-center">
            <div class="sticky">Поздравляем! Вы участник вебинара!</div>
            <br>
            <div class="sticky">
                <small>
                    Письмо с «Пошаговым планом развития» и подробной информацией придёт Вам на почту в течение 10 минут!
                </small>
            </div>
        </div>
        <div class="row outer">
            <div class="col-md-6">
                <span class="before-title text-uppercase">Онлайн-курс</span>
                <h1 class="page-title">
                    Как настроить
                    <br>
                    рекламу в
                    <img src="img/instagram.png" alt="instagram">
                </h1>
                <img src="img/top-section/thumb.png" class="hidden-sm hidden-md hidden-lg" alt="thumb">
                <div class="after-title">
                    и овладеть навыком, который сейчас наиболее востребованный в SMM,
                    <br class="hidden-xs hidden-sm hidden-md">
                    и за который готовы платить <nobr>от $15</nobr> в час!
                </div>
                <div class="likely">
                    <span class="title">Расскажите о курсе своим друзьям:</span>
                    <div class="facebook">Поделиться</div>
                    <div class="vkontakte">Поделиться</div>
                    <div class="telegram">Отправить</div>
                </div>
            </div>
            <div class="col-md-6 hidden-xs">
                <img src="img/top-section/thumb.png" alt="thumb">
            </div>
        </div>
        <div class="buy text-center">
            <div class="special">
                Спецпредложение для участников вебинара
                <span class="old-price">499 грн</span>
            </div>
            <a href="buy/" class="btn btn-default text-uppercase">Получить доступ <nobr>к курсу 49 грн</nobr></a>
            <div class="payments">
                <img src="img/payments/1.png" alt="payment">
                <img src="img/payments/2.png" alt="payment">
                <img src="img/payments/3.png" alt="payment">
                <img src="img/payments/4.png" alt="payment">
            </div>
        </div>
    </div>
    <div class="arrow text-center wow slideInDown">
        <img src="img/icons/arrow-down.png" alt="arrow-down">
    </div>
</section>

<!-- Questions section -->
<section class="questions-section">
    <div class="container">
        <div class="questions text-center">
            <img class="wow fadeInDown" src="img/questions-section/1.png" alt="question">
            <img class="wow fadeInDown" data-wow-delay="0.25s" src="img/questions-section/2.png" alt="question">
            <img class="wow fadeInDown" data-wow-delay="0.5s" src="img/questions-section/3.png" alt="question">
        </div>
        <div class="big-text text-center wow fadeInDown">
            Такие и подобные вопросы мы регулярно получаем от наших подписчиков, касательно рекламы в Instagram.
        </div>
        <div class="row wow fadeInDown">
            <div class="col-md-10 col-md-offset-1">
                <div class="before-list">Позвольте теперь мне задать Вам вопросы:</div>
                <p>
                    1.
                    <b>
                        Что Вы будете делать, если узнаете, что стоимость одного потенциального клиента в Вашем бизнесе (или
                        бизнесе Вашего клиента), например, $0,50
                    </b>
                    (т.е. Вы платите тогда, когда получаете контакты Вашего потенциального клиента)? А из 10 потенциальных
                    клиентов 3 становятся реальными клиентами и покупают у Вас?
                </p>
            </div>
        </div>
        <img class="company wow fadeInDown" src="img/questions-section/company.png" alt="company">
        <div class="prices text-center">
            <span class="number wow fadeInDown">
                14,46
                <small>руб</small>
            </span>
            <span class="delimiter"></span>
            <span class="number wow fadeInDown">$0,23</span>
            <span class="delimiter wow fadeInDown"></span>
            <span class="number wow fadeInDown">
                6,17
                <small>грн</small>
            </span>
        </div>
        <div class="after-prices text-center wow fadeInDown">стоимость одной заявки</div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>
                    2. Что Вы будете делать со своим навыком — умением настраивать рекламу в Instagram с оплатой только
                    за потенциальных клиентов, имея реальные кейсы в подтверждение, сделанные своими руками, зная,
                    что на международной бирже фриланса Upwork
                    <b>за Ваш навык обычно платят от $15?</b>
                </p>
            </div>
        </div>
        <div class="upwork text-center wow fadeInDown">
            <img src="img/questions-section/upwork.png" alt="upwork">
        </div>
    </div>
</section>

<!-- Causes section -->
<section class="causes-section">
    <div class="container">
        <div class="text-center after-title text-uppercase wow fadeInDown">4 причины почему мы создали видеокурс</div>
        <h2 class="text-center section-title wow fadeInDown">
            «Как настроить рекламу в Instagram c оплатой
            <br>
            только за заявки от потенциальных клиентов»
        </h2>
        <div class="row">
            <div class="col-md-6 item wow fadeInDown">
                <span class="icon orange">
                    <img src="img/icons/check.svg" alt="check">
                </span>
                <span class="text">
                    Только за 2016 год мы <b>успешно настроили рекламу в Instagram для наших клиентов более, чем на $200 000</b> 
                    и теперь хотим поделиться с Вами нашим опытом.
                   
                </span>
            </div>
            <div class="col-md-6 item wow fadeInDown" data-wow-delay="0.25s">
                <span class="icon pink">
                    <img src="img/icons/check.svg" alt="check">
                </span>
                <span class="text">
                    <b>Рынок SMM в Украине сильно отстает от США.</b> То, что сейчас работает на Западе, будет работать
                    у нас только через несколько лет. Чтобы зарабатывать в долларах необходимо знать и использовать
                    тренды в рекламе.
                </span>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 item wow fadeInDown">
                <span class="icon purple">
                    <img src="img/icons/check.svg" alt="check">
                </span>
                <span class="text">
                    <b>1 из 10 SMM-специалистов пробовал</b> настроить рекламу в Instagram с оплатой только за
                    потенциальных клиентов и сделал это успешно.
                </span>
            </div>
            <div class="col-md-6 item wow fadeInDown" data-wow-delay="0.25s">
                <span class="icon blue">
                    <img src="img/icons/check.svg" alt="check">
                </span>
                <span class="text">
                    Часто нам приходится отказывать клиентам из-за загруженности, и мы готовы способствовать
                    профессиональному становлению будущих партнеров.
                </span>
            </div>
        </div>
    </div>
</section>

<!-- Program section -->
<section class="program-section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 wow fadeInUp">
                <div class="outer">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#program" aria-controls="program" role="tab" data-toggle="tab">
                                Программа
                                <br class="visible-xs">
                                курса
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#result" aria-controls="result" role="tab" data-toggle="tab">
                                Результат после
                                <br class="visible-xs">
                                обучения
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="program">
                            <div class="row">
                                <div class="col-md-8">
                                    <ol>
                                        <li>Создание и привязка Instagram-аккаунта к Facebook Ads.</li>
                                        <li>Создание и настройка пикселя конверсии и индивидуально настроенных аудиторий, система аналитики.</li>
                                        <li>Создание и настройка рекламных объявлений.</li>
                                        <li>Аналитика рекламных объявлений и масштабирование.</li>
                                        <li>Бонусный модуль «Скрытые возможности рекламы в Instagram».</li>
                                    </ol>
                                    <div class="description">
                                        <div class="v-center">
                                            <img src="img/icons/play-button.png" alt="play-button">
                                            <span>
                                                <small>Длительность курса</small>
                                                <br>
                                                67 мин
                                            </span>
                                        </div>
                                        <div class="delimiter"></div>
                                        <div class="v-center">
                                            <img src="img/icons/clock.png" alt="clock">
                                            <span>
                                                <small>Кол-во уроков</small>
                                                <br>
                                                5
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 hidden-sm hidden-xs text-center">
                                    <img src="img/icons/docs.png" alt="docs">
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="result">
                            <div class="row">
                                <div class="col-md-8">
                                    <ol>
                                        <li>Вы умеете создавать и брендировать продающие профили в Instagram.</li>
                                        <li>Знаете как создать рекламный кабинет и интегрировать аккаунт Instagram в Facebook Ads.</li>
                                        <li>Хорошо ориентируетесь и пользуетесь инструментами Ads manager.</li>
                                        <li>Умеете правильно настраивать и работать с пикселем конверсии.</li>
                                        <li>Хорошо владеете сервисами аналитики Яндекс.Метрика и Google Analytics, умеете ставить счётчики на нужный сайт, правильно настраивать цели отслеживания, знаете как анализировать результаты рекламной кампании.</li>
                                        <li>Знаете как создавать продающие рекламные объявления в Instagram.</li>
                                        <li>Вам не составляет труда настроить таргетированную рекламу в Instagram.</li>
                                    </ol>
                                </div>
                                <div class="col-md-4 hidden-sm hidden-xs text-center">
                                    <img src="img/icons/medal.png" alt="medal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Guaranty section -->
<section class="guaranty-section">
    <div class="container">
        <div class="text-center wow fadeInDown">
            <img class="v-center icon" src="img/icons/guaranty.png" alt="guaranty">
            <div class="v-center description text-left">
                <h4 class="title text-uppercase">ГАРАНТИЯ ВОЗВРАТА СРЕДСТВ</h4>
                Действует гарантия возврата. Если курс Вам не подойдёт —
                <br class="hidden-sm">
                я без лишних вопросов <b>верну Вам 49 грн!</b>
            </div>
        </div>
    </div>
    <div class="arrow text-center wow fadeInDown">
        <img src="img/icons/arrow-down.png" alt="arrow-down">
    </div>
</section>

<!-- Top section -->
<section class="top-section last">
    <div class="container">
        <div class="row outer">
            <div class="col-md-6">
                <span class="before-title text-uppercase">Онлайн-курс</span>
                <h2 class="page-title">
                    Как настроить
                    <br>
                    рекламу в
                    <img src="img/instagram.png" alt="instagram">
                </h2>
                <img src="img/top-section/thumb.png" class="hidden-sm hidden-md hidden-lg" alt="thumb">
                <div class="after-title">
                    и овладеть навыком, который сейчас наиболее востребованный в SMM,
                    <br class="hidden-sm">
                    и за который готовы платить от $15 в час!
                </div>
                <div class="likely">
                    <span class="title">Расскажите о курсе своим друзьям:</span>
                    <div class="facebook">Поделиться</div>
                    <div class="vkontakte">Поделиться</div>
                    <div class="telegram">Отправить</div>
                </div>
            </div>
            <div class="col-md-6 hidden-xs">
                <img src="img/top-section/thumb.png" alt="thumb">
            </div>
        </div>
        <div class="buy text-center">
            <div class="special">
                Спецпредложение для участников вебинара
                <span class="old-price">499 грн</span>
            </div>
            <a href="buy/" class="btn btn-default text-uppercase">Получить доступ к курсу 49 грн</a>
            <div class="payments">
                <img src="img/payments/1.png" alt="payment">
                <img src="img/payments/2.png" alt="payment">
                <img src="img/payments/3.png" alt="payment">
                <img src="img/payments/4.png" alt="payment">
            </div>
        </div>
    </div>
</section>

<?php require_once('footer.php'); ?>