<?php require_once('header-paginal.php'); ?>

<!-- Buy section -->
<section class="buy-section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="outer clearfix">
                    <div class="item">
                        <h3 class="form-title text-center">
                            Стать участником
                            <br>
                            <small>и получить мгновенный доступ к курсу</small>
                        </h3>
                        <form id="buy-form" class="subscribe-form" name="order_form_251425" action="http://smmstudiopro.e-autopay.com/ordering/do_order.php" method="post">
                            <div class="form-group">
                                <div class="label v-center">
                                    <label>Имя</label>
                                </div>
                                <div class="input v-center">
                                    <input type="text" name="name" class="form-control" placeholder="Иван" maxlength="50" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label v-center">
                                    <label>Email</label>
                                </div>
                                <div class="input v-center">
                                    <input type="email" name="email" class="form-control" placeholder="ivan@gmail.com" maxlength="50" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label v-center">
                                    <label>Телефон</label>
                                </div>
                                <div class="input v-center">
                                    <input type="tel" name="phone" class="form-control" placeholder="+38 (093) 312-72-02" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="pay_mode" required>
                                    <option value="14">Банковскими картами через Приват24</option>
                                    <option value="15">Яндекс.Деньги</option>
                                    <option value="8">WebMoney гривны (WMU)</option>
                                    <option value="0">WebMoney доллары (WMZ)</option>
                                </select>
                            </div>
                            <input type="hidden" name="order_page" value="http://smmstudio.pro/instagram-course">
                            <input type="hidden" name="tovar_id" value="303474">
                            <button type="submit" class="btn btn-primary text-uppercase">Принять участие</button>
                        </form>
                    </div>
                    <div class="triangle-right"></div>
                    <div class="item">
                        <div class="text-center">
                            <img src="https://smmstudio.pro/webinar/instagram/img/instagram-logo.png" alt="instagram-logo">
                        </div>
                        <span class="before-title text-uppercase">Онлайн-курс</span>
                        <h1 class="page-title">
                            Как настроить
                            <br>
                            рекламу в
                            <img src="https://smmstudio.pro/webinar/instagram/img/instagram.png" alt="instagram">
                        </h1>
                        <div class="text-center prices">
                            <span class="old v-center">
                                499
                                <sup>грн</sup>
                            </span>
                            <span class="current v-center">
                                49
                                <sup>грн</sup>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center payments">
            <img src="https://smmstudio.pro/webinar/instagram/img/payments-color/1.png" alt="payment">
            <img src="https://smmstudio.pro/webinar/instagram/img/payments-color/2.png" alt="payment">
            <img src="https://smmstudio.pro/webinar/instagram/img/payments-color/3.png" alt="payment">
            <img src="https://smmstudio.pro/webinar/instagram/img/payments-color/4.png" alt="payment">
        </div>
    </div>
</section>

<?php require_once('footer.php'); ?>
