<!doctype html>
<html lang="ru">
    <head>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MQCL598');</script>
        <!-- End Google Tag Manager -->

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <meta property="og:title" content="Как настроить рекламу в Facebook и Instagram" />
        <meta property="og:description" content="Зарегистрируйтесь прямо сейчас и расскажите друзьям о мероприятии, чтобы получить в подарок сборку материаллов «Пошаговый план развития SMM-специалиста на 6 месяцев»" />
        <meta property="og:type" content="video.movie" />
        <meta property="og:url" content="https://smmstudio.pro/webinar/" />
        <meta property="og:image" content="https://smmstudio.pro/webinar/img/opengraph.png" />

        <title>Как настроить рекламу в Instagram</title>

        <link rel="apple-touch-icon" sizes="57x57" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="https://smmstudio.pro/webinar/instagram/img/favicon/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="https://smmstudio.pro/webinar/instagram/img/favicon/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="https://smmstudio.pro/webinar/instagram/img/favicon/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="https://smmstudio.pro/webinar/instagram/img/favicon/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="https://smmstudio.pro/webinar/instagram/img/favicon/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="https://smmstudio.pro/webinar/instagram/img/favicon/manifest.json">
        <link rel="mask-icon" href="https://smmstudio.pro/webinar/instagram/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="https://smmstudio.pro/webinar/instagram/img/favicon/favicon.ico">

        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="https://smmstudio.pro/webinar/instagram/img/favicon/mstile-144x144.png">
        <meta name="msapplication-config" content="https://smmstudio.pro/webinar/instagram/img/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">

        <!-- Styles -->
        <link rel="stylesheet" href="https://smmstudio.pro/webinar/instagram/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://smmstudio.pro/webinar/instagram/bower_components/Likely/release/likely.css">
        <link rel="stylesheet" href="https://smmstudio.pro/webinar/instagram/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="https://smmstudio.pro/webinar/instagram/css/style.css">

    </head>
    <body>

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQCL598" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <!-- Loader -->
        <div class="loader"></div>

        <!-- Main header -->
        <header class="main-header alt">
            <div class="container">
                <div class="logo pull-left">
                    <a href="/">
                        <img src="https://smmstudio.pro/webinar/instagram/img/logo.svg" alt="logo">
                    </a>
                </div>
                <div class="phone pull-right">
                    <a href="tel:0800754754" class="v-center">0 800 754 754</a>
                    <span class="v-center">
                        Звонки по всей
                        <br>
                        Украине бесплатно
                    </span>
                </div>
            </div>
        </header>