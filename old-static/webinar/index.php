
<?php require_once('header.php'); ?>

<!-- Top section -->
<section class="top-section">
    <div class="overlay"></div>
    <div class="bg-video">
        <video muted="muted" loop="loop" autoplay="autoplay">
            <source src="video/promo.mp4" type="video/mp4">
        </video>
    </div>
    <img class="message" src="img/top-section/message.png" alt="message">
    <div class="container">
        <div class="outer">
            <div class="date text-uppercase">
                <b>Бесплатный вебинар / </b>
                22 августа, вторник, 13:00
            </div>
            <h1 class="page-title">
                Как применить рекламу в Facebook
                и Instagram в своем бизнесе
                для увеличения продаж
            </h1>
            <div class="after-title">
                Зарегистрируйтесь прямо сейчас и получите PDF-инструкцию —
                «Как автоматизировать общение с клиентами с помощью чат-бота в Facebook»
            </div>
            <div class="share">
                Расскажите о мероприятии друзьям:
                <br>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                    <div class="vkontakte">Поделиться</div>
                    <div class="telegram">Отправить</div>
                </div>
            </div>
        </div>

        <?php include('partials/form.php'); ?>

    </div>
</section>

<!-- You know section -->
<section class="you-know-section">
    <div class="container">
        <h2 class="section-title text-center wow fadeInDown">На вебинаре Вы узнаете</h2>
        <div class="row">
            <div class="col-md-6 hidden-sm hidden-xs text-center">
                <img class="wow fadeIn" src="img/you-know-section/thumb.png" alt="thumb">
            </div>
            <div class="col-md-6">
                <ul class="description">
                    <li class="wow fadeInDown">

                        Возможно ли в Вашей сфере ежедневно получать заявки от клиентов?
                    </li>
                    <li class="wow fadeInDown">

                        Чем отличается реклама в Facebook и Instagram от поисковой и медийной рекламы в Google.
                    </li>
                    <li class="wow fadeInDown">

                        Какие форматы рекламы эффективны в разных нишах бизнеса.
                    </li>
                    <li class="wow fadeInDown">

                        Сколько должно стоить обращение от потенциального клиента из Facebook/Instagram в вашем бизнесе.

                    </li>
                    <li class="wow fadeInDown">

                        Зачем необходимо координировать ход рекламной кампании в Facebook и Instagram с работой отдела продаж.

                    </li>
                    <li class="wow fadeInDown">

                        Что необходимо сделать внутри бизнеса, чтобы SMM-специалисты работали с гарантией результата.
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Who useful section -->
<section class="who-useful-section">
    <div class="line"></div>
    <div class="container">
        <h2 class="section-title text-center wow fadeInDown">Кому будет полезен вебинар</h2>
        <div class="row">
            <div class="col-md-4 item text-center wow fadeInDown">
                <h4 class="title">Владельцы</h4>
                <p class="description">
                    Вы узнаете как увеличить количество входящих заявок в отдел продаж
                </p>
            </div>
            <div class="col-md-4 item text-center wow fadeInDown" data-wow-delay="0.25s">
                <h4 class="title">Маркетологи</h4>
                <p class="description">
                    Вы научитесь правильно прогнозировать и считать эффективность рекламы
                </p>
            </div>
            <div class="col-md-4 item text-center wow fadeInDown" data-wow-delay="0.5s">
                <h4 class="title">Эффективные сотрудники</h4>
                <p class="description">
                    Вы сможете подсказать владельцу или маркетологу, как зарабатывать больше
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Programs section -->
<section class="programs-section">
    <div class="container">
        <div class="outer clearfix wow fadeInUp">
            <div class="col-md-6 item bg">
                <img src="img/icons/docs.png" alt="docs">
                <h2 class="section-title">
                    Программа
                    <br class="hidden-sm">
                    вебинара
                </h2>
                <a href="#subscribe-section" class="btn scroll-to">Принять участие</a>
            </div>
            <div class="col-md-6 item description">
                <ol>
                    <li>Роль рекламы в Facebook и  Instagram в общей
                        маркетинговой стратегии.</li>
                    <li>Виды и форматы рекламы в Facebook и Instagram.</li>
                    <li>Тестирование рекламных объявлений, аудиторий, предложений.</li>
                    <li>Определение стоимости заявки, масштабирование результатов.</li>
                    <li>Координация между трафик-специалистами
                        и отделом продаж.
                    </li>
                    <li>Анализ и оптимизация рекламы.</li>
                    <li>Разбор последних кейсов SMMSTUDIO.</li>
                    <li>Ответы на вопросы.</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Speaker section -->
<section class="speaker-section">
    <div class="container">
        <div class="row">
            <div class="thumb wow fadeInUp">
                <img src="img/speaker-section/thumb.png" alt="thumb">
            </div>
            <div class="col-md-6 col-md-offset-6 description wow fadeInUp">
                <h2 class="section-title">Спикер вебинара</h2>
                <span class="name v-center">Михаил Фёдоров</span>
                <a href="https://www.facebook.com/fedorov1991" target="_blank" class="social v-center">
                </a>
                <ul>
                    <li>Основатель, маркетинг-директор SMMSTUDIO</li>

                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Why section -->
<section class="why-section">

    <div class="container">
        <h2 class="section-title text-center wow fadeInDown">
            Почему стоит разбираться в основах рекламы <br> в Facebook и Instagram
        </h2>
        <div class="row">
            <div class="col-md-offset-1 col-md-4 item wow fadeInDown">
                Это наиболее популярные социальные сети в Мире. Соответственно самый
                крупный рынок для Вашего бизнеса.
            </div>
            <div class="col-md-offset-2 col-md-4 item wow fadeInDown">
                Реклама в Facebook и Instagram — один из самых
                быстрых инструментов маркетинга для получения
                заявок от потенциальных клиентов.

            </div>
            <div class="clearfix"></div>
            <div class="col-md-offset-1 col-md-4 item wow fadeInDown">
                Это отличная возможность услышать мнение о Вашем продукте и сделать
                его лучше.
            </div>
            <div class="col-md-offset-2 col-md-4 item wow fadeInDown">
                Вы перестанете быть конкурентными, если не
                начнете активно использовать таргетинг
                в Facebook и Instagram.
            </div>
        </div>
    </div>
</section>

<!-- Top section -->
<section id="subscribe-section" class="top-section">
    <div class="overlay"></div>
    <div class="bg-video">
        <video muted="muted" loop="loop" autoplay="autoplay">
            <source src="video/promo.mp4" type="video/mp4">
        </video>
    </div>
    <img class="message" src="img/top-section/message.png" alt="message">
    <div class="container">
        <div class="outer">
            <div class="date text-uppercase">
                <b>Бесплатный вебинар / </b>
                 22 августа, вторник, 13:00
            </div>
            <h1 class="page-title">
                Как применить рекламу в Facebook
                и Instagram в своем бизнесе
                для увеличения продаж
            </h1>
            <div class="after-title">
                Зарегистрируйтесь прямо сейчас и получите PDF-инструкцию —
                «Как автоматизировать общение с клиентами с помощью чат-бота в Facebook»
            </div>
            <div class="share">
                Расскажите о мероприятии друзьям:
                <br>
                <div class="likely">
                    <div class="facebook">Поделиться</div>
                    <div class="vkontakte">Поделиться</div>
                    <div class="telegram">Отправить</div>
                </div>
            </div>
        </div>

        <?php include('partials/form.php'); ?>

    </div>

</section>

<?php require_once('footer.php'); ?>
