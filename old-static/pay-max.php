<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#e8f3fe">

    <title>Тариф «MAX»</title>
    <title>SMMSTUDIO — Увеличиваем продажи с помощью рекламы в Facebook и Instagram</title>
    <meta name="description" content="Увеличиваем продажи с помощью рекламы в Facebook и Instagram — основное направление компании SMMSTUDIO."/>

    <meta property="og:title" content="SMMSTUDIO" />
    <meta property="og:description" content="Увеличиваем продажи с помощью рекламы в Facebook и Instagram" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="http://smmstudio.com.ua" />
    <meta property="og:image" content="http://smmstudio.com.ua/img/og.png" />

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5RSJG8L');</script>
    <!-- End Google Tag Manager -->
    
        <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '182480625611556'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=182480625611556&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '142692132975924'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=142692132975924&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '480042189046599'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=480042189046599&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


</head>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<body>

<div class="wrapper price-page">
    <header>
        <div class="wrap">
            <div class="logo-phon">
                <div class="logo-block">
                    <a href="index.php" class="logo"><img src="img/logo.svg" alt="">
                        <p>SMMSTUDIO</p>
                    </a>
                    <p class="signature">цифровое маркетинговое агентство</p>
                </div>
                <div class="phon">
                    <p><img src="img/voip.svg" alt=""><span>0 800 754 754</span></p>
                    <a href="#" class="open-phone-list"><img src="img/arrowsdown.svg" alt=""></a>
                </div>
                <div class="phone-list">
                    <p class="phon"><img src="img/voip.svg" alt="" ></p>
                    <a href="#" class="close"><img src="img/arrows.svg" alt=""></a>
                    <ul>
                        <li><img src="img/ua.svg" alt=""><a href="tel:0800754754">0 800 754 754</a></li>
                        <li><img src="img/ru.svg" alt=""><a href="tel:74993227910">7 499 322 79 10</a></li>
                        <li><img src="img/kz.svg" alt=""><a href="tel:77172696230">77 1726 96 230</a></li>
                        <li><img src="img/gb.svg" alt=""><a href="tel:448000698473">44 800 069 84 73</a></li>
                        <li><p class="schedule">Пн-пт, 9:00 — 18:00 по Киеву</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <section class="pay-content">
        <div class="wrap">
            <h1>Оформление и оплата тарифа</h1>
            <div class="content">
                <div class="form">
                    <h3>Введите личные данные</h3>
                    <form method="POST" accept-charset="utf-8" action="https://www.liqpay.com/api/3/checkout"" role="form" data-toggle="validator" id="pay-form">
                        <div class="form-group input-field has-feedback">
                            <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                            <label for="pay-form-name">Имя</label>
                        </div>
                        <div class="input-field">
                            <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                            <label for="pay-form-phone" class="active-pay">Телефон</label>
                        </div>
                        <div class="form-group input-field has-feedback">
                            <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                            <label for="pay-form-phone">Email</label>
                        </div>

                        <input type="hidden" name="package">
                        <input type="hidden" name="description">
                        <input type="hidden" name="data" value="eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTQyMjI4MzgwNDQ0IiwiYW1vdW50IjoiNDk5IiwiY3VycmVuY3kiOiJVU0QiLCJkZXNjcmlwdGlvbiI6ItCf0LDQutC10YIgwqsgTUFYwrsiLCJ0eXBlIjoiYnV5IiwibGFuZ3VhZ2UiOiJydSJ9" />
                        <input type="hidden" name="signature" value="g+7wHHf3EWiYW4j0gG5miLC4s/k=" />
                        <button type="submit">Оплатить тариф</button>
                    </form>
                    <p>После оплаты мы приступаем к работе</p>
                </div>
                <div class="tarif">
                    <div class="tarif-body">
                        <div class="circle left"></div>
                        <div class="circle right"></div>
                        <div class="top">
                            <div class="text">
                                <p>Тариф</p>
                                <h3>MAX</h3>
                            </div>
                            <div class="logo">
                                <a href="index.php"><img src="img/logo.svg" alt=""></a>
                            </div>
                        </div>
                        <hr>
                        <div class="bottom">
                            <p class="title">Стоимость настройки одного проекта</p>
                            <p class="price">
                                <span>$</span>499
                            </p>
                        </div>
                    </div>

                    <div class="pay-sistem">
                        <p><img src="img/zamok.png" alt=""></p>
                        <p><img src="img/m-card.png" alt=""></p>
                        <p><img src="img/visa.png" alt=""></p>
                        <p><img src="img/p-bank.png" alt=""></p>
                        <p class="text">Оплата происходит на защищенной странице сервиса для оплат <span>Liqpay</span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
        include ('partials/contacts.php');
    ?>
    <?php
        include ('partials/privacy-policy.php');
    ?>
    <?php
        include ('partials/floating-messegers.php');
    ?>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/intlTelInput.css">
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
<link type="text/css" rel="stylesheet" href="css/style.css" />
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/intlTelInput.min.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/prise.js"></script>

</body>

</html>