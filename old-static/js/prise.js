$( function() {
    $('.activ-closer-icon').on('click', function () {
        $('.activ-closer-icon').toggleClass('click-open-rotate');
        $('.activ-closer-icon .acive-icon').toggleClass('togle-active');
        $('.activ-closer-icon .closer-icon').toggleClass('togle-close');
        $('.floating-messages').find('.massegers').slideToggle(300);
    });

    //scroll

    $(".privacy-policy-content").mCustomScrollbar({
        theme:"minimal-dark"
    });


//privasy popup



    $( "#accordion" ).accordion();

    $('.phone-list').hide();

    $('header .phon').click(function(){
        $('.phone-list').slideDown(400);
    });

    $('.phone-list .close').click(function(){
        $('.phone-list').slideUp(400);
    });

    $('.phon-footer').click(function(e){
        e.preventDefault();
        $('.phone-list-footer').slideDown(400);
    });

    $('.phone-list-footer .close').click(function(e){
        e.preventDefault();
        $('.phone-list-footer').slideUp(400);
    });

    $('input[type=tel]').intlTelInput();

    $('input[type=tel]').on('countrychange', function(e, countryData) {
        $(this).val(countryData.dialCode);
    });

    $('input[type=tel]').val($('input[type=tel]').intlTelInput("getSelectedCountryData").dialCode);

    $('.input-field input[type=tel]').on('focus', function () {
        $('.input-field').addClass('form-group has-feedback');
    });



    $.fn.forceNumbericOnly = function() {
        return this.each(function() {
            $(this).keydown(function(e) {
                var key = e.charCode || e.keyCode || 0;
                return (key == 8 || key == 9 || key == 46 || (key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 107 || key == 109 || key == 173 || key == 61);
            });
        });
    };

    $('input[type=tel]').forceNumbericOnly();


} );
$('.input-pay').on('focus', function() {
    $(this).closest('.form-group').find('label').addClass('active-pay');
});
$('.input-pay').on('blur', function() {
    var $this = $(this);
    if ($this.val() == '') {
        $this.closest('.form-group').find('label').removeClass('active-pay');
    }
});

$('#pay-form').validator();

$('a.privacy-open').click(function(event) {
    event.preventDefault();
    $('.privacy-policy-body').fadeIn(400, function() {
        $('.privacy-policy-content').css('display', 'block');
    });
});
$('.privacy-policy-body, .privacy-policy-content .closer').click(function() {
    $('.privacy-policy-content').css('display', 'none');
    $('.privacy-policy-body').fadeOut(400);
});