
<section class="prefooter">
    <div class="wrap">
        <div class="logo-phon">
            <div class="footer-logo">
                <div class="logo">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsmmstudio.com.ua&tabs&width=280&height=220&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=1892932684290317" width="280" height="220" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true">
                    </iframe>
                </div>
            </div>
            <div class="address">
                <p><span>1</span> Украина, Запорожье, пр. Соборный, 192, офис 225</p>
                <p><span>2</span> Уайтхолл, Вестминстер, Лондон SW1A 2EU, Великобритания</p>
            </div>
            <div class="phon-footer">
                <p class="phon">
                    <img src="img/voip.svg" alt="" >
                </p>
                <ul>
                    <li><img src="img/ua_li.svg" alt=""><a href="tel:0800754754">0 800 754 754</a></li>
                    <li><img src="img/ru_li.svg" alt=""><a href="tel:78123095790">7 812 309 57 90</a></li>
                    <!--
                    <li><img src="img/kz_li.svg" alt=""><a href="tel:77172696230">77 1726 96 230</a></li>
                    -->
                    <li><img src="img/gb_li.svg" alt=""><a href="tel:448000698473">44 800 069 84 73</a></li>
                    <li><p class="schedule">Пн-пт, 9:00 — 18:00 по Киеву</p></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="wrap">
        <p>© 2013-2017 SMMSTUDIO</p>
        <p><a href="#" class="privacy-open">Политика конфиденциальности</a></p>
    </div>
</footer>