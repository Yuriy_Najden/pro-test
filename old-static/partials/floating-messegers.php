<section class="floating-messages">
    <div class="massegers">
        <div class="masseger viber">
            <div class="inner">
                <a href="https://www.viber.com/ghsalesbot" target="_blank"></a>
                <div class="help">
                    <p>Viber</p>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="masseger fb-messeger">
            <div class="inner">
                <a href="https://www.facebook.com/messages/t/1272256326219925" target="_blank"></a>
                <div class="help">
                    <p>Messenger</p>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="masseger telegram">
            <div class="inner">
                <a href="https://t.me/GHsales_bot" target="_blank"></a>
                <div class="help">
                    <p>Telegram</p>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="activ-closer-icon">
        <p class="acive-icon"></p>
        <p class="closer-icon"></p>
        <div class="angle"></div>
    </div>
    <!--
    <div class="hidden-hint">
        <p>Пишите нам в мессенджер</p>
        <div class="figure"></div>
    </div>
    -->
</section>