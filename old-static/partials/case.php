
<section class="ours-cases" id="case">
    <h2 class="block-title">Истории успеха</h2>
    <div class="wrap">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#all-case" data-toggle="tab">Все кейсы</a></li>
            <li><a href="#service-case" data-toggle="tab">Услуги</a></li>
            <li><a href="#training-case" data-toggle="tab">Обучение</a></li>
            <li><a href="#events-case" data-toggle="tab">Мероприятия</a></li>
            <li><a href="#goods-case" data-toggle="tab">Товары</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane" id="service-case">
                <div class="cases-part">
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-1">
                        <div class="img">
                            <img src="img/cases/serv/pangeo.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>PanGeo</h3>
                                <p>Круизы на лайнере</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">20</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$3,62</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">10</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-2">
                        <div class="img">
                            <img src="img/cases/serv/SergeyKantsirenko.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Сергей Канциренко</h3>
                                <p>Свадебный фотограф</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">4 456</span><span class="text">переходов на сайт</span></p>
                                <p><span class="number">$0,08</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                <p><span class="number">73</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-3">
                        <div class="img">
                            <img src="img/cases/serv/viviano.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Viviano</h3>
                                <p>Центр инновационной косметологии</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">312</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$2,02</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">25</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-4">
                        <div class="img">
                            <img src="img/cases/serv/imperialgroup.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Империал Групп Украина</h3>
                                <p>Производитель поликарбоната в Украине</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">267</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$4,7</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">30</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-5">
                        <div class="img">
                            <img src="img/cases/serv/silkepil.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Silk Epil Studio</h3>
                                <p>Студия лазерной эпиляции</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">283</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$6,03</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">98</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="serv-hidden-cases hidden-cases"></div>

                <div class="button-block button-down-block">
                    <p>больше примеров</p>
                    <a href="#"><img src="img/more.svg" alt=""></a>
                </div>
                <div class="button-block button-up-block">
                    <p>Скрыть</p>
                    <a href="#up-position"><img src="img/more.svg" alt=""></a>
                </div>
            </div>
            <!-- -->
            <div class="tab-pane" id="training-case">
                <div class="cases-part">
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-1">
                        <div class="img">
                            <img src="img/cases/study/AirAcademy.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3> Air Academy</h3>
                                <p>Курс по видеомаркетингу «YouTube для бизнеса»</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">91</span><span class="text">ЦЕЛЕВая ЗАЯВКа</span></p>
                                <p><span class="number">$5,5</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">30</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-2">
                        <div class="img">
                            <img src="img/cases/study/Simpletrade.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Simpletrade</h3>
                                <p>Онлайн мастер-класс «Как заработать на криптовалюте»</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">157</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$1,99</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                <p><span class="number">5</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-3">
                        <div class="img">
                            <img src="img/cases/study/krupkin.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>«Бюро продаж Андрея Крупкина»</h3>
                                <p>Бесплатный вебинар Андрея Крупкина «Построить отдел продаж за 7 дней: от найма до адаптации»</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">240</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$2,16</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">7</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-4">
                        <div class="img">
                            <img src="img/cases/study/karpachov.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Дмитрий Карпачёв</h3>
                                <p>Заявки на бесплатный вебинар</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">31559</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$0,27</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">7</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-5">
                        <div class="img">
                            <img src="img/cases/study/30-steps-to-a-slender-figure.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>«30 шагов к стройной фигуре»</h3>
                                <p>Онлайн-курс по похудению</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">3886</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$0,26</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">27</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-6">
                        <div class="img">
                            <img src="img/cases/study/together.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>«ВМЕСТЕ»</h3>
                                <p>Детский клуб </p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">460</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$0,42</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">23</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="study-hidden-cases hidden-cases">
                    <div class="cases-part">
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-7">
                            <div class="img">
                                <img src="img/cases/study/release.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>Release</h3>
                                    <p>Комплекс инновационного внешкольного образования для детей</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">529</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$1,7</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">18</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-8">
                            <div class="img">
                                <img src="img/cases/study/bra.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>«Автоматизация учета в 1С 8 Бухгалтерия»</h3>
                                    <p>Бесплатный вебинар от онлайн-школы для бухгалтеров bpa.School</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">1065</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$0,28</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                    <p><span class="number">7</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>

                    </div>
                </div>
                <div class="button-block button-down-block">
                    <p>больше примеров</p>
                    <a href="#"><img src="img/more.svg" alt=""></a>
                </div>
                <div class="button-block button-up-block">
                    <p>Скрыть</p>
                    <a href="#service-case"><img src="img/more.svg" alt=""></a>
                </div>
            </div>
            <div class="tab-pane" id="events-case">
                <div class="cases-part">
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-events-1">
                        <div class="img">
                            <img src="img/cases/events/AndreyShatyrkoOnlyTop.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Only Top </h3>
                                <p>Заявки на живое мероприятие для предпринимателей в Киеве «Youtube. Мастер-класс Андрея
                                    Шатырко»
                                </p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">199</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$3,99</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">30</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-events-2">
                        <div class="img">
                            <img src="img/cases/events/Vysotsky.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Visotsky Consulting</h3>
                                <p>Заявки на тренинг-практикум для владельцев бизнеса</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">550</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$10,80</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                <p><span class="number">94</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="events-hidden-cases hidden-cases"></div>
                <div class="button-block button-down-block">
                    <p>больше примеров</p>
                    <a href="#"><img src="img/more.svg" alt=""></a>
                </div>
                <div class="button-block button-up-block">
                    <p>Скрыть</p>
                    <a href="#events-case"><img src="img/more.svg" alt=""></a>
                </div>

            </div>
            <div class="tab-pane" id="goods-case">
                <div class="cases-part">
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-goods-1">
                        <div class="img">
                            <img src="img/cases/goods/1Case.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>1case</h3>
                                <p>Именные футболки и свитшоты</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">260</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$2,53</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">42</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-goods-2">
                        <div class="img">
                            <img src="img/cases/goods/Proshar.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>«Proshar»</h3>
                                <p>Латексные шары оптом</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">60</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$2</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                <p><span class="number">33</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-goods-3">
                        <div class="img">
                            <img src="img/cases/goods/g_kids_ua.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>G kids</h3>
                                <p>Стильная детская одежда</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">40</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$0,66</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">9</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-goods-4">
                        <div class="img">
                            <img src="img/cases/goods/DERZHAK.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>DERZHAK</h3>
                                <p>Беспроводные зарядные устройства для авто</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">475</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$2</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">61</span><span class="text">ДЕНЬ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>

                </div>
                <div class="goods-hidden-cases hidden-cases"></div>
                <div class="button-block button-down-block">
                    <p>больше примеров</p>
                    <a href="#"><img src="img/more.svg" alt=""></a>
                </div>
                <div class="button-block button-up-block">
                    <p>Скрыть</p>
                    <a href="#goods-case"><img src="img/more.svg" alt=""></a>
                </div>
            </div>

            <div class="tab-pane active" id="all-case">
                <div class="cases-part">
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-1">
                        <div class="img">
                            <img src="img/cases/serv/pangeo.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>PanGeo</h3>
                                <p>Круизы на лайнере</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">20</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$3,62</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">10</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-1">
                        <div class="img">
                            <img src="img/cases/study/AirAcademy.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3> Air Academy</h3>
                                <p>Курс по видеомаркетингу «YouTube для бизнеса»</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">91</span><span class="text">ЦЕЛЕВая ЗАЯВКа</span></p>
                                <p><span class="number">$5,5</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">30</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-events-1">
                        <div class="img">
                            <img src="img/cases/events/AndreyShatyrkoOnlyTop.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Only Top </h3>
                                <p>Заявки на живое мероприятие для предпринимателей в Киеве «Youtube. Мастер-класс Андрея
                                    Шатырко»
                                </p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">199</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$3,99</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">30</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-goods-1">
                        <div class="img">
                            <img src="img/cases/goods/1Case.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>1case</h3>
                                <p>Именные футболки и свитшоты</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">260</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$2,53</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                <p><span class="number">42</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-2">
                        <div class="img">
                            <img src="img/cases/serv/SergeyKantsirenko.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Сергей Канциренко</h3>
                                <p>Свадебный фотограф</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">4 456</span><span class="text">переходов на сайт</span></p>
                                <p><span class="number">$0,08</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                <p><span class="number">73</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-2">
                        <div class="img">
                            <img src="img/cases/study/Simpletrade.jpg" alt="">
                        </div>
                        <div class="preview">
                            <div class="name-case">
                                <h3>Simpletrade</h3>
                                <p>Онлайн мастер-класс «Как заработать на криптовалюте»</p>
                            </div>

                            <div class="case-numbers">
                                <p><span class="number">157</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                <p><span class="number">$1,99</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                <p><span class="number">5</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="all-hidden-cases hidden-cases">
                    <div class="cases-part">
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-events-2">
                            <div class="img">
                                <img src="img/cases/events/Vysotsky.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>Visotsky Consulting</h3>
                                    <p>Заявки на тренинг-практикум для владельцев бизнеса</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">550</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$10,80</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                    <p><span class="number">94</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-goods-2">
                            <div class="img">
                                <img src="img/cases/goods/Proshar.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>«Proshar»</h3>
                                    <p>Латексные шары оптом</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">60</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$2</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                    <p><span class="number">33</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-3">
                            <div class="img">
                                <img src="img/cases/serv/viviano.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>Viviano</h3>
                                    <p>Центр инновационной косметологии</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">312</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$2,02</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">25</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-3">
                            <div class="img">
                                <img src="img/cases/study/krupkin.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>«Бюро продаж Андрея Крупкина»</h3>
                                    <p>Бесплатный вебинар Андрея Крупкина «Построить отдел продаж за 7 дней: от найма до адаптации»</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">241</span><span class="text">ЦЕЛЕВая ЗАЯВКа</span></p>
                                    <p><span class="number">$2,16</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">7</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-goods-3">
                            <div class="img">
                                <img src="img/cases/goods/g_kids_ua.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>G kids</h3>
                                    <p>Стильная детская одежда</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">40</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$0,66</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">9</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-4">
                            <div class="img">
                                <img src="img/cases/serv/imperialgroup.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>Империал Групп Украина</h3>
                                    <p>Производитель поликарбоната в Украине</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">267</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$4,7</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">30</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="cases-part">
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-4">
                            <div class="img">
                                <img src="img/cases/study/karpachov.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>Дмитрий Карпачёв</h3>
                                    <p>Заявки на бесплатный вебинар</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">31559</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$0,27</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">7</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-goods-4">
                            <div class="img">
                                <img src="img/cases/goods/DERZHAK.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>DERZHAK</h3>
                                    <p>Беспроводные зарядные устройства для авто</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">475</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$2</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">61</span><span class="text">ДЕНЬ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-serv-5">
                            <div class="img">
                                <img src="img/cases/serv/silkepil.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>Silk Epil Studio</h3>
                                    <p>Студия лазерной эпиляции</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">283</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$6,03</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">98</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-5">
                            <div class="img">
                                <img src="img/cases/study/30-steps-to-a-slender-figure.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>«30 шагов к стройной фигуре»</h3>
                                    <p>Онлайн-курс по похудению</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">3886</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$0,26</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">27</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-6">
                            <div class="img">
                                <img src="img/cases/study/together.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>«ВМЕСТЕ»</h3>
                                    <p>Детский клуб </p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">460</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$0,42</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">23</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-7">
                            <div class="img">
                                <img src="img/cases/study/release.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>Release</h3>
                                    <p>Комплекс инновационного внешкольного образования для детей</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">529</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$1,7</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                                    <p><span class="number">18</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="case-item" data-toggle="modal" data-target="#modal-study-8">
                            <div class="img">
                                <img src="img/cases/study/bra.jpg" alt="">
                            </div>
                            <div class="preview">
                                <div class="name-case">
                                    <h3>«Автоматизация учета в 1С 8 Бухгалтерия»</h3>
                                    <p>Бесплатный вебинар от онлайн-школы для бухгалтеров bpa.School</p>
                                </div>

                                <div class="case-numbers">
                                    <p><span class="number">1065</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                                    <p><span class="number">$0,28</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                                    <p><span class="number">7</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="button-block button-down-block">
                    <p>больше примеров</p>
                    <a href="#"><img src="img/more.svg" alt=""></a>
                </div>
                <div class="button-block button-up-block">
                    <p>Скрыть</p>
                    <a href="#all-case"><img src="img/more.svg" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal serv -->
<div class="modal fade case-modal" id="modal-serv-1" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/serv/pangeo-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">PanGeo</h4>
                <h3>Круизы на лайнере</h3>
            </div>
            <div class="modal-body">
                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Часто путешествующие люди, которым надоел стандартный
                            отдых. Аудитория 30 000 человек.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">от $600</p>
                    </div>
                    <div class="item">
                        <p class="title">KPI</p>
                        <p class="text">Максимальное количество заявок, стоимостью не дороже $5.</p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Отсутствие сайта. <br>
                            Слабая узнаваемость бренда. <br>
                            Не популярный вид отдыха.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Качественный видеоконтент, демонстрирующий круизы, как динамичный, комфортабельный и насыщенный отдых. <br>
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили кампании на лид-форму. Рекламные объявления, раскрывающие основные
                            преимущества круизного отдыха, и дающие общее представление о нем. <br>
                            Использовали тематические картинки с призывом подробнее узнать о круизах.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">После нашей консультации с компанией PanGeo было принято решение подготовить
                            рекламные материалы, подробно раскрывающие ценность услуги, для запуска на охват, и
                            получения с помощью этого теплых клиентов.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">20</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОК</span></p>
                    <p><span class="number">$3,62</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">10</span><span class="text">ДНЕЙ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <h3>Вам нужен системный
                рентабельный трафик в бизнес?
            </h3>
            <form data-toggle="validator"  action="send.php"  method="POST">
                <div class="form-group input-field has-feedback">
                    <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                    <label for="pay-form-name">Имя</label>
                </div>
                <div class="input-field">
                    <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                    <label for="pay-form-phone" class="active-pay">Телефон</label>
                </div>
                <div class="form-group input-field has-feedback">
                    <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                    <label for="pay-form-phone">Email</label>
                </div>
                <input type="hidden" name="caselid" value="с кейса PanGeo">
                <button type="submit">Обсудить задачу</button>
            </form>
        </div>
    </div>
</div>


<div class="modal fade case-modal" id="modal-serv-2" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/serv/SergeyKantsirenko-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Сергей Канциренко</h4>
                <h3>Свадебный фотограф</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Женщины 20-35, помолвленные, интересующиеся организацией собственной свадьбы.
                            Всего — 300 000.
                        </p>
                    </div>
                    <!--<div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">от $600</p>
                    </div>-->
                    <div class="item">
                        <p class="title">KPI</p>
                        <p class="text">Максимальное количество переходов по низкой стоимости.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Не активированы другие рекламные каналы.
                            Большая конкуренция в данной сфере.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Портфолио. <br>
                            15 лет опыта работы фотографом: <br>
                            — публиковался в National  Geographic Ukraine; <br>
                            — авторские фотовыставки; <br>
                            — клиенты — такие крупные компании как:  Euroevent, Renault, Cisco, MTI, Samsung,  Bacardi,
                            Subaru, Lamborghini.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустить рекламное объявление в формате кольцевой галереи с примерами работ фотографа.
                            <br>

                            Написали текст объявления с акцентом на опыт фотографа. <br>

                            Призыв — переход на сайт для ознакомления с портфолио/возможность получить консультацию по телефону.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Свадьба — очень серьёзное событие для любой пары, поэтому важно вызвать доверие к
                            фотографу: показать примеры работ, акцентировать внимание на том, что фотограф не новичок, а имеет
                            багаж опыта за плечами. Все это следует передать с помощью рекламного объявления потенциальному клиенту.

                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">4 456</span><span class="text">переходов на сайт</span></p>
                    <p><span class="number">$0,08</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ клика</span></p>
                    <p><span class="number">73</span><span class="text">ДНЯ ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Сергей Канциренко">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-serv-3" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/serv/viviano-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Viviano</h4>
                <h3>Центр инновационной косметологии</h3>
            </div>
            <div class="modal-body">
                <h3>Viviano — центр инновационной косметологии.
                    Основные услуги: гальваническое омоложение кожи, ультразвуковое омоложение лица, обертывание против
                    целлюлита.
                </h3>

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Целевая аудитория — женщины от 25 лет. Их в Facebook и Instagram — 70 000.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">За один сеанс — 500 грн. <br>
                            За курс процедур — 4000 грн.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Максимальное количество заявок.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Центр только открылся. <br>
                            Не активированы другие рекламные каналы. <br>
                            Отсутствие сайта. <br>
                            Отсутствие рекламных материалов. <br>

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Очень осознанный клиент. <br>
                            Есть отдел продаж, который готов принимать заявки. <br>
                            Закуплено современное оборудование. <br>
                            Центр находится в центре города. <br>
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Настроили рекламу на всех женщин в Черновцах от 25 лет. Таким образом, дали
                            возможность Facebook самому выбрать целевую аудиторию. Сделали акцент на том, что центр только открылся,
                            подчеркнули его уникальность. Призыв к действию — получите бесплатную консультацию от
                            косметолога центра + скидку на первый сеанс.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Для локального рынка важно расширять аудиторию, при этом, если компания только
                            открывается — необходимо об этом сообщить, создав дополнительный информационный повод. Рекламный
                            текст, уникальное предложение — залог успеха.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">312</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$2,02</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">25</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Viviano">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-serv-4" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/serv/imperialgroup-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Империал Групп Украина</h4>
                <h3>Производитель поликарбоната в Украине</h3>
            </div>
            <div class="modal-body">
                <h3>Основные направления работы: производство сотового и монолитного поликарбоната, изготовление теплиц
                    из поликарбоната, производство конструкций и комплектующих.
                </h3>

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Мужчины от 25 лет (без сегментации по потребностям, широкая) — 3 200 000.
                            Ядро — фермеры, владельцы коттеджей и домов, промышленные предприятия.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">От 30 000 до 100 000 грн по разным продуктам.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">200-300 заявок.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Отсутствие успешного опыта работы бренда в социальных медиа.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Осознанное руководство.
                            Сильный отдел продаж.
                            Контекстная реклама от топового агентства в СНГ.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Разделить кампанию на 3 аудитории: <br>
                            — широкая аудитория (использовать настройки по интересам внутри Facebook с предложением оставить
                            заявку на консультацию (специальный мини-лендинг); <br>
                            — на тех, кто был на сайте. По каждому продукту. ; <br>
                            — реклама со спецпредложением на широкую аудиторию, с использованием лид-формы.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Данный проект продолжает развиваться. Он показал, что за короткий период времени
                            можно резко увеличить количество заявок в сфере 2b2. Стратегия, которая разработана на основе
                            анализа конкурентов, тестов и понимания специфики социальных медиа — залог успеха любого проекта.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">267</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$4,7</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">30</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Империал Групп Украина">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-serv-5" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/serv/silkepil-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Silk Epil Studio</h4>
                <h3>Студия лазерной эпиляции</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Женщины от 18 лет (120 000 человек, зарегистрированных в Facebook).

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">Лазерная эпиляция — 1000 грн. <br>
                            Перманентный макияж — 2300 грн.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Максимальное количество заявок стоимостью не более $7.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Страх целевой аудитории попасть к неквалифицированным мастерам. <br>
                            Наличие негативных мифов о лазерной эпиляции.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Опытные специалисты, которые подбирают параметры индивидуально для каждого клиента.
                            <br>
                            Лазер последнего поколения с охлаждающей насадкой. <br>
                            Цена, гарантии.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Учитывая, что у клиента была хорошая посадочная страница, мы решили запускать
                            рекламную кампанию на неё с призывом оставить заявку. <br>

                            Для тех людей, которые посещали сайт или взаимодействовали с брендом в Facebook мы запустили
                            отдельные рекламные кампании с полезными статьями:«Как подготовиться к процедуре лазерной эпиляции»
                            «Основные мифы о лазерной эпиляции» и т.д. <br>

                            Для праздников и других мероприятий (Чёрная пятница, Новый год, Рождество) были разработаны
                            отдельные лендинги со специальными предложениями. На данные лендинги также были запущены рекламные
                            кампании с указанием скидок. Данные запуски принесли наилучшие результаты. <br>

                            Кроме того мы протестировали чат-бота в Facebook Messenger, что дало хороший результат.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Для локального рынка важно расширять аудиторию. Кроме того, для аудитории, которая
                            уже знает о бренде, важно давать качественный контент в виде полезных статей и скидок. Таким
                            образом мы превращаем «теплого» клиента, который только узнал о бренде, в «горячего», который
                            полностью доверяет бренду и готов заказать услугу.
                        </p>
                    </div>
                </div>

                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">283</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$6,03</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">98</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Silk Epil Studio">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal study -->

<div class="modal fade case-modal" id="modal-study-1" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/study/AirAcademy-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Air Academy</h4>
                <h3>Курс по видеомаркетингу «YouTube для бизнеса»</h3>
            </div>
            <div class="modal-body">
                <h3>Первый в СНГ системный курс по созданию и продвижению Youtube-каналов для бизнеса. <br>
                </h3>

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Киев 150 000. <br>
                            Украина, Россия 2 500 000.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">Стоимость обучения делилась на 2 категории: <br>
                            Онлайн $299 <br>
                            Оффлайн $450 и $960
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">70 заявок по $7.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Первая рекламная кампания в социальных сетях, бренд академии никто не знал.
                            Высокая стоимость обучения.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Хорошая посадочная страница. <br>
                            Актуальность темы. <br>
                            Хороший видеоматериал снятый заказчиком, видеоотзывы людей, прошедших курс. <br>
                            Академия гарантирует получение результата. <br>
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили отдельно кампании на Киев и всю остальную гео-аудиторию.
                            <br>

                            Сделали акцент на необходимости продвижения современного бизнеса в YouTube и уникальности
                            предоставляемых академией услуг. <br>

                            Показали успешный опыт работы спикеров с крупными брендами. На всех, кто посещал сайт и
                            взаимодействовал со страницей, настроили ретаргет с видеоотзывами людей, прошедшими обучение
                            в академии.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Заказчик создал хорошую посадочную страницу, с подробным описанием уникальности
                            своей услуги, а также качественным видеоматериалом об обучении в академии. Поэтому потенциальных
                            клиентов не смутила цена. <br>

                            Хорошо показала себя настройка на lkl аудитории и ретаргет с отзывами на людей, посетивших сайт.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">91</span><span class="text">ЦЕЛЕВая ЗАЯВка</span></p>
                    <p><span class="number">$5,5</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">30</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Air Academy">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-study-2" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/study/Simpletrade-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Simpletrade</h4>
                <h3>Онлайн мастер-класс «Как заработать на криптовалюте»</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Целевая аудитория без Москвы и Санкт-Петербурга 8 200 000.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">Стоимость платного курса, который компания продает посредством вебинаров от $700
                            до $3000. Мастер-класс — бесплатный.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Максимальное количество заявок, стоимостью не дороже $2.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Посадочная страница на онлайн-конструкторе, непродуманное УТП, отсутствие отзывов
                            с прошлых мастер-классов. <br>
                            Высокий уровень конкуренции из-за актуальности темы.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Актуальность тренда криптовалюты. <br>
                            Регулярное освещение и поддержка интереса к теме со стороны центральных СМИ. <br>
                            Успешные примеры большого заработка на криптовалюте. <br>
                            Большая аудитория взаимодействия с бизнес-страницей заказчика. <br>
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Принято решение настраивать рекламу на 4 группы аудиторий: <br>
                            Инвесторы. <br>
                            Бизнесмены, владельцы. <br>
                            Взаимодействие со страницей. <br>
                            lkl по регистрациям прошлых мастер-классов. <br>

                            Помимо стандартных рекламных объявлений, было принято решение привлекать людей на мастер-класс,
                            обыграв актуальную тему в СМИ. <br>
                            Запуск рекламы в instagram stories на аудиторию lkl, с целью привлечения большего количества
                            людей на сайт.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Самые дешевые лиды были из историй в instagram.  <br>
                            Лучше всего сработала аудитория lkl.

                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">157</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$1,99</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">5</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Simpletrade">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-study-3" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/study/krupkin-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">«Бюро продаж Андрея Крупкина»</h4>
                <h3>Бесплатный вебинар Андрея Крупкина «Построить отдел продаж за 7 дней: от найма до адаптации»</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Руководители, владельцы бизнеса, предприниматели, руководители отдела продаж,
                            онлайн бизнесмены 500 000.
                        </p>
                    </div>
                    <!--<div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">Стоимость платного курса, который компания продает посредством вебинаров от $700
                            до $3000. Мастер-класс — бесплатный.
                        </p>
                    </div>-->

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Максимальное количество заявок, стоимостью не дороже $3.
                        </p>
                    </div>
                    <!--<div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Посадочная страница на онлайн-конструкторе, непродуманное УТП, отсутствие отзывов
                            с прошлых мастер-классов. <br>
                            Высокий уровень конкуренции из-за актуальности темы.
                        </p>
                    </div>-->
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Хорошая посадочная страница. <br>
                            Репутация бренда. <br>
                            Качественный видеоконтент. <br>
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Дало хороший результат видео, где Андрей интервьюирует своего успешного клиента.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Качественный продукт и высокая конверсия сайта всегда благоприятно влияют на ход рекламной кампании.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">241</span><span class="text">ЦЕЛЕВая ЗАЯВка</span></p>
                    <p><span class="number">$2,16</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">7</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса «Бюро продаж Андрея Крупкина»">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-study-4" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/study/karpachov-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Дмитрий Карпачёв</h4>
                <h3>Заявки на бесплатный вебинар</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Женщины, возраст 18 – 50.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">Бесплатный вебинар.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Цена за заявку не более $0,40, как можно большее количество регистраций.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Более года таргетировались в этом проекте на одну аудиторию.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Дмитрий Карпачёв — известный психолог. Ему доверяют.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили рекламу на конверсии на сайте и ManyChat (люди в два клика становились
                            участниками вебинара с рекламного поста).
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">С ManyChat заявки дешевле. <br>
                            Дмитрий Карпачёв — проект, у которого ЦА — женщины всей страны. За счет того, что он профессионал в
                            своем деле и уже пользуется их доверием, на его вебинары женщины регистрируются легко.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">31559</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$0,27</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">7</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Дмитрий Карпачёв">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-study-5" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/study/30-steps-to-a-slender-figure-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">«30 шагов к стройной фигуре»</h4>
                <h3>Онлайн-курс по похудению</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Женщины, возраст 18 — 47 лет, владеют бизнесом, интересуются диетами,
                            диетическим питанием, одеждой больших размеров.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">2000 руб.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Целевые заявки по минимальной стоимости.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Отсутствие сайта. <br>
                            Неизвестный диетолог.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Личное сопровождение и поддержка диетолога на протяжении всего курса. Консультации.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили чат-бота. Настроили рекламную кампанию на платежеспособную аудиторию.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">В России клики и показы, были дороже, но они себя окупали, так как там было больше
                            продаж.

                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">3886</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$0,26</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">27</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса «30 шагов к стройной фигуре»">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-study-6" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/study/together-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">«ВМЕСТЕ»</h4>
                <h3>Детский клуб</h3>
            </div>
            <div class="modal-body">
                <h3>Основные направления работы: клуб ТВ, киноклуб, фотоклуб, вокальный клуб, творческий
                    клуб, бизнес клуб.
                </h3>

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Родители, члены семьи от 25 лет. Всего — 370 000 человек.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">4000 грн за полный курс обучения.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">300-400 заявок, чтобы набрать 100 студентов.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Плохой сайт, который не работает. <br>
                            Нет качественных фото и видеоматериалов.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Клуб давно работает в городе. Только положительные отзывы. Владельцы живут
                            своим делом. Полное доверие к нашему агентству. Администраторы, которые обрабатывают
                            заявки — опытные и ответственные.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили рекламу на лид-форму. Показали фотографиями жизнь клуба (дети снимают
                            фильмы, выступают, к ним приезжают звезды). Использовали призыв зарегистрироваться на день
                            открытых дверей. Упоминаем все регалии и достижения клуба.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Мы попали с предложением с первого запуска: объявили громко о новом наборе,
                            затронули боли родителей, упомянули все регалии клуба. Клиент остался счастливым.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">460</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$0,42</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">23</span><span class="text">ДНя ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса «ВМЕСТЕ»">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-study-7" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/study/release-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Release</h4>
                <h3>Комплекс инновационного внешкольного образования для детей</h3>
            </div>
            <div class="modal-body">
                <h3>Основные направления: спорт и танцы, кино, наука и технологии, искусство и творчество, иностранные
                    языки, я — лидер.
                </h3>

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Родители, члены семьи от 25 лет. Всего — 2 100 000 человек.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">От 24 000 грн/год.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">400-500 заявок.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Высокая стоимость обучения. Элитарность бренда (хотя есть относительно и
                            доступные пакеты обучения). Новый отдел продаж, скрытие цены обучения в рекламе (попадают не
                            целевые заявки).

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">В школе преподают украинские звезды: Влад Яма, Олег Лисогор, Остап Ступка,
                            Владислав Ващук.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Мы запустили рекламу на разные направления, используя уникальный текст, фотографии
                            занятий и звезд в формате кольцевой галереи.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Если компания имеет широкий спектр услуг — необходимо тестирование всех
                            предложений на широкую аудиторию. Гипотезы на счет потребительского поведения могут вызвать
                            неэффективные действия со стороны специалистов в digital.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">529</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$1,7</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">18</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Release">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-study-8" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/study/bra-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">«Автоматизация учета в 1С 8 Бухгалтерия»</h4>
                <h3>Бесплатный вебинар от онлайн-школы для бухгалтеров bpa.School</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Бухгалтера от 23 до 50 лет.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">Бесплатный вебинар.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Максимальное количество заявок стоимостью не более $2.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Первый практический опыт запуска вебинара у клиента.
                            Сложность в прогнозировании стоимости регистраций на вебинар и количества зарегистрированных
                            участников. Узкая, специфическая аудитория.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Вебинар проводится бесплатно для всех желающих.
                            Спикер вебинара поделится практическим опытом бухгалтерской компании «Бухгалтерский сервис
                            b.p.a.» в автоматизации работы с 1С. Все решения и советы, которые будут раскрыты на вебинаре,
                            участники смогут самостоятельно внедрить в своей работе.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Разделить кампанию на 2 аудитории: <br>
                            — мужчины и женщины от 25 до 50 лет, которые потенциально являются бухгалтерами или связаны с
                            бухгалтерией; <br>
                            — мужчины и женщины от 25 до 50 лет, которые ранее взаимодействовали с компанией b.p.a.: посещали
                            сайт компании и страницы отдельных продуктов, взаимодействовали со страницей в Facebook и т.д.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Данный запуск показал, что даже за короткий промежуток времени можно собрать
                            большое количество заявок на узкоспециализированный продукт — бесплатный вебинар для бухгалтеров.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">1065</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$0,28</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">7</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса «Автоматизация учета в 1С 8 Бухгалтерия»">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal goods -->

<div class="modal fade case-modal" id="modal-goods-1" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/goods/1-case-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">1case</h4>
                <h3>Именные футболки и свитшоты</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Мужчины и женщины 18-35 лет.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">449 грн.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Привлечение максимального количества заявок стоимостью не дороже $1,5.
                        </p>
                    </div>
                    <!--<div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Первый практический опыт запуска вебинара у клиента.
                            Сложность в прогнозировании стоимости регистраций на вебинар и количества зарегистрированных
                            участников.Узкая, специфическая аудитория
                        </p>
                    </div>-->
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Стильная одежда. <br>
                            Приемлемая стоимость. <br>
                            Возможность создать уникальный дизайн принта. <br>

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили рекламную кампанию на широкую аудиторию по всей Украине.
                            Первая аудитория — влюбленные пары. Интересы: помолвка, свадьба, скоро день рождения, годовщина.
                            <br>
                            Ретаргет на тех, кто был на сайте. <br>
                            Lookalike на тех, кто сделал заказ.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Хорошо показал себя формат Instagram Stories и слайд шоу.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">260</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$2,53</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">42</span><span class="text">ДНя ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса 1case">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-goods-2" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/goods/Proshar-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">«Proshar»</h4>
                <h3>Латексные шары оптом</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Организаторы мероприятий от 25 лет — 190 000 человек.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">449 грн.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Максимальное количество заявок стоимостью до $3,5.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Не внушающий доверия сайт.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Возможность получить бесплатные образцы шаров. <br>
                            Продажа напрямую от производителя, без посредников.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили рекламную кампанию на предпринимателей, которые занимаются организацией
                            различных мероприятий, в том числе свадеб. Таким образом вышли на оптовиков. <br>

                            Запустили рекламное объявление с видеороликом и изображениями самого процесса производства.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Предложение получить бесплатные образцы товара дают возможность оптовым заказчикам
                            убедиться в качестве продукции и решиться, в дальнейшем, на приобретение оптовой партии. Поэтому,
                            при продвижении оптовых производителей и продавцов такой оффер даёт хорошие результаты.
                        </p>
                    </div>
                </div>

                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">60</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$2</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">33</span><span class="text">ДНя ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса «Proshar»">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-goods-3" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/goods/g_kids_ua-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">G kids</h4>
                <h3>Стильная детская одежда</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Молодые мамы 20-34 года.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">600 грн.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">40 обращений в день (вопросы в директ, вопросы в комменарии) по $0,66.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Опасения и недоверие к организации из-за отсутствия сайта.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Стильная одежда, индивидуальный пошив на любой размер, качественные материалы.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили таргетированную рекламу в Instagram и Facebook (мобильные устройства)
                            с целью перехода на аккаунт в instagram.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Аккаунт в Instagram приносит на данный момент больше продаж, чем оффлайн-шоурум в
                            Одессе.

                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">40</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$0,66</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">9</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса G kids">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-goods-4" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/goods/DERZHAK-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">DERZHAK</h4>
                <h3>Беспроводные зарядные устройства для авто</h3>
            </div>
            <div class="modal-body">

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Часто путешествующие, арендующие авто люди.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">2794 грн.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">25 заявок в день по цене $2.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Конверсия сайта 0%. <br>
                            Высокая стоимость устройства.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Устройство незаменимо для тех, кто много времени находится в дороге, часто
                            путешествует на авто. <br>
                            Отсутствуют регулярно ломающиеся и запутывающиеся провода.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили лид-форму. Оффер: будьте всегда на связи, инновационное устройство
                            украинского бренда, которое призвано решить проблему запутанных проводов раз и навсегда.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">В Facebook и Instagram можно продавать товары подобной специфики.

                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">475</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$2</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">61</span><span class="text">День ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса DERZHAK">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal events -->

<div class="modal fade case-modal" id="modal-events-1" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/events/AndreyShatyrkoOnlyTop-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Only Top </h4>
                <h3>Заявки на живое мероприятие для предпринимателей в Киеве «Youtube. Мастер-класс Андрея Шатырко»
                </h3>
            </div>
            <div class="modal-body">
                <h3>На мастер-классе Андрей Шатырко учит создавать, оформлять и наполнять канал на YouTube, поддерживать
                    к нему интерес с целью дальнейшего привлечения клиентов из YouTube.
                </h3>

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Владельцы бизнеса, руководители предприятий, маркетологи.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">Билет на мероприятие 300 грн предзаказ, 500 грн в день мастер-класса.
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">200 лидов по $4.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Платное мероприятие по продвижению канала на YouTube, но у спикера отсутствует
                            свой раскрученный канал.
                            Отсутствие видеоконтента.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">YouTube — узнаваемый бренд, второй по посещаемости сайт в интернете, бизнес
                            интуитивно понимает, что продвижение на YouTube может увеличить продажи. <br>

                            Хороший сайт, раскрывающий преимущества и вызывающий доверие к мастер-классу.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Кампания на конверсию.
                            Рекламные посты, описывающие важность работы с YouTube, презентация спикера, как одного из самых
                            опытных YouTubeров Украины.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Мероприятие рассчитано на бизнесменов и маркетологов. <br>
                            Было запущено 3 группы объявлений на аудитории: <br>
                            бизнес, пересеченный с интересом youtube, <br>
                            маркетологи, онлайн-бизнесмены. <br>
                            Все три аудитории были расширены и дали хороший результат.


                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">199</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$3,99</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">30</span><span class="text">ДНей ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Only Top">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade case-modal" id="modal-events-2" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close mob-close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/cases/events/VisotskyConsulting-bg.jpg')">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/close-icon.svg" alt=""></button>
                <h4 class="modal-title">Visotsky Consulting</h4>
                <h3>Заявки на тренинг-практикум для владельцев бизнеса</h3>
            </div>
            <div class="modal-body">
                <h3>Visotsky Consulting — консалтинговый проект для малого и среднего бизнеса Школы Владельцев Бизнеса.
                </h3>

                <div class="item-wrapper">
                    <div class="item">
                        <p class="title">Целевая аудитория
                            в Facebook и Instagram</p>
                        <p class="text">Владельцы, руководители крупного, среднего и малого бизнеса.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Средний чек использования
                            услуги/стоимость товара
                        </p>
                        <p class="text">$1500
                        </p>
                    </div>

                    <div class="item">
                        <p class="title">KPI
                        </p>
                        <p class="text">Привлечение заявок на 4-дневный практический тренинг-практикум. Заявки не дороже
                            $11.
                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Барьеры</p>
                        <p class="text">Высокая стоимость обучения. <br>
                            Высокий уровень конкуренции в инфобизнесе. <br>

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Драйверы</p>
                        <p class="text">Высокий уровень доверия к бренду Visotsky Consulting. <br>
                            Хорошая посадочная страница. <br>
                            Много качественного фото- и видеоконтента. <br>

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Решение</p>
                        <p class="text">Запустили рекламную кампанию на аудитории взаимодействия со страницей Visotsky
                            Consulting (тёплая аудитория, знакомая с брендом). Запустили новогоднюю акцию со скидкой 15% на
                            участие в мероприятии.

                        </p>
                    </div>
                    <div class="item">
                        <p class="title">Выводы</p>
                        <p class="text">Чем лучше обучена индивидуально настроенная конверсия и чем больше целевых заявок
                            на нее получено, тем лучше машина Facebook оптимизируется под получение заявок и тем выше их
                            качество.
                        </p>
                    </div>
                </div>
                <div class="case-numbers">
                    <h3>результат</h3>
                    <p><span class="number">550</span><span class="text">ЦЕЛЕВЫХ ЗАЯВОк</span></p>
                    <p><span class="number">$10,80</span><span class="text">СРЕДНЯЯ СТОИМОСТЬ ЗАЯВКИ</span></p>
                    <p><span class="number">94</span><span class="text">ДНя ДЛИТЕЛЬНОСТЬ КАМПАНИИ</span></p>
                </div>
            </div>
            <div class="modal-footer">
                <h3>Вам нужен системный
                    рентабельный трафик в бизнес?
                </h3>
                <form data-toggle="validator"  action="send.php"  method="POST">
                    <div class="form-group input-field has-feedback">
                        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
                        <label for="pay-form-name">Имя</label>
                    </div>
                    <div class="input-field">
                        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
                        <label for="pay-form-phone" class="active-pay">Телефон</label>
                    </div>
                    <div class="form-group input-field has-feedback">
                        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
                        <label for="pay-form-phone">Email</label>
                    </div>
                    <input type="hidden" name="caselid" value="с кейса Visotsky Consulting">
                    <button type="submit">Обсудить задачу</button>
                </form>
            </div>
        </div>
    </div>
</div>