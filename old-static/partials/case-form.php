<form data-toggle="validator"  action="send.php"  method="POST">
    <div class="form-group input-field has-feedback">
        <input type="text" name="fields[name]" id="pay-form-name" class="name input-pay" data-minlength="2" required >
        <label for="pay-form-name">Имя</label>
    </div>
    <div class="input-field">
        <input type="tel" name="fields[phone]" class="phone input-pay" id="pay-form-phone" data-minlength="6" maxlength="13" required>
        <label for="pay-form-phone" class="active-pay">Телефон</label>
    </div>
    <div class="form-group input-field has-feedback">
        <input type="email" name="fields[email]" class="email input-pay" id="pay-form-email" required>
        <label for="pay-form-phone">Email</label>
    </div>
    <input type="hidden" name="caselid" value="с кейса">
    <button type="submit">Обсудить задачу</button>
</form>