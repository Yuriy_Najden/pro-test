
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#e8f3fe">

	<title>SMMSTUDIO — Увеличиваем продажи с помощью рекламы в Facebook и Instagram</title>
    <meta name="description" content="Увеличиваем продажи с помощью рекламы в Facebook и Instagram — основное направление компании SMMSTUDIO."/>

    <meta property="og:title" content="SMMSTUDIO" />
	<meta property="og:description" content="Увеличиваем продажи с помощью рекламы в Facebook и Instagram" />
	<meta property="og:type" content="video.movie" />
	<meta property="og:url" content="http://smmstudio.com.ua" />
	<meta property="og:image" content="http://smmstudio.com.ua/img/og.png" />

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
    
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5RSJG8L');</script>
<!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '182480625611556'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=182480625611556&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
<div class="wrapper">
    <header id="gotop">
        <div class="wrap">
            <div class="logo-phon">
                <div class="logo-block">
                    <a href="#" class="logo"><img src="img/logo.svg" alt="">
                        <p>SMMSTUDIO</p>
                    </a>
                    <p class="signature">цифровое маркетинговое агентство</p>
                </div>
                <div class="phon">
                    <p><img src="img/voip.svg" alt=""><span>0 800 754 754</span></p>
                    <a href="#" class="open-phone-list"><img src="img/arrowsdown.svg" alt=""></a>
                </div>
                <div class="phone-list">
                    <p class="phon"><img src="img/voip.svg" alt="" ></p>
                    <a href="#" class="close"><img src="img/arrows.svg" alt=""></a>
                    <ul>
                        <li><img src="img/ua.svg" alt=""><a href="tel:0800754754">0 800 754 754</a></li>
                        <li><img src="img/ru.svg" alt=""><a href="tel:74993227910">7 499 322 79 10</a></li>
                        <li><img src="img/kz.svg" alt=""><a href="tel:77172696230">77 1726 96 230</a></li>
                        <li><img src="img/gb.svg" alt=""><a href="tel:448000698473">44 800 069 84 73</a></li>
                        <li><p class="schedule">Пн-пт, 9:00 — 18:00 по Киеву</p></li>
                    </ul>
                </div>
            </div>
            <div class="content">
                <div class="text">
                    <h1 class="slogan">Увеличиваем продажи с помощью рекламы в Facebook и Instagram</h1>
                    <h2>Первые заявки через 12 часов после старта работы или сделаем <br> свою работу бесплатно</h2>
                    <a href="#contact-form" class="button">Обсудить задачу</a>
                    <a href="#case" class="rm">Посмотреть кейсы</a></div>
                <div class="examples-advertising">
                    <div class="ad-slid">

                        <div class="phon-img">
                            <img src="img/portrait_light.svg" alt="">
                            <div class="video-block" id="header-video-slider">
                                <div class="slide">
                                    <video loop muted="" preload="auto" poster="img/preloader.gif" class="h-video">
                                        <source src="video/video5.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                                        <source src="video/video5.ogv" type='video/ogg; codecs="theora, vorbis"'>
                                        <source src="video/video5.webm" type='video/webm; codecs="vp8, vorbis"'> </video>
                                </div>
                                <div class="slide">
                                    <video loop muted="" preload="auto" poster="img/preloader.gif" class="h-video">
                                        <source src="video/video3.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                                        <source src="video/video3.ogv" type='video/ogg; codecs="theora, vorbis"'>
                                        <source src="video/video3.webm" type='video/webm; codecs="vp8, vorbis"'>
                                    </video>
                                </div>
                                <div class="slide">
                                    <video loop muted="" preload="auto" poster="img/preloader.gif" class="h-video">
                                        <source src="video/video4.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                                        <source src="video/video4.ogv" type='video/ogg; codecs="theora, vorbis"'>
                                        <source src="video/video4.webm" type='video/webm; codecs="vp8, vorbis"'>
                                    </video>
                                </div>
                                <div class="slide">
                                    <video loop muted="" preload="auto" poster="img/preloader.gif" class="h-video">
                                        <source src="video/video2.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                                        <source src="video/video2.ogv" type='video/ogg; codecs="theora, vorbis"'>
                                        <source src="video/video2.webm" type='video/webm; codecs="vp8, vorbis"'>
                                    </video>
                                </div>
                            </div>
                            <div class="arrow"></div>
                        </div>
                    </div>
                    <div class="applications">
                        <h3>Заявки с сайта</h3>
                        <p><span>0</span></p>
                        <div class="lids">
                            <div class="hero__title" id="animatedHeading">
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка на курс</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Андрей</p>
                                    <div class="dot"></div>
                                </div>
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка на курс</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Евгений</p>
                                    <div class="dot"></div>
                                </div>
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка на курс</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Виталий</p>
                                    <div class="dot"></div>
                                </div>
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка на курс</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Михаил</p>
                                    <div class="dot"></div>
                                </div>
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка на курс</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Сергей</p>
                                    <div class="dot"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="history-success" id="case">
        <div class="slide">
            <h2 class="block-title">Истории успеха</h2>
            <div class="wrap">
                <div class="img"> <img src="pic/karp.png" alt=""></div>
                <div class="text">
                    <div class="slider-title">
                        <p class="data">Январь-май 2017</p>
                        <h2>Серия Вебинаров с <span>Дмитрием Карпачевым</span></h2></div>
                    <h3>сертифицированный тренер НЛП, гештальт-терапевт, коуч, бизнес-тренер, также известный как телеведущий психологических шоу на телеканале СТБ.</h3>
                    <div class="work-result">
                        <ul class="our-numbers-sl3">
                            <li>
                                <p><span class="up-numbers-sl3-1">150</span>k</p> <span class="litle">лидов на программу</span></li>
                            <li>
                                <p>$ <span class="up-numbers-sl3-2">0</span>,<span class="up-numbers-sl3-3">39</span></p> <span class="litle">средняя стоимость заявки</span></li>
                            <li>
                                <p class="up-numbers-sl3-4">70</p> <span class="litle">дней длительность кампании</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <h2 class="block-title">Истории успеха</h2>
            <div class="wrap">
                <div class="img"> <img src="pic/krypkin.png" alt=""></div>
                <div class="text">
                    <div class="slider-title">
                        <p class="data">Июнь 2017 </p>
                        <h2>Бесплатный вебинар <span>Бюро продаж Андрея Крупкина</span></h2>
                    </div>
                    <h3>«Как создать прибыльный отдел продаж в Digital эпоху.» Для владельцев и руководителей, для руководителей отдела продаж, для бойцов из отдела продаж.</h3>
                    <div class="work-result">
                        <ul class="our-numbers-sl5">
                            <li>
                                <p class="up-numbers-sl5-1">367</p> <span class="litle">лидов на программу</span></li>
                            <li>
                                <p>$ <span class="up-numbers-sl5-2">1</span>,<span class="up-numbers-sl5-3">97</span></p> <span class="litle">средняя стоимость заявки</span></li>
                            <li>
                                <p class="up-numbers-sl5-4">7</p> <span class="litle">дней длительность кампании</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <h2 class="block-title">Истории успеха</h2>
            <div class="wrap">
                <div class="img"> <img src="pic/dash.png" alt=""></div>
                <div class="text">
                    <div class="slider-title">
                        <p class="data">Январь-февраль 2017</p>
                        <h2>Заявки на мастер-класс <span>Михаила Дашкиева в Киеве</span></h2></div>
                    <h3>Сооснователь «Бизнес Молодости» — самого крупного в СНГ сообщества предпринимателей с более 2 000 000 участников</h3>
                    <div class="work-result">
                        <p>Результаты работы за 2 месяца:</p>
                        <ul class="our-numbers-sl1">
                            <li>
                                <p class="up-numbers-sl1-1">2000</p> <span class="litle">лидов на программу</span></li>
                            <li>
                                <p>$<span class="up-numbers-sl1-2">1</span>,<span class="up-numbers-sl1-3">5</span></p> <span class="litle">средняя стоимость заявки</span></li>
                            <li>
                                <p class="up-numbers-sl1-4">4000<span class="litle">грн</span></p> <span class="litle">стоимость участия на мероприятии</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <h2 class="block-title">Истории успеха</h2>
            <div class="wrap">
                <div class="img"> <img src="pic/glysh.png" alt=""></div>
                <div class="text">
                    <div class="slider-title">
                        <p class="data">Мая 2017</p>
                        <h2>Заявки на мастер-класс <span>Сергея Глущенко «Жажда бренда»</span></h2></div>
                    <h3>Эксперт в теме брендинга. Владелец 4-х бизнесов «Tetra pac», «Фанні», «Sandora», «Шостка».</h3>
                    <div class="work-result">
                        <ul class="our-numbers-sl2">
                            <li>
                                <p class="up-numbers-sl2-1">201</p> <span class="litle">лид на программу</span></li>
                            <li>
                                <p>$<span class="up-numbers-sl2-2">2</span>,<span class="up-numbers-sl2-3">87</span></p> <span class="litle">средняя стоимость заявки</span></li>
                            <li>
                                <p class="up-numbers-sl2-4">12</p> <span class="litle">дней длительность кампании</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <h2 class="block-title">Истории успеха</h2>
            <div class="wrap">
                <div class="img"> <img src="pic/kryp.png" alt=""></div>
                <div class="text">
                    <div class="slider-title">
                        <p class="data">Апрель-май 2017</p>
                        <h2>Конференция по продажам <span >«Новая эра продаж»</span></h2></div>
                    <h3>Спикеры — практики, за плечами которых 17 выстроенных отделов продаж, которые принесли более <span>$890 288</span> прибыли. Спикеры: Алексей Кушнир, Илья рейниш, Андрей Крупкин.</h3>
                    <div class="work-result">
                        <ul class="our-numbers-sl4">
                            <li>
                                <p class="up-numbers-sl4-1">365</p> <span class="litle">лидов на программу</span></li>
                            <li>
                                <p>$ <span class="up-numbers-sl4-2">2</span>,<span class="up-numbers-sl4-3">87</span></p> <span class="litle">средняя стоимость заявки</span></li>
                            <li>
                                <p class="up-numbers-sl4-4">40</p> <span class="litle">дней длительность кампании</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <h2 class="block-title">Истории успеха</h2>
            <div class="wrap">
                <div class="img"> <img src="pic/kvartal.png" alt=""></div>
                <div class="text">
                    <div class="slider-title">
                        <p class="data">Июнь 2017 </p>
                        <h2><span>Шоу-бизнес.</span> Как добиться успеха без денег и связей</h2>
                    </div>
                    <h3>Беспрецедентный практический мастер-класс о том, как построить карьеру в шоу-бизнесе. Спикеры: Макс Ткаченко, Юрий Фалёса, Тимофей Нагорный</h3>
                    <div class="work-result">
                        <ul class="our-numbers-sl6">
                            <li>
                                <p class="up-numbers-sl6-1">245</p> <span class="litle">лидов на мастер - класс</span></li>
                            <li>
                                <p>$ <span class="up-numbers-sl6-2">3</span>,<span>0</span><span class="up-numbers-sl6-3">2</span></p> <span class="litle">средняя стоимость заявки</span></li>
                            <li>
                                <p class="up-numbers-sl6-4">3</p> <span class="litle">дней длительность кампании</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="expenses">
        <h2><span>$</span><span id="odometer" class="odometer">0</span></h2>
        <p>потрачено в Facebook и Instagram
            <br> за прошлый год *</p> <a href="#" class="open-popup">* Скрины рекламных кабинетов</a> </section>
    <section class="our-approach">
        <h2 class="block-title">Наш подход</h2>
        <div class="content">
            <div class="step step1">
                <div class="text">
                    <p class="step-number"> 01</p>
                    <h3>Детально изучаем продукт и целевую аудиторию. Проводим бриф, «пробуем продукт на вкус».</h3></div>
                <div class="img-content"> <img src="img/step1_img.svg" alt="" class="wow slideInUp" data-wow-delay="0.5s" data-wow-duration="2s"></div>
            </div>
            <hr>
            <div class="step">
                <div class="text">
                    <p class="step-number"> 02</p>
                    <h3>Строим трафик-систему на основе вашей маркетинговой воронки.</h3></div>
                <div class="img-content"> <img id="zoom_img" src='img/zoom_img_small.jpg' data-zoom-image="img/zoom_img_big.png" /></div>
            </div>
            <hr>
            <div class="step">
                <div class="text">
                    <p class="step-number"> 03</p>
                    <h3>Прогнозируем стоимость заявки, определяем KPI и бюджет.</h3></div>
                <div class="img-content step-count">
                    <div>
                        <p class="step-count-num1">1000</p>
                        <p class="little">лидов</p>
                    </div>
                    <div>
                        <p>$<span class="step-count-num2">2</span></p>
                        <p class="little">средняя стоимость заявки</p>
                    </div>
                    <div>
                        <p class="step-count-num3">20</p>
                        <p class="little">дней</p>
                    </div>
                </div>
            </div>

            <div class="step">
                <div class="container">
                    <div class="text">
                        <p class="step-number"> 04</p>
                        <h3>Разрабатываем рекламные сообщения и дизайн объявлений.</h3>
                    </div>
                    <div class="img-content">
                        <div class="column">
                            <img src="pic/kvartal-ad.png" alt="" class="wow slideInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                            <img src="pic/nobles-ad.png" alt="" class="wow slideInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                        </div>
                        <div class="column">
                            <img src="pic/art-ditt-ad.png" alt="" class="wow slideInUp" data-wow-duration="1s" data-wow-delay="1s">
                            <img src="pic/finansist-ad.png" alt="" class="wow slideInUp" data-wow-duration="1s" data-wow-delay="1s">
                        </div>
                    </div>

                    <div class="img-content-hidden-block">
                        <div class="hidden-conteiner">
                            <div class="column">
                                <img src="pic/karpa-ad.jpg" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                            </div>
                            <div class="column top">
                                <img src="pic/visot-ad.jpg" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <p>больше примеров</p>
                        <a href="#"><img src="img/more.svg" alt=""></a>
                    </div>
                </div>
            </div>

            <div class="step ">
                <div class="text">
                    <p class="step-number"> 05</p>
                    <h3>Техническая подготовка: настройка пикселей, сквозной аналитики.</h3></div>
                <div class="img-content"> <img src="img/step5_img.jpg" alt=""></div>
            </div>
            <hr>
            <div class="step">
                <div class="text">
                    <p class="step-number"> 06</p>
                    <h3>Запускаем кампанию, анализируем и оптимизируем.</h3></div>
                <div class="img-content"> <img src="img/step6_img.png" alt=""></div>
            </div>
        </div>
    </section>
    <section class="we-manage">
        <div class="wrap">
            <div class="text">
                <h2>Мы управляем тем, что измеряем</h2>
                <p>Каждое утро Вы получаете SMS с результатами за прошлый день по каждому ключевому показателю.</p> <a href="#contact-form" class="button">Обсудить задачу</a></div>
            <div class="img-masseg">
                <div class="phon-screen">
                    <div class="phon-top-bar"></div>
                    <p>sms</p>
                    <p>Сегодня 9:01 AM</p>
                    <div class="masseger" id="iphonMassege">
                        <div class="messeg wow slideInUp" data-wow-delay="1s" data-wow-duration="1s"> <img src="pic/message_right1.svg" alt=""></div>
                        <div class="messeg wow slideInUp" data-wow-delay="0.8s" data-wow-duration="1s"> <img src="pic/message_right2.svg" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="five-facts">
        <h2 class="block-title">5 фактов о нашем клиенте</h2>
        <div class="content">
            <div class="img">
                <iframe src="https://docs.google.com/spreadsheets/d/1neEumHmnEgaA72ZQ5FK1dwUjiXBJSQlu5FvmCDMT1Js/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe><a href="https://docs.google.com/spreadsheets/d/1neEumHmnEgaA72ZQ5FK1dwUjiXBJSQlu5FvmCDMT1Js/edit#gid=0" target="_blank">График загруженности SMMSTUDIO</a></div>
            <div class="text">
                <div class="fact">
                    <div class="shape"></div>
                    <p>Он любит и верит в свой продукт</p>
                </div>
                <div class="fact">
                    <div class="shape"></div>
                    <p>Он основатель, руководитель, маркетолог или продукт-менеджер компании</p>
                </div>
                <div class="fact">
                    <div class="shape"></div>
                    <p>Один запуск кампании расcчитан минимум на 2000 заявок в проект</p>
                </div>
                <div class="fact">
                    <div class="shape"></div>
                    <p>Он разделяет тезис: «Будущее за компаниями, которые развиваются в направлении своей ключевой компетенции, а все остальные процессы отдают на аутсорсинг»</p>
                </div>
                <div class="fact">
                    <div class="shape"></div>
                    <p>Он читал «Трактат об искусстве войны» Сунь-цзы (этот пункт конечно же шутка)</p>
                </div>
            </div>
        </div>
    </section>
    <section class="doing-reject">
        <div class="wrap">
            <h2 class="block-title">Что мы делаем... и от чего отказываемся</h2>
            <div class="content">
                <ul>
                    <li>Проводим аудит рекламных кампаний</li>
                    <li>Готовим запуски продуктов с нуля</li>
                    <li>Настраиваем сквозную аналитику</li>
                    <li>Специализируемся на масштабном трафике</li>
                    <li>Предоставляем услуги консультаций и консалтинг в SMM</li>
                    <li>Считаем стоимость каждого клиента</li>
                </ul>
                <ul>
                    <li>Оптимизация сообществ</li>
                    <li>Разработка контент-плана</li>
                    <li>Организация розыгрышей и акций</li>
                    <li>Накручивание ботов и лайков</li>
                    <li>Проекты из сфер: интим, табак и алкоголь, МЛМ, Форекс и т.д.</li>
                    <li>Устранение негатива</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="about">
        <div class="wrap">
            <h2 class="block-title">Об SMMSTUDIO</h2>
            <div class="content">
                <div class="img"> <img src="pic/fedorov2.png" alt=""></div>
                <div class="text">
                    <p>В 2013 году мы создали цифровое маркетинговое агентство <span>SMMSTUDIO</span>.</p>
                    <p>В нашей компании работают <span>13 профессионалов</span>, а география клиентов — <span>более 10 стран</span>.</p>
                    <p>Сегодня нам доверяют серьезные бренды, мы любим то, что делаем, и говорим спасибо Вам за интерес к нашему продукту!</p>
                    <p><span>Вы на правильном пути!</span></p>
                    <div class="founder">
                        <h3>Михаил Фёдоров</h3>
                        <a href="https://www.facebook.com/fedorov1991?fref=ts" target="_blank"></a>
                        <p>Основатель компании SMMSTUDIO</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="partners">
        <div class="wrap">
            <div class="content">
                <div class="partner"><img src="pic/logo-v.svg" alt=""></div>
                <div class="partner"><img src="pic/kvartal-logo.png" alt=""></div>
                <div class="partner"><img src="pic/ym.png" alt=""></div>
                <div class="partner"><img src="pic/krypkin-logo.svg" alt=""></div>
                <div class="partner"><img src="pic/karpachov-logo.png" alt=""></div>
                <div class="partner"><img src="pic/multiplex.jpg" alt=""></div>
                <div class="partner"><img src="pic/bayadera.png" alt=""></div>
                <div class="partner"><img src="pic/kibit.jpg" alt=""></div>
                <div class="partner"><img src="pic/cost.png" alt=""></div>
                <div class="partner"><img src="pic/kyivatar.png" alt=""></div>
                <div class="partner"><img src="pic/ashan.png" alt=""></div>
                <div class="partner"><img src="pic/pokypon.png" alt=""></div>
                <div class="partner"><img src="pic/nissan.png" alt=""></div>
                <div class="partner"><img src="pic/toyota.png" alt=""></div>
                <div class="partner"><img src="pic/superdeaf.png" alt=""></div>
                <div class="partner"><img src="pic/PM.png" alt=""></div>
                <div class="partner"><img src="pic/marketing.png" alt=""></div>
                <div class="partner"><img src="pic/nobles.png" alt=""></div>
                <div class="partner"><img src="pic/stb.png" alt=""></div>
                <div class="partner"><img src="pic/JP.png" alt=""></div>
                <div class="partner"><img src="pic/gemeini.png" alt=""></div>
                <div class="partner"><img src="pic/bakshit.png" alt=""></div>
                <div class="partner"><img src="pic/goit.png" alt=""></div>
                <div class="partner"><img src="pic/mers.png" alt=""></div>
                <div class="partner"><img src="pic/proryv-logo.svg" alt=""></div>
            </div>
            <h3>Бренды, которые воспользовались нашими услугами</h3>
        </div>
    </section>
    <section id="contact-form" class="contact-form">
        <div class="wrap">
            <h2>Вам нужен системный рентабельный трафик в бизнес?</h2>
            <div class="form-body">
                <?php include('partials/subscribe-form.php'); ?>
            </div>
        </div>
    </section>
    <?php
        include ('partials/messeger.php');
    ?>
    <?php
        include ('partials/contacts.php');
    ?>
    <?php
        include ('partials/privacy-policy.php');
    ?>
    <?php
        include ('partials/floating-messegers.php');
    ?>
    <div id="popup-content" class="popup-content">
        <a href="#" class="close-popup"></a>
        <div class="galery fotorama" data-allowfullscreen="true">
            <img src="pic/cabinet1.png" alt="">
            <img src="pic/cabinet2.png" alt="">
        </div>
    </div>
    <div id="popup-galery" class="popup"></div>   
</div>
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/odometer-theme-default.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="css/intlTelInput.css">
<link rel="stylesheet" href="css/fotorama.css">
<link rel="stylesheet" href="css/animate.css">
<link type="text/css" rel="stylesheet" href="css/style.css" />

<!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
<!--[if lt IE 7]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script> <![endif]-->
<!--[if lt IE 8]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script> <![endif]-->
<!--[if lt IE 9]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script> <![endif]-->
<!--[if lt IE 9]> <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
<script src="js/jquery.js"></script>
<script src="js/intlTelInput.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/jquery.viewportchecker.min.js"></script>
<script src="js/odometer.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="js/fotorama.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/myscripts.js"></script>
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
</body>

</html>