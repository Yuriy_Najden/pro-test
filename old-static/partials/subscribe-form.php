<form role="form" data-toggle="validator"  action="send.php"  method="POST">

    <div class="form-group input-field has-feedback">
        <input id="input_name" name="fields[name]" class="name form-input" type="text" data-minlength="2" data-required-error="Введите своё имя (не менее 2 символов)" required>
        <label for="input_name">Имя</label>
    </div>

    <div class=" input-field">
        <input id="input_phon" name="fields[phone]" class="phone phone-mask-input form-input" type="tel" data-minlength="6" maxlength="13" placeholder="" required>
        <label for="input_phon" class="active">Телефон</label>
    </div>

    <div class="form-group input-field has-feedback">
        <input id="input_mail" name="fields[email]" class="email form-input" type="email" required>
        <label for="input_mail">Email</label>
    </div>

    <div class="input-field textarea-filds">
        <textarea id="textarea1" name="message" class="message form-input" ></textarea>
        <label for="textarea1">"Задача: наш — сайт smmstudio.com.ua. Мы хотим получать больше заявок от потенциальных
            клиентов. Сейчас работают разные рекламные каналы. Хотим подключить Facebook и Instagram"
        </label>
    </div>

    <div class="runner">
        <p>
            <label for="amount">Требуемое количество лидов</label>
            <input type="text" name="leads" class="leads-count" id="amount" value="" readonly>
        </p>
        <div id="slider"></div>
    </div>


    <h3>Ответим в течение рабочего дня</h3>

    <button class="button" type="submit" id="send" >Отправить <img src="img/button_icon.svg" alt=""></button>
</form>

