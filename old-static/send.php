<?php
session_start();

require_once('vendor/autoload.php');

require_once('classes/AmoCrm.php');

use PHPMailer\PHPMailer\PHPMailer;

function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name = clearData($_POST['fields']['name']);
$email = clearData($_POST['fields']['email']);
$phone = clearData($_POST['fields']['phone']);
$message = clearData($_POST['message']);
$leadsCount = clearData($_POST['leads']);

$caselid = clearData($_POST['caselid']);

$adWords = clearData($_SESSION['gclid']);

$utmSource = clearData($_SESSION['utm_source']);
$utmMedium = clearData($_SESSION['utm_medium']);
$utmCampaign = clearData($_SESSION['utm_campaign']);
$utmTerm = clearData($_SESSION['utm_term']);
$utmContent = clearData($_SESSION['utm_content']);


if(!empty($name) && !empty($email) && !empty($phone)) {

    // Store in AmoCrm.
    $amoCrm = new AmoCrm();

    $lead = $amoCrm->storeLead(15516604, $message, $leadsCount, $adWords, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $_COOKIE['roistat_visit'], $caselid);
    $leadId = (int)$lead['response']['leads']['add'][0]['id'];

    $contact = $amoCrm->storeContact($name, $email, $phone, $leadId);

    //mailerLite

	$groupsApi = (new \MailerLiteApi\MailerLite('93c97a38e6f63d676d4f49f03fa3ef77'))->groups();

	$groupId = 6944635;

	$subscriber = [
		'email' => $email,
		'name' => $name,
		'fields' => [
			'phone' => $phone
		]
	];

	$addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber); // returns added subscriber

    //

    $mail = new PHPMailer();

    try {

        //Server settings
        $mail->isSMTP();
        $mail->Host = 'mail.adm.tools';
        $mail->SMTPAuth = true;
        $mail->Username = 'amo@smmstudio.com.ua';
        $mail->Password = 'D27rTBaF4p4o';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 25;
        $mail->CharSet = 'UTF-8';

        //Recipients
        $mail->setFrom('amo@smmstudio.com.ua', 'info');
        $mail->addAddress('amo@smmstudio.com.ua', 'info');

        //Content
        $mail->isHTML(true);
        $mail->Subject = 'Новая заявка';
        $mail->Body = "<p>Имя: $name</p><p>Телефон: $phone</p><p>Email: $email</p><p>Задача: $message</p><p>utmSource: $utmSource</p><p>utmMedium: $utmMedium</p><p>utmCampaign: $utmCampaign</p><p>utmTerm: $utmTerm</p><p>utmContent: $utmContent</p>";

        $mail->send();

    } catch (Exception $e) {
        echo 'Message could not be sent.';

        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }

	header('Location: thx');
}


