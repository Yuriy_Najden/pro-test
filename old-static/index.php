<?php

session_start();

if (!empty($_GET['gclid'])) {

    $_SESSION['gclid'] = $_GET['gclid'];
    $_SESSION['utm_source'] = 'adwords';
}

if (!empty($_GET['utm_source'])) {

    $_SESSION['utm_source'] = $_GET['utm_source'];
}

if (!empty($_GET['utm_medium'])) {

    $_SESSION['utm_medium'] = $_GET['utm_medium'];
}

if (!empty($_GET['utm_campaign'])) {

    $_SESSION['utm_campaign'] = $_GET['utm_campaign'];
}

if (!empty($_GET['utm_term'])) {

    $_SESSION['utm_term'] = $_GET['utm_term'];
}

if (!empty($_GET['utm_content'])) {

    $_SESSION['utm_content'] = $_GET['utm_content'];
}


?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   

	<title>SMMSTUDIO — Увеличиваем продажи с помощью рекламы в Facebook и Instagram</title>
    <meta name="description" content="Увеличиваем продажи с помощью рекламы в Facebook и Instagram — основное направление компании SMMSTUDIO."/>

    <meta property="og:title" content="Увеличиваем продажи с помощью рекламы в Facebook и Instagram" />
	<meta property="og:description" content="Первые заявки через 12 часов после старта работы" />
	<meta property="og:type" content="video.movie" />
	<meta property="og:url" content="http://smmstudio.com.ua" />
	<meta property="og:image" content="http://smmstudio.com.ua/img/og.png" />

<meta name="theme-color" content="#007fff">
    
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5RSJG8L');</script>
<!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '182480625611556'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=182480625611556&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '142692132975924'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=142692132975924&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '480042189046599'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=480042189046599&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '258060081377526'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=258060081377526&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
<div class="wrapper">

    <section class="big-baner">
        <a href="#" class="closer-baner"><img src="pic/close-rez.svg" alt=""></a>
        <a href="https://smmstudio.com.ua/webinar/?utm_source=smmstudiocomua&utm_medium=banner" target="_blank"><img src="pic/banner.jpg"  alt=""></a>
    </section>
    <section class="webinar-baner">
        <div class="wrap">
            <img src="pic/close-button-w.svg" alt="">
            <p>5 сентября, 13:00. Бесплатный вебинар «Как применить рекламу в Facebook и Instagram в своём бизнесе для
                увеличения продаж»
            </p>
            <a href="https://smmstudio.com.ua/webinar/?utm_source=smmstudiocomua&utm_medium=notification" target="_blank">Регистрация</a>
        </div>
    </section>


    <div class="youtoob-baner" id="start-youtoobe" data-video-id="_nNJNOV5UEs">
        <div class="pley">
            <a href="#" >
                <img src="img/youtoob-start.svg" alt="">
            </a>
        </div>
        <div class="text">
            <p>Как мы работаем</p>
        </div>
    </div>

    <div class="video-page">
        <div class="video-window">
            <img src="img/video-closer.svg" alt="" class="close-video">
            <div class="video">
                <div id="player"></div>
            </div>
        </div>
    </div>
    <header id="gotop">
        <div class="wrap">
            <div class="logo-phon">
                <div class="logo-block">
                    <a href="#" class="logo"><img src="https://smmstudio.com.ua/img/logo.svg" alt="">
                        <p>SMMSTUDIO</p>
                    </a>
                    <p class="signature">цифровое маркетинговое агентство</p>
                </div>
                <div class="inner-sites-menu">
                    <nav>
                        <ul class="t-menu">
                            <!--<li class="open-sub-in"><a href="#" class="open-sub" id="open-sub2">Услуги</a>
                                <ul class="sub-menu m-body" id="sub-menu2">
                                    <li><a href="#case" class="scroll-to">Реклама в Facebook <br>и Instagram</a></li>
                                    <li><a href="https://motion.smmstudio.com.ua/" target="_blank">Motion Design</a></li>
                                    <li><a href="https://automation.smmstudio.com.ua/" target="_blank">Автоматизация бизнеса <br>и продаж</a></li>
                                </ul>
                            </li>-->
                            <li><a href="#case">Кейсы</a></li>
                            <li class="open-sub-in"><a href="#" class="open-sub" id="open-sub1">Карьера</a>
                                <ul class="sub-menu m-body" id="sub-menu1">
                                    <li><a href="https://career.smmstudio.com.ua/" target="_blank">Факультет Facebook рекламы</a></li>
                                    <li><a href="https://sales.smmstudio.com.ua/" target="_blank">Факультет продаж</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="phon">
                    <p><img src="img/voip.svg" alt=""><span>0 800 754 754</span></p>
                    <a href="#" class="open-phone-list"><img src="img/arrowsdown.svg" alt=""></a>
                </div>
                <div class="phone-list">
                    <p class="phon"><img src="img/voip.svg" alt="" ></p>
                    <a href="#" class="close"><img src="img/arrows.svg" alt=""></a>
                    <ul>
                        <li><img src="img/ua.svg" alt=""><a href="tel:0800754754">0 800 754 754</a></li>
                        <li><img src="img/ru.svg" alt=""><a href="tel:78123095790">7 812 309 57 90</a></li>
                        <!--
                        <li><img src="img/kz.svg" alt=""><a href="tel:77172696230">77 1726 96 230</a></li>
                        -->
                        <li><img src="img/gb.svg" alt=""><a href="tel:448000698473">44 800 069 84 73</a></li>
                        <li><p class="schedule">Пн-пт, 9:00 — 18:00 по Киеву</p></li>
                    </ul>
                </div>
            </div>
            <div class="content">
                <div class="text">
                    <h1 class="slogan">Увеличиваем продажи с помощью рекламы в Facebook и Instagram</h1>
                    <h2>Первые заявки через 12 часов после старта работы</h2>
                    <!--<a href="#new" class="new scroll-to">Автоматизация бизнеса и продаж</a>-->
                    <a href="#contact-form" class="button scroll-to">Обсудить задачу</a>
                    <a href="#case" class="rm scroll-to">Посмотреть кейсы</a>
                    <div class="like">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fsmmstudio.com.ua%2F&width=450&layout=standard&action=like&size=small&show_faces=true&share=true&height=80&appId" width="450" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                    </div>
                </div>
                <div class="examples-advertising">
                    <div class="ad-slid">

                        <div class="phon-img">
                            <img src="img/portrait_light.svg" alt="">
                            <div class="video-block" id="header-video-slider">
                                <div class="slide">
                                    <video loop muted="" preload="auto" poster="img/preloader.gif" class="h-video">
                                        <source src="video/video5.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                                        <source src="video/video5.ogv" type='video/ogg; codecs="theora, vorbis"'>
                                        <source src="video/video5.webm" type='video/webm; codecs="vp8, vorbis"'> </video>
                                </div>
                                <!--<div class="slide">
                                    <video loop muted="" preload="auto" poster="img/preloader.gif" class="h-video">
                                        <source src="video/video3.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                                        <source src="video/video3.ogv" type='video/ogg; codecs="theora, vorbis"'>
                                        <source src="video/video3.webm" type='video/webm; codecs="vp8, vorbis"'>
                                    </video>
                                </div>-->
                                <div class="slide">
                                    <video loop muted="" preload="auto" poster="img/preloader.gif" class="h-video">
                                        <source src="video/video4.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                                        <source src="video/video4.ogv" type='video/ogg; codecs="theora, vorbis"'>
                                        <source src="video/video4.webm" type='video/webm; codecs="vp8, vorbis"'>
                                    </video>
                                </div>
                                <div class="slide">
                                    <video loop muted="" preload="auto" poster="img/preloader.gif" class="h-video">
                                        <source src="video/video2.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                                        <source src="video/video2.ogv" type='video/ogg; codecs="theora, vorbis"'>
                                        <source src="video/video2.webm" type='video/webm; codecs="vp8, vorbis"'>
                                    </video>
                                </div>
                            </div>
                            <div class="arrow"></div>
                        </div>
                    </div>
                    <div class="applications">
                        <h3>Заявки с сайта</h3>
                        <p><span>0</span></p>
                        <div class="lids">
                            <div class="hero__title" id="animatedHeading">
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Андрей</p>
                                    <div class="dot"></div>
                                </div>
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Евгений</p>
                                    <div class="dot"></div>
                                </div>
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Виталий</p>
                                    <div class="dot"></div>
                                </div>
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Михаил</p>
                                    <div class="dot"></div>
                                </div>
                                <div class="app-block">
                                    <div class="title">
                                        <h4>Заявка</h4>
                                        <p class="data">24.05.17</p>
                                    </div>
                                    <p class="name">Сергей</p>
                                    <div class="dot"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="video-about">
        <video src="video/video-block.mp4" controls poster="img/cover.jpg"></video>
    </section>


    <?php include ('partials/case.php'); ?>

    <section class="expenses">
        <h2><span>$</span><span id="odometer" class="odometer">0</span></h2>
        <p>потрачено в Facebook и Instagram
            <br> за прошлый год *</p> <a href="#" class="open-popup">* Скрины рекламных кабинетов</a> </section>
    <section class="our-approach">
        <h2 class="block-title">Наш подход</h2>
        <div class="content">
            <div class="step step1">
                <div class="text">
                    <p class="step-number"> 01</p>
                    <h3>Детально изучаем продукт и целевую аудиторию. Проводим бриф, «пробуем продукт на вкус».</h3></div>
                <div class="img-content"> <img src="img/step1_img.svg" alt="" class="wow slideInUp" data-wow-delay="0.5s" data-wow-duration="2s"></div>
            </div>
            <hr>
            <div class="step">
                <div class="text">
                    <p class="step-number"> 02</p>
                    <h3>Строим трафик-систему на основе вашей маркетинговой воронки.</h3></div>
                <div class="img-content" id="zoom_img">
                    <img src='img/zoom_img_big.jpg' />
                </div>
            </div>
            <hr>
            <div class="step">
                <div class="text">
                    <p class="step-number"> 03</p>
                    <h3>Прогнозируем стоимость заявки, определяем KPI и бюджет.</h3>
                </div>
                <div class="img-content step-count">
                    <img src="img/step3_img2.svg" alt="" class="wow slideInUp" data-wow-delay="0.5s" data-wow-duration="2s">
                </div>
            </div>

            <div class="step">
                <div class="container">
                    <div class="text">
                        <p class="step-number"> 04</p>
                        <h3>Разрабатываем рекламные сообщения и дизайн объявлений.</h3>
                    </div>
                    <div class="img-content">
                        <div class="column">
                            <img src="pic/kvartal-ad.png" alt="" class="wow slideInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                            <img src="pic/7.png" alt="" class="wow slideInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                           <!-- <img src="pic/7.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="0.5s">-->
                        </div>
                        <div class="column" id="up-position">
                            <img src="pic/art-ditt-ad.png" alt="" class="wow slideInUp" data-wow-duration="1s" data-wow-delay="1s">
                            <img src="pic/finansist-ad.png" alt="" class="wow slideInUp" data-wow-duration="1s" data-wow-delay="1s">
                        </div>
                    </div>

                    <div class="img-content-hidden-block">
                        <div class="hidden-conteiner">
                            <div class="column">
                                <img src="pic/karpa-ad.jpg" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                                <img src="pic/1.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                                <img src="pic/3.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                                <img src="pic/5.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                                <img src="pic/8.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                            </div>
                            <div class="column top">
                                <img src="pic/visot-ad.jpg" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                                <img src="pic/2.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                                <img src="pic/4.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                                <img src="pic/6.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                                <img src="pic/9.png" alt="" class="wow slideInUp" data-wow-duration="0.5s" data-wow-offset="300">
                            </div>
                        </div>
                    </div>
                    <div class="button-block button-down-block">
                        <p>больше примеров</p>
                        <a href="#"><img src="img/more.svg" alt=""></a>
                    </div>
                    <div class="button-block button-up-block">
                        <p>Скрыть</p>
                        <a href="#up-position"><img src="img/more.svg" alt=""></a>
                    </div>
                </div>
            </div>

            <div class="step ">
                <div class="text">
                    <p class="step-number"> 05</p>
                    <h3>Техническая подготовка: настройка пикселей, сквозной аналитики.</h3></div>
                <div class="img-content"> <img src="img/step5_img.jpg" alt=""></div>
            </div>
            <hr>
            <div class="step">
                <div class="text">
                    <p class="step-number"> 06</p>
                    <h3>Запускаем кампанию, анализируем и оптимизируем.</h3></div>
                <div class="img-content"> <img src="img/step6_img.png" alt=""></div>
            </div>
        </div>
    </section>

    <section class="five-facts">
        <div class="wrap">
            <h2 class="block-title">5 фактов о нашем клиенте</h2>
            <div class="content">
                <div class="fact">
                    <div class="shape"></div>
                    <p>Он любит и верит в свой продукт</p>
                </div>
                <div class="fact">
                    <div class="shape"></div>
                    <p>Он основатель, руководитель, маркетолог или продукт-менеджер компании</p>
                </div>
                <div class="fact">
                    <div class="shape"></div>
                    <p>Один запуск кампании расcчитан минимум на 100 заявок в проект</p>
                </div>
                <div class="fact">
                    <div class="shape"></div>
                    <p>Он разделяет тезис: «Будущее за компаниями, которые развиваются в направлении своей ключевой компетенции, а все остальные процессы отдают на аутсорсинг»</p>
                </div>
                <div class="fact">
                    <div class="shape"></div>
                    <p>Он читал «Трактат об искусстве войны» Сунь-цзы (этот пункт конечно же шутка)</p>
                </div>
            </div>
        </div>
    </section>

    <section class="doing-reject">
        <div class="wrap">
            <h2 class="block-title">Что мы делаем... и от чего отказываемся</h2>
            <div class="content">
                <ul>
                    <li>Проводим аудит рекламных кампаний</li>
                    <li>Готовим запуски продуктов с нуля</li>
                    <li>Настраиваем сквозную аналитику</li>
                    <li>Специализируемся на масштабном трафике</li>
                    <li>Предоставляем услуги консультаций и консалтинг в SMM</li>
                    <li>Считаем стоимость каждого клиента</li>
                </ul>
                <ul>
                    <li>Оптимизация сообществ</li>
                    <li>Разработка контент-плана</li>
                    <li>Организация розыгрышей и акций</li>
                    <li>Накручивание ботов и лайков</li>
                    <li>Проекты из сфер: интим, табак и алкоголь, МЛМ, Форекс и т.д.</li>
                    <li>Устранение негатива</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="about">
        <div class="wrap">
            <h2 class="block-title">Команда SMMSTUDIO</h2>
            <div class="content">
                <div class="associate">
                    <img src="pic/worker1.jpg" alt="">
                    <p>Михаил</p>
                </div>
                <div class="associate">
                    <img src="pic/worker2.jpg" alt="">
                    <p>Михаил</p>
                </div>
                <div class="associate">
                    <img src="pic/worker3.jpg" alt="">
                    <p>Олег</p>
                </div>
                <div class="associate">
                    <img src="pic/worker4.jpg" alt="">
                    <p>Андрей</p>
                </div>
                <div class="associate">
                    <img src="pic/worker5.jpg" alt="">
                    <p>Егор</p>
                </div>
                <div class="associate">
                    <img src="pic/worker6.jpg" alt="">
                    <p>Александер</p>
                </div>
                <div class="associate">
                    <img src="pic/worker7.jpg" alt="">
                    <p>Яна</p>
                </div>
                <div class="associate">
                    <img src="pic/worker8.jpg" alt="">
                    <p>Александер</p>
                </div>
                <div class="associate">
                    <img src="pic/worker9.jpg" alt="">
                    <p>Алёна</p>
                </div>
                <div class="associate">
                    <img src="pic/worker10.jpg" alt="">
                    <p>Антон</p>
                </div>
                <div class="associate">
                    <img src="pic/worker11.jpg" alt="">
                    <p>Юрий</p>
                </div>
                <div class="associate">
                    <img src="pic/worker12.jpg" alt="">
                    <p>Анастасия</p>
                </div>
                <div class="associate">
                    <img src="pic/worker13.jpg" alt="">
                    <p>Константин</p>
                </div>
                <div class="associate">
                    <img src="pic/worker14.jpg" alt="">
                    <p>Ирина</p>
                </div>
                <div class="associate">
                    <img src="pic/worker15.jpg" alt="">
                    <p>Алексей</p>
                </div>
                <div class="associate rez">
                    <h5>Присоединиться <br>к команде</h5>

                    <img src="pic/plus.svg" alt="" class="plus">
                </div>
            </div>
        </div>
    </section>
    <section class="partners">
        <div class="wrap">
            <div class="content">
                <div class="partner"><img src="pic/logo-v.png" alt=""></div>
                <div class="partner"><img src="pic/kvartal-logo.png" alt=""></div>
                <div class="partner"><img src="pic/ym.png" alt=""></div>
                <div class="partner"><img src="pic/krypkin-logo.svg" alt=""></div>
                <div class="partner"><img src="pic/karpachov-logo.png" alt=""></div>                
                <div class="partner"><img src="pic/bayadera.png" alt=""></div>
                <div class="partner"><img src="pic/kibit.jpg" alt=""></div>
                <div class="partner"><img src="pic/cost.png" alt=""></div>               

                <div class="partner"><img src="pic/pokypon.png" alt=""></div>
                <div class="partner"><img src="pic/nissan.png" alt=""></div>
                <div class="partner"><img src="pic/toyota.png" alt=""></div>
                <div class="partner"><img src="pic/superdeaf.png" alt=""></div>
                <div class="partner"><img src="pic/PM.png" alt=""></div>
                <div class="partner"><img src="pic/marketing.png" alt=""></div>
                <div class="partner"><img src="pic/nobles.png" alt=""></div>                
                <div class="partner"><img src="pic/JP.png" alt=""></div>
                
                <div class="partner"><img src="pic/bakshit.png" alt=""></div>
                <div class="partner"><img src="pic/goit.png" alt=""></div>
                <div class="partner"><img src="pic/mers.png" alt=""></div>
                <div class="partner"><img src="pic/proryv-logo.svg" alt=""></div>
            </div>
            <h3>Бренды, которые воспользовались нашими услугами</h3>
        </div>
    </section>
    <section id="contact-form" class="contact-form">
        <div class="wrap">
            <h2>Вам нужен системный рентабельный трафик в бизнес?</h2>
            <div class="form-body">
                <?php include('partials/subscribe-form.php'); ?>
            </div>
        </div>
    </section>
    <?php
        include ('partials/messeger.php');
    ?>
    <?php
        include ('partials/contacts.php');
    ?>
    <?php
        include ('partials/privacy-policy.php');
    ?>
    <?php
        include ('partials/floating-messegers.php');
    ?>
    <div id="popup-content" class="popup-content">
        <a href="#" class="close-popup"></a>
        <div class="galery fotorama" data-allowfullscreen="true">
            <img src="pic/cabinet1.png" alt="">
            <img src="pic/cabinet2.png" alt="">
        </div>
    </div>
    <div id="popup-galery" class="popup"></div>

    <div class="popup-fone"></div>

    <div class="resume-popup">
        <img src="pic/close-rez.svg" alt="" class="close-rezume">
        <h2>Присоединяйтесь к команде </h2>
        <form role="form" data-toggle="validator" action="rez-send.php" id="rez-form" method="post" enctype="multipart/form-data">
            <div class="form-group input-field has-feedback">
                <input id="rez_name" name="rez_name" class="name form-input" type="text" data-minlength="2" data-required-error="Введите своё имя (не менее 2 символов)" required>
                <label for="rez_name">Имя</label>
            </div>

            <div class=" input-field">
                <input id="rez_phon" name="rez_phone" class="phone phone-mask-input form-input" type="tel" data-minlength="6" maxlength="13" placeholder="" required>
                <label for="rez_phon" class="active">Телефон</label>
            </div>

            <div class="form-group input-field has-feedback">
                <input id="rez_mail" name="rez_email" class="email form-input" type="email" required>
                <label for="rez_mail">Email</label>
            </div>

            <div class="input-field textarea-filds">
                <textarea id="textarea1" name="rez_message" class="message form-input" placeholder="Ваши сильные стороны; почему Вы хотите работать; профессиональную цель на ближайшие 6 месяцев." ></textarea>
            </div>

            <div class="affix">
                <div class="file-upload">
                    <label>
                        <input type="file" name="rez_file" id="rez-file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                        <span>Выберите файл</span>
                    </label>
                </div>
                <input type="text" id="filename" class="filename" disabled>
                <p>Прикрепить резюме (PDF, Word, RTF, до 10 мб)</p>
            </div>

            <button class="button" type="submit" >Отправить <img src="img/button_icon.svg" alt=""></button>
        </form>
    </div>
</div>
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/odometer-theme-default.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="css/intlTelInput.css">
<link rel="stylesheet" href="css/fotorama.css">
<link rel="stylesheet" href="css/animate.css">
<link type="text/css" rel="stylesheet" href="css/style.css" />

<!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
<!--[if lt IE 7]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script> <![endif]-->
<!--[if lt IE 8]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script> <![endif]-->
<!--[if lt IE 9]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script> <![endif]--><!--[if lt IE 9]> <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->

    <script>


        // 4. The API will call this function when the video player is ready.
        function onPlayerReady() {
            player.playVideo();

            //event.target.playVideo();
        }

        // 5. The API calls this function when the player's state changes.
        //    The function indicates that when playing a video (state=1),
        //    the player should play for six seconds and then stop.
        var done = false;
        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.PLAYING && !done) {
                setTimeout(stopVideo, 6000);
                done = true;
            }
        }
        function stopVideo() {

            player.stopVideo();
        }
    </script>
    <script src="https://www.youtube.com/iframe_api"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
<script src="js/intlTelInput.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/jquery.viewportchecker.min.js"></script>
<script src="js/odometer.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/fotorama.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/myscripts.js"></script>
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    
</body>

</html>