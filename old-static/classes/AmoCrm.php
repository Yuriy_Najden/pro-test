<?php

class AmoCrm {
    private $subdomain = 'smmstudioagency';

    private function auth()
    {
        $user = array(
            'USER_LOGIN'    => 'alex@smmstudio.com.ua',
            'USER_HASH'     => '13c4bfdd15669a48193e2b74e93d37d4'
        );

        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/auth.php?type=json';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($user));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);

        curl_close($curl);
    }

    public function __construct()
    {
        $this->auth();
    }

    /**
     * Store lead.
     *
     * @param $statusId
     * @param $message
     * @param $leadsCount
     * @param $adWords
     * @param $utmSource
     * @param $utmMedium
     * @param $utmCampaign
     * @param $utmTerm
     * @param $utmContent    
     * @param $roistat
     * @param $caselid
     * @return object
     */
    public function storeLead($statusId, $message, $leadsCount, $adWords, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $roistat, $caselid)
    {
        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/leads/set';

        $leads['request']['leads']['add'] = array(
            array(
                'name'          => 'Обсудить задачу ' . $caselid,
                'date_create'   => time(),
                'status_id'     => $statusId,
                'custom_fields' => array(
                    array(
                        'id'        => 1956863,
                        'values'    => array(
                            array(
                                'value' => $message
                            )
                        )
                    ),
                    array(
                        'id'        => 1956865,
                        'values'    => array(
                            array(
                                'value' => $leadsCount
                            )
                        )
                    ),
                    array(
                        'id'        => 1958257,
                        'values'    => array(
                            array(
                                'value' => $adWords
                            )
                        )
                    ),
                    array(
                        'id'        => 1958329,
                        'values'    => array(
                            array(
                                'value' => $utmSource
                            )
                        )
                    ),
                    array(
                        'id'        => 1958331,
                        'values'    => array(
                            array(
                                'value' => $utmMedium
                            )
                        )
                    ),
                    array(
                        'id'        => 1958333,
                        'values'    => array(
                            array(
                                'value' => $utmCampaign
                            )
                        )
                    ),
                    array(
                        'id'        => 1958335,
                        'values'    => array(
                            array(
                                'value' => $utmTerm
                            )
                        )
                    ),
                    array(
                        'id'        => 1958337,
                        'values'    => array(
                            array(
                                'value' => $utmContent
                            )
                        )
                    ),            
                    array(
						'id'        => 1958663,
						'values'    => array(
							array(
								'value' => $roistat
							)
						)
					)
                )
            )
        );

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($leads));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $response = json_decode($out, true);

        return $response;
    }

    /**
     * Store contact.
     *
     * @param $name
     * @param $email
     * @param $phone
     * @param $leadId
     */
    public function storeContact($name, $email, $phone, $leadId)
    {
        $link = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/contacts/set';

        $contacts['request']['contacts']['add'] = array(
            array(
                'name'              => $name,
                'last_modified'     => time(),
                'linked_leads_id'   => array(
                    $leadId
                ),
                'custom_fields'     => array(
                    array(
                        // Phones
                        'id'        => 1851983,
                        'values'    => array(
                            array(
                                'value' => $phone,
                                'enum'  => 'MOB'
                            )
                        )
                    ),
                    array(
                        // Emails
                        'id'        => 1851985,
                        'values'    => array(
                            array(
                                'value' => $email,
                                'enum'  => 'PRIV',
                            )
                        )
                    )
                )
            )
        );

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($contacts));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    }
}