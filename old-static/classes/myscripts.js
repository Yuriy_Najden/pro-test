$(function() {

    var player;

    $('#start-youtoobe').click(function(event) {

        var videoId = $(this).data('video-id');


        event.preventDefault();

        player = new YT.Player('player', {
            height: '360',
            width: '640',
            videoId: videoId,
            events: {
                'onReady': function (e) {
                    e.target.playVideo();
                }
            }
        });

        $('.video-page').slideDown(1000);

    });

    $('.video-page, .close-video').click( function (e) {
        e.preventDefault();

        $('.video-page').slideUp(500);

        player.stopVideo();

        player.destroy();
    });

    //gallery popup
    $('a.open-popup').click(function(event) {
        event.preventDefault();
        $('.popup').fadeIn(400, function() {
            $('#popup-content').css('display', 'block');
        });
    });
    $('.popup, .close-popup').click(function(event) {
        event.preventDefault();
        $('#popup-content').css('display', 'none');
        $('.popup').fadeOut(400);
    });

    //rezyme popup
    $('.rez').click(function() {
        $('.popup-fone').fadeIn(400, function() {
            $('.resume-popup').css('display', 'block');
        });
    });
    $('.popup-fone, .close-rezume').click(function() {
        $('.resume-popup').css('display', 'none');
        $('.popup-fone').fadeOut(400);
    });
    //

    $(".file-upload input[type=file]").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $("#filename").val(filename);
    });

    //Slider case
    $('.history-success').slick({
        adaptiveHeight: true,
        dots: true,
        fade: true,
        autoplay: false,
        autoplaySpeed: 2000,
        responsive: [{
            breakpoint: 1160,
            settings: {
                arrows: false
            }
        }]
    });

    setTimeout(function() {
        $('.webinar-baner').slideDown(400);
    }, 120000);

    $('.webinar-baner img').on('click', function () {
        $('.webinar-baner').css('display', 'none');

    });
//
   //$(document).on('mouseleave', function () {
   //    $('.big-baner').slideDown();
   //    $('.popup-fone').show();
//
   //     $(document).off('mouseleave');
   // });
//
   // $('.popup-fone, .closer-baner').click(function(event) {
   //     event.preventDefault();
   //     $('.big-baner').css('display', 'none');
   //     $('.popup-fone').fadeOut(400);
   // });
//

   //Smooth scrolling
   var $page = $('html, body');
   $('a[href*="#"]').click(function() {
       $page.animate({
           scrollTop: $($.attr(this, 'href')).offset().top
       }, 1000);
       return false;
   });
   //



    //Lead amount slider
    $("#slider").slider({
        value: 100,
        min: 100,
        max: 10000,
        step: 10,
        slide: function(event, ui) {
            $("#amount").val(ui.value);
        }
    }).each(function() {
        var opt = $(this).data().uiSlider.options;
        var vals = opt.max - opt.min;
        for (var i = 0; i <= vals; i = i + 1400) {
            var ell = $('<label>' + (i + 100) + '</label>').css('left', (i / vals * 100) + '%');
            $("#slider").append(ell);
        }
        $("#amount").val($("#slider").slider("value"));
    });
    $('#slider').draggable();


    //Slider video in content
    $('#step-slider').on('init', function(ev, el) {
        $('.b-video').each(function() {
            this.play();
        });
    });

    $('.step .button-down-block').on('click', function(e){
        e.preventDefault();
        $('.step .img-content-hidden-block').show(500);
        $('.step .button-down-block').hide();
        $('.step .button-up-block').show();
    });

    var upToscroll = $('#up-position').offset().top;

    $('.step .button-up-block').on('click', function(e){
        e.preventDefault();
        $('.step .img-content-hidden-block').hide();
        $('.step .button-down-block').show();
        $('.step .button-up-block').hide();
        var elementClick = $('.step .button-up-block a').attr("href");
        var destination = $(elementClick).offset().top;

        $('html').animate({ scrollTop: destination }, 1100);

    });


    //animation init
    new WOW().init();

    //Animation of increasing numbers
    //$('.our-numbers-sl1').viewportChecker({
    //    callbackFunction: function(elem, action) {
    //        $('.up-numbers-sl1-1').animateNumber({
    //            number: 2000
    //        }, 8000);
    //        $('.up-numbers-sl1-2').animateNumber({
    //            number: 1
    //        }, 8500);
    //        $('.up-numbers-sl1-3').animateNumber({
    //            number: 5
    //        }, 8000);
    //        $('.up-numbers-sl1-4').animateNumber({
    //            number: 4000
    //        }, 8000);
    //    }
    //});
    //$('.our-numbers-sl2').viewportChecker({
    //    callbackFunction: function(elem, action) {
    //        $('.up-numbers-sl2-1').animateNumber({
    //            number: 201
    //        }, 10000);
    //        $('.up-numbers-sl2-2').animateNumber({
    //            number: 2
    //        }, 9000);
    //        $('.up-numbers-sl2-3').animateNumber({
    //            number: 87
    //        }, 10000);
    //        $('.up-numbers-sl2-4').animateNumber({
    //            number: 12
    //        }, 10000);
    //    }
    //});
    //$('.our-numbers-sl3').viewportChecker({
    //    callbackFunction: function(elem, action) {
    //        $('.up-numbers-sl3-1').animateNumber({
    //            number: 150
    //        }, 4000);
    //        $('.up-numbers-sl3-2').animateNumber({
    //            number: 0
    //        }, 500);
    //        $('.up-numbers-sl3-3').animateNumber({
    //            number: 39
    //        }, 4000);
    //        $('.up-numbers-sl3-4').animateNumber({
    //            number: 70
    //        }, 4000);
    //    }
    //});
    //$('.our-numbers-sl4').viewportChecker({
    //    callbackFunction: function(elem, action) {
    //        $('.up-numbers-sl4-1').animateNumber({
    //            number: 365
    //        }, 12000);
    //        $('.up-numbers-sl4-2').animateNumber({
    //            number: 2
    //        }, 11000);
    //        $('.up-numbers-sl4-3').animateNumber({
    //            number: 87
    //        }, 12000);
    //        $('.up-numbers-sl4-4').animateNumber({
    //            number: 40
    //        }, 12000);
    //    }
    //});
    //$('.our-numbers-sl5').viewportChecker({
    //    callbackFunction: function(elem, action) {
    //        $('.up-numbers-sl5-1').animateNumber({
    //            number: 1393
    //        }, 6000);
    //        $('.up-numbers-sl5-2').animateNumber({
    //            number: 1
    //        }, 5000);
    //        $('.up-numbers-sl5-3').animateNumber({
    //            number: 97
    //        }, 6000);
    //        $('.up-numbers-sl5-4').animateNumber({
    //            number: 12
    //        }, 6000);
    //    }
    //});
    //$('.our-numbers-sl6').viewportChecker({
    //    callbackFunction: function(elem, action) {
    //        $('.up-numbers-sl6-1').animateNumber({
    //            number: 245
    //        }, 14000);
    //        $('.up-numbers-sl6-2').animateNumber({
    //            number: 3
    //        }, 13000);
    //        $('.up-numbers-sl6-3').animateNumber({
    //            number: 2
    //        }, 14000);
    //        $('.up-numbers-sl6-4').animateNumber({
    //            number: 3
    //        }, 14000);
    //    }
    //});
    //$('.our-numbers-sl7').viewportChecker({
    //    callbackFunction: function(elem, action) {
    //        $('.up-numbers-sl7-1').animateNumber({
    //            number: 3994
    //        }, 2000);
    //        $('.up-numbers-sl7-2').animateNumber({
    //            number: 2
    //        }, 1000);
    //        $('.up-numbers-sl7-3').animateNumber({
    //            number: 4
    //        }, 2000);
    //        $('.up-numbers-sl7-4').animateNumber({
    //            number: 26
    //        }, 2000);
    //    }
    //});
    //$('.step-count').viewportChecker({
    //    callbackFunction: function(elem, action) {
    //        $('.step-count-num1').animateNumber({
    //            number: 1000
    //        }, 2000);
    //        $('.step-count-num2').animateNumber({
    //            number: 2
    //        }, 2000);
    //        $('.step-count-num3').animateNumber({
    //            number: 20
    //        }, 2000);
    //    }
    //});
//

    //Slider lead
    $('#animatedHeading').slick({
        autoplay: true,
        arrows: false,
        dots: false,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: "10px",
        draggable: false,
        infinite: true,
        pauseOnHover: false,
        swipe: false,
        touchMove: false,
        vertical: true,
        autoplaySpeed: 5000,
        adaptiveHeight: true
    });

    $('#animatedHeading').on('afterChange', function(event, slick, currentSlide) {
        var i = $('.applications p span').text();
        i++;
        $('.applications p span').text(i);
    });

    //Telephone mask

    $('input[type=tel]').intlTelInput();

    $('input[type=tel]').on('countrychange', function(e, countryData) {
        $(this).val(countryData.dialCode);
    });

    $('input[type=tel]').val($('input[type=tel]').intlTelInput("getSelectedCountryData").dialCode);

    $('.input-field input[type=tel]').on('focus', function () {
        $('.input-field').addClass('form-group has-feedback');
    });



    $.fn.forceNumbericOnly = function() {
        return this.each(function() {
            $(this).keydown(function(e) {
                var key = e.charCode || e.keyCode || 0;
                return (key == 8 || key == 9 || key == 46 || (key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 107 || key == 109 || key == 173 || key == 61);
            });
        });
    };

    $('input[type=tel]').forceNumbericOnly();

    //

    var uploadField = document.getElementById("rez-file");

    uploadField.onchange = function() {
        if(this.files[0].size > 10000000){
            alert("Размер файла слишком большой!");
            this.value = "";
        };
    };

   // $('.phone-mask-input').intlTelInput();
//
   // $('.phone-mask-input').on('focus', function () {
   //     $('.phone-mask-input').val($('.phone-mask-input').intlTelInput("getSelectedCountryData").dialCode);
   // });
//
   // $('.phone-mask-input').on('countrychange', function(e, countryData) {
   //     $(this).val(countryData.dialCode);
   // });
//
   // $.fn.forceNumbericOnly = function() {
   //     return this.each(function() {
   //         $(this).keydown(function(e) {
   //             var key = e.charCode || e.keyCode || 0;
   //             return (key == 8 || key == 9 || key == 46 || (key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 107 || key == 109 || key == 173 || key == 61);
   //         });
   //     });
   // };
   // $('.phone-mask-input').forceNumbericOnly();

    $('.form-input').on('focus', function() {
        $(this).closest('.input-field').find('label').addClass('active');
    });
    $('.form-input').on('blur', function() {
        var $this = $(this);
        if ($this.val() == '') {
            $this.closest('.input-field').find('label').removeClass('active');
        }
    });


    //Picture slider in cap
    $('#img-slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: -1,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 5000
    });

    //Slider video in cap
    $('#header-video-slider').on('init', function(ev, el) {
        $('.h-video').each(function() {
            this.play();
        });
    });
    $('#header-video-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        fade: true,
        arrows: false,
        autoplaySpeed: 5000
    });

    //Zoom at aiming
    //$("#zoom_img").elevateZoom({
    //    zoomType: "inner",
    //    cursor: "crosshair",
    //    zoomWindowFadeIn: 500,
    //    zoomWindowFadeOut: 750
    //});

    $('#zoom_img').zoom();

    //Today's date in the leads
    var d = new Date();
    var month = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    var myData = (d.getDate() + "." + month[d.getMonth()] + "." + d.getFullYear());
    $('.app-block .data').text(myData);

    //odometrr options
    window.odometerOptions = {
        auto: false
    };

});

//odomert
$(window).scroll(function() {
    var OdometrPosition = $('.expenses').offset().top;
    var ScrollTop = $(window).scrollTop();
    if (ScrollTop = OdometrPosition) {
        setTimeout(function() {
            odometer.innerHTML = 1700000;
        }, 1000);
    }
});


$('.phone-list').hide();

$('header .phon').click(function(){
    $('.phone-list').slideDown(400);
});

$('.phone-list .close').click(function(){
    $('.phone-list').slideUp(400);
});

$('.phon-footer').click(function(e){
    e.preventDefault();
    $('.phone-list-footer').slideDown(400);
});

$('.phone-list-footer .close').click(function(e){
    e.preventDefault();
    $('.phone-list-footer').slideUp(400);
});

//

$('.thank-pag .phon').click(function(){
    $('.phone-list').slideDown(400);
});

$('.phone-list .close').click(function(){
    $('.phone-list').slideUp(400);
});


//messeger

$('.activ-closer-icon').on('click', function () {
    $('.floating-messages .hidden-hint').toggleClass('dnon');
    $('.activ-closer-icon').toggleClass('click-open-rotate');
    $('.activ-closer-icon .acive-icon').toggleClass('togle-active');
    $('.activ-closer-icon .closer-icon').toggleClass('togle-close');
    $('.floating-messages').find('.massegers').slideToggle(300);
});

$('.floating-messages .activ-closer-icon').on('mouseover', function(){
    $('.floating-messages .hidden-hint').css({'opacity':'1'});
});
$('.floating-messages .activ-closer-icon').on('mouseout', function(){
    $('.floating-messages .hidden-hint').css({'opacity':'0'});
});

//scroll

$(".privacy-policy-content").mCustomScrollbar({
    theme:"minimal-dark"
});


//privasy popup

$('a.privacy-open').click(function(event) {
    event.preventDefault();
    $('.privacy-policy-body').fadeIn(400, function() {
        $('.privacy-policy-content').css('display', 'block');
    });
});
$('.privacy-policy-body, .privacy-policy-content .closer').click(function() {
    $('.privacy-policy-content').css('display', 'none');
    $('.privacy-policy-body').fadeOut(400);
});

$('#open-sub1').click(function(){
    $('#sub-menu1').slideToggle(400);
    $('#open-sub1').toggleClass('arrow-revers');
    $('#sub-menu2').css('display', 'none');
    $('#open-sub2').removeClass('arrow-revers');
});

$('#open-sub2').click(function(){
    $('#sub-menu2').slideToggle(400);
    $('#open-sub2').toggleClass('arrow-revers');
    $('#sub-menu1').css('display', 'none');
    $('#open-sub1').removeClass('arrow-revers');
});

$('#sub-menu2 a').on('click', function () {
    $('#open-sub2').toggleClass('arrow-revers');
    $('#sub-menu2').slideToggle(400);
});

$('#sub-menu1 a').on('click', function () {
    $('#open-sub1').toggleClass('arrow-revers');
    $('#sub-menu1').slideToggle(400);
});


