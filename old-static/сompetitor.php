<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#e8f3fe">

    <title>Спасибо! Мы просмотрим Ваше резюме и свяжемся с Вами.</title>
    <meta name="description" content="Увеличиваем продажи с помощью рекламы в Facebook и Instagram — основное направление компании SMMSTUDIO."/>

    <meta property="og:title" content="SMMSTUDIO" />
    <meta property="og:description" content="Увеличиваем продажи с помощью рекламы в Facebook и Instagram" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="http://smmstudio.com.ua" />
    <meta property="og:image" content="http://smmstudio.com.ua/img/og.png" />

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5RSJG8L');</script>
    <!-- End Google Tag Manager -->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="wrapper">
    <section class="thank-pag">
        <div class="wrap">
            <div class="logo-phon">
                <div class="logo-block">
                    <a href="index.php" class="logo"><img src="img/logo.svg" alt="">
                        <p>SMMSTUDIO</p>
                    </a>
                    <p class="signature">цифровое маркетинговое агентство</p>
                </div>
                <div class="phon">
                    <p><img src="img/voip.svg" alt=""><span>0 800 754 754</span></p>
                    <a href="#" class="open-phone-list"><img src="img/arrowsdown.svg" alt=""></a>
                </div>
                <div class="phone-list">
                    <p class="phon header-phone"><img src="img/voip.svg" alt="" ></p>
                    <a href="#" class="close"><img src="img/arrows.svg" alt=""></a>
                    <ul>
                        <li><img src="img/ua.svg" alt=""><a href="tel:0800754754">0 800 754 754</a></li>
                        <li><img src="img/ru.svg" alt=""><a href="tel:74993227910">7 499 322 79 10</a></li>
                        <li><img src="img/kz.svg" alt=""><a href="tel:77172696230">77 1726 96 230</a></li>
                        <li><img src="img/gb.svg" alt=""><a href="tel:448000698473">44 800 069 84 73</a></li>
                        <li><p class="schedule">Пн-пт, 9:00 — 18:00 по Киеву</p></li>
                    </ul>
                </div>
            </div>
            <h1>Спасибо! Мы просмотрим Ваше резюме и свяжемся с Вами.</h1>
            <a href="index.php" class="button">Вернуться на сайт</a>
        </div>
    </section>
    <?php
    include ('partials/contacts.php');
    ?>
    <?php
    include ('partials/privacy-policy.php');
    ?>
    <?php
    include ('partials/floating-messegers.php');
    ?>
</div>
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/odometer-theme-default.css">
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="css/intlTelInput.css">
<link rel="stylesheet" href="css/fotorama.css">
<link rel="stylesheet" href="css/animate.css">
<link type="text/css" rel="stylesheet" href="css/style.css" />

<!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
<!--[if lt IE 7]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script> <![endif]-->
<!--[if lt IE 8]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script> <![endif]-->
<!--[if lt IE 9]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script> <![endif]-->
<!--[if lt IE 9]> <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
<script src="js/jquery.js"></script>
<script src="js/intlTelInput.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/jquery.viewportchecker.min.js"></script>
<script src="js/odometer.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="js/fotorama.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/myscripts.js"></script>
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
</body>

</html>