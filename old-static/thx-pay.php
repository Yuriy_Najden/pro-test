<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#e8f3fe">

    <title>SMMSTUDIO — целевой трафик из социальных сетей</title>
    <meta name="description" content="Запуск системного  целевого трафик из социальных сетей для бизнеса — основное направление компании SMMSTUDIO."/>

    <meta property="og:title" content="SMMSTUDIO" />
    <meta property="og:description" content="Запускаем системный целевой трафик и социальных сетей" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="http://smmstudio.com.ua" />
    <meta property="og:image" content="http://smmstudio.com.ua/img/og.png" />

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5RSJG8L');</script>
    <!-- End Google Tag Manager -->
    
        <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '182480625611556'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=182480625611556&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '142692132975924'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=142692132975924&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '480042189046599'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=480042189046599&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '480042189046599'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=480042189046599&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RSJG8L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="wrapper">
    <section class="thank-pag">
        <div class="wrap">
            <div class="logo-phon">
                <div class="logo-block">
                    <a href="index.php" class="logo"><img src="img/logo.svg" alt="">
                        <p>SMMSTUDIO</p>
                    </a>
                    <p class="signature">цифровое маркетинговое агентство</p>
                </div>
                <div class="phon">
                    <p><img src="img/voip.svg" alt=""><span>0 800 754 754</span></p>
                    <a href="#" class="open-phone-list"><img src="img/arrowsdown.svg" alt=""></a>
                </div>
                <div class="phone-list">
                    <p class="phon"><img src="img/voip.svg" alt="" ></p>
                    <a href="#" class="close"><img src="img/arrows.svg" alt=""></a>
                    <ul>
                        <li><img src="img/ua.svg" alt=""><a href="tel:0800754754">0 800 754 754</a></li>
                        <li><img src="img/ru.svg" alt=""><a href="tel:74993227910">7 499 322 79 10</a></li>
                        <li><img src="img/kz.svg" alt=""><a href="tel:77172696230">77 1726 96 230</a></li>
                        <li><img src="img/gb.svg" alt=""><a href="tel:448000698473">44 800 069 84 73</a></li>
                        <li><p class="schedule">Пн-пт, 9:00 — 18:00 по Киеву</p></li>
                    </ul>
                </div>
            </div>
            <h1>Спасибо за Вашу оплату!</h1>
            <a href="index.php" class="button">Вернуться на сайт</a></div>
    </section>
    <?php
    include ('partials/contacts.php');
    ?>
    <?php
    include ('partials/privacy-policy.php');
    ?>
    <?php
    include ('partials/floating-messegers.php');
    ?>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/odometer-theme-default.css">
<link rel="stylesheet" href="css/fotorama.css">
<link rel="stylesheet" href="css/animate.css">
<link type="text/css" rel="stylesheet" href="css/style.css" />
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
<!--[if lt IE 7]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script> <![endif]-->
<!--[if lt IE 8]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script> <![endif]-->
<!--[if lt IE 9]> <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script> <![endif]-->
<!--[if lt IE 9]> <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script> <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="js/materialize.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/odometer.min.js"></script>
<script src="js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="js/fotorama.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/myscripts.js"></script>
<script src="mailform.js"></script>
</body>

</html>