<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package smmstudio
 */


	$linkTrigger = get_bloginfo('language');

	$workTime = get_field('vremya_raboty', 'options');
	$textContacts = get_field('podpis_kontakty', 'options');
	$textPravPoly = get_field('podpis_politika_konfidenczialnosti', 'options');
	$linkPravPol = get_field('ssylka_na_straniczu_politiki_konfidenczialnosti', 'options');

	$workTime = $workTime[$linkTrigger];
	$textContacts = $textContacts[$linkTrigger];
	$textPravPoly = $textPravPoly[$linkTrigger];
	$linkPravPol = $linkPravPol[$linkTrigger];
?>

	<footer>
		<div class="container">
            <div class="row">
                <div class="content col-12">
	                <?php
		                $linkTrigger = get_bloginfo('language');
		                $domName = home_url();
		                $logoLink = '';

		                if( $linkTrigger == 'ua' ){
			                $logoLink = $domName.'/ua/target-2';
		                }elseif ( $linkTrigger == 'ru' ){
			                $logoLink = $domName.'/target';
		                }
	                ?>
                    <a href="<?php echo $logoLink;?>" class="logo">
                        <svg width="200" height="41" viewBox="0 0 200 41" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clipHeaderLogo)">
                                <path d="M67.2818 19.2413C64.2707 18.5226 63.5583 17.9672 63.5583 16.7585C63.5583 15.6804 64.4973 14.831 66.1162 14.831C67.5409 14.831 68.9655 15.3864 70.3578 16.4644L71.8471 14.3736C70.2606 13.0995 68.4474 12.3808 66.1486 12.3808C63.0403 12.3808 60.8062 14.2429 60.8062 16.9545C60.8062 19.8947 62.6841 20.9075 66.0191 21.6915C68.9331 22.4103 69.5483 22.9983 69.5483 24.1417C69.5483 25.3832 68.4798 26.1672 66.7638 26.1672C64.8211 26.1672 63.3317 25.4158 61.8099 24.1091L60.1586 26.1019C62.0366 27.8007 64.303 28.6174 66.699 28.6174C70.0016 28.6174 72.2681 26.8533 72.2681 23.9131C72.3004 21.2342 70.5844 20.0581 67.2818 19.2413Z" fill="white"/>
                                <path d="M88.0039 12.6103L83.2443 20.0589L78.4847 12.6103H75.5706V28.3896H78.258V17.1187L83.1471 24.5019H83.2443L88.1981 17.0533V28.3896H90.9503V12.6103H88.0039Z" fill="white"/>
                                <path d="M107.431 12.6103L102.671 20.0589L97.9116 12.6103H94.9651V28.3896H97.6849V17.1187L102.574 24.5019H102.671L107.593 17.0533V28.3896H110.345V12.6103H107.431Z" fill="white"/>
                                <path d="M120.414 19.2421C117.403 18.5234 116.691 17.968 116.691 16.7593C116.691 15.6812 117.63 14.8318 119.249 14.8318C120.673 14.8318 122.066 15.3872 123.49 16.4652L124.98 14.3744C123.393 13.1003 121.58 12.3816 119.281 12.3816C116.173 12.3816 113.939 14.2437 113.939 16.9553C113.939 19.8955 115.817 20.9083 119.152 21.6923C122.033 22.4111 122.681 22.9991 122.681 24.1425C122.681 25.384 121.612 26.168 119.896 26.168C117.954 26.168 116.464 25.4166 114.975 24.1099L113.324 26.1027C115.202 27.8015 117.468 28.6182 119.864 28.6182C123.167 28.6182 125.465 26.8541 125.465 23.9138C125.433 21.235 123.717 20.0589 120.414 19.2421Z" fill="white"/>
                                <path d="M127.214 12.6103V15.1912H132.168V28.4222H134.952V15.1912H139.906V12.6103H127.214Z" fill="white"/>
                                <path d="M153.214 12.6103V21.6924C153.214 24.5999 151.724 26.1027 149.263 26.1027C146.803 26.1027 145.313 24.5346 145.313 21.5944V12.6103H142.561V21.6924C142.561 26.2661 145.151 28.6509 149.231 28.6509C153.343 28.6509 155.966 26.2661 155.966 21.5617V12.6103H153.214Z" fill="white"/>
                                <path d="M165.582 12.6103H159.754V28.3896H165.582C170.504 28.3896 173.903 24.9266 173.903 20.4836C173.903 16.0079 170.504 12.6103 165.582 12.6103ZM165.582 25.874H162.506V15.1258H165.582C168.885 15.1258 171.022 17.38 171.022 20.4836C171.022 23.6199 168.885 25.874 165.582 25.874Z" fill="white"/>
                                <path d="M177.4 12.6103V28.3896H180.152V12.6103H177.4Z" fill="white"/>
                                <path d="M191.841 12.349C187.016 12.349 183.649 16.0733 183.649 20.5163C183.649 24.992 187.016 28.6836 191.808 28.6836C196.633 28.6836 200.032 24.9593 200.032 20.5163C200 16.0079 196.633 12.349 191.841 12.349ZM191.841 26.1028C188.732 26.1028 186.498 23.5546 186.498 20.4836C186.498 17.38 188.668 14.8645 191.776 14.8645C194.884 14.8645 197.086 17.4127 197.086 20.4836C197.086 23.6199 194.917 26.1028 191.841 26.1028Z" fill="white"/>
                                <path d="M44.6171 20.4835C44.6171 22.3457 44.4876 24.2078 44.261 26.0046L0 21.5616V19.4054L44.2933 14.9624C44.52 16.7919 44.6171 18.6214 44.6171 20.4835Z" fill="white"/>
                                <path d="M43.4839 10.1928L43.3544 10.2255L0 17.3474V15.1259L39.1776 0.228685L39.7604 0C41.3793 3.20159 42.6421 6.63187 43.4839 10.1928Z" fill="white"/>
                                <path d="M43.4515 30.8726C42.6097 34.4335 41.3469 37.8311 39.7604 41.0001L39.1776 40.7714L0 25.8742V23.6527L43.322 30.7746V30.8399L43.4515 30.8726Z" fill="white"/>
                            </g>
                            <defs>
                                <clipPath id="clipHeaderLogo">
                                    <rect width="200" height="41" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>
                    </a>
                    <div class="footer-menu">
	                    <?php
		                    wp_nav_menu(
			                    array(
				                    'theme_location' => 'menu-2',
				                    'menu_id'        => 'footer-menu',
				                    'container' => false,
				                    'menu_class' => 'second-menu',
			                    )
		                    );
	                    ?>
                    </div>
                    <div class="our-contacts">
                        <h3><?php echo $textContacts;?></h3>
	                    <?php
		                    $mainPhone = get_field('telefon', 'options');

		                    $phoneToCall = preg_replace( '/[^0-9]/', '', $mainPhone);
	                    ?>
                        <a href="tel:<?php echo $phoneToCall;?>" class="phone">
                            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.44285 5.59453L5.93894 3.10078C6.24363 2.79609 6.65144 2.625 7.08269 2.625C7.51394 2.625 7.92175 2.79375 8.22644 3.10078L10.9147 5.78437C11.2194 6.08906 11.3905 6.49922 11.3905 6.93047C11.3905 7.36406 11.2218 7.76953 10.9147 8.07656L8.81472 10.1789C9.29312 11.2936 9.98049 12.3063 10.8397 13.1625C11.6999 14.0273 12.7053 14.7094 13.821 15.1922L15.921 13.0898C16.2257 12.7852 16.6335 12.6141 17.0647 12.6141C17.2774 12.6133 17.4882 12.655 17.6846 12.7366C17.881 12.8183 18.0591 12.9384 18.2085 13.0898L20.8991 15.7734C21.2038 16.0781 21.3749 16.4883 21.3749 16.9195C21.3749 17.3531 21.2061 17.7586 20.8991 18.0656L18.4053 20.5594C17.885 21.0797 17.1678 21.3773 16.4319 21.3773C16.2796 21.3773 16.1319 21.3656 15.9819 21.3398C12.8788 20.8289 9.7991 19.1766 7.31238 16.6922C4.828 14.2031 3.178 11.1234 2.66238 8.01797C2.51472 7.13672 2.81004 6.22969 3.44285 5.59453ZM4.3241 7.73906C4.78113 10.5023 6.26707 13.2586 8.50535 15.4969C10.7436 17.7352 13.4975 19.2211 16.2608 19.6781C16.6077 19.7367 16.9639 19.6195 17.2171 19.3687L19.6663 16.9195L17.0694 14.3203L14.2616 17.1328L14.2405 17.1539L13.7343 16.9664C12.1993 16.402 10.8054 15.5107 9.64912 14.354C8.49288 13.1974 7.60198 11.8032 7.03816 10.268L6.85066 9.76172L9.68191 6.93281L7.08503 4.33359L4.63581 6.78281C4.38269 7.03594 4.2655 7.39219 4.3241 7.73906Z" fill="#F0F0F4"/>
                                <path opacity="0.1" d="M8 14C8 10.6863 10.6863 8 14 8H22C25.3137 8 28 10.6863 28 14V22C28 25.3137 25.3137 28 22 28H8V14Z" fill="#F0F0F4"/>
                            </svg>

                            <?php echo  $mainPhone;?>
                        </a>
                        <p class="working-time"><?php echo $workTime;?></p>
                        <div class="social">
                            <a href="<?php echo get_field('ssylka_na_instagramm', 'options')?>" target="_blank">
                                <!--<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M17.8711 15C17.8711 16.5857 16.5857 17.8711 15 17.8711C13.4143 17.8711 12.1289 16.5857 12.1289 15C12.1289 13.4143 13.4143 12.1289 15 12.1289C16.5857 12.1289 17.8711 13.4143 17.8711 15Z" fill="white"/>
                                    <path d="M21.7144 9.91991C21.5764 9.54591 21.3562 9.2074 21.0701 8.92953C20.7923 8.64343 20.454 8.42325 20.0797 8.28523C19.7762 8.16736 19.3203 8.02705 18.4805 7.98883C17.5721 7.9474 17.2997 7.93848 14.9999 7.93848C12.6999 7.93848 12.4275 7.94717 11.5193 7.9886C10.6796 8.02705 10.2234 8.16736 9.92014 8.28523C9.54591 8.42325 9.2074 8.64343 8.92976 8.92953C8.64366 9.2074 8.42348 9.54568 8.28523 9.91991C8.16736 10.2234 8.02705 10.6796 7.98883 11.5193C7.9474 12.4275 7.93848 12.6999 7.93848 14.9999C7.93848 17.2997 7.9474 17.5721 7.98883 18.4805C8.02705 19.3203 8.16736 19.7762 8.28523 20.0797C8.42348 20.454 8.64343 20.7922 8.92953 21.0701C9.2074 21.3562 9.54568 21.5764 9.91991 21.7144C10.2234 21.8325 10.6796 21.9728 11.5193 22.011C12.4275 22.0525 12.6997 22.0612 14.9997 22.0612C17.3 22.0612 17.5723 22.0525 18.4803 22.011C19.3201 21.9728 19.7762 21.8325 20.0797 21.7144C20.8309 21.4246 21.4247 20.8309 21.7144 20.0797C21.8323 19.7762 21.9726 19.3203 22.011 18.4805C22.0525 17.5721 22.0612 17.2997 22.0612 14.9999C22.0612 12.6999 22.0525 12.4275 22.011 11.5193C21.9728 10.6796 21.8325 10.2234 21.7144 9.91991ZM14.9999 19.4226C12.5571 19.4226 10.5768 17.4426 10.5768 14.9997C10.5768 12.5569 12.5571 10.5768 14.9999 10.5768C17.4426 10.5768 19.4229 12.5569 19.4229 14.9997C19.4229 17.4426 17.4426 19.4226 14.9999 19.4226ZM19.5977 11.4356C19.0269 11.4356 18.5641 10.9728 18.5641 10.4019C18.5641 9.8311 19.0269 9.3683 19.5977 9.3683C20.1685 9.3683 20.6313 9.8311 20.6313 10.4019C20.6311 10.9728 20.1685 11.4356 19.5977 11.4356Z" fill="white"/>
                                    <path d="M15 0C6.717 0 0 6.717 0 15C0 23.283 6.717 30 15 30C23.283 30 30 23.283 30 15C30 6.717 23.283 0 15 0ZM23.5613 18.5511C23.5197 19.468 23.3739 20.094 23.161 20.6419C22.7135 21.7989 21.7989 22.7135 20.6419 23.161C20.0942 23.3739 19.468 23.5194 18.5513 23.5613C17.6328 23.6032 17.3394 23.6133 15.0002 23.6133C12.6608 23.6133 12.3676 23.6032 11.4489 23.5613C10.5322 23.5194 9.90601 23.3739 9.35829 23.161C8.78334 22.9447 8.26286 22.6057 7.83257 22.1674C7.39449 21.7374 7.05551 21.2167 6.83922 20.6419C6.62636 20.0942 6.48056 19.468 6.4389 18.5513C6.39656 17.6326 6.38672 17.3392 6.38672 15C6.38672 12.6608 6.39656 12.3674 6.43867 11.4489C6.48033 10.532 6.6259 9.90601 6.83876 9.35806C7.05505 8.78334 7.39426 8.26263 7.83257 7.83257C8.26263 7.39426 8.78334 7.05528 9.35806 6.83899C9.90601 6.62613 10.532 6.48056 11.4489 6.43867C12.3674 6.39679 12.6608 6.38672 15 6.38672C17.3392 6.38672 17.6326 6.39679 18.5511 6.4389C19.468 6.48056 20.094 6.62613 20.6419 6.83876C21.2167 7.05505 21.7374 7.39426 22.1677 7.83257C22.6057 8.26286 22.9449 8.78334 23.161 9.35806C23.3741 9.90601 23.5197 10.532 23.5616 11.4489C23.6034 12.3674 23.6133 12.6608 23.6133 15C23.6133 17.3392 23.6034 17.6326 23.5613 18.5511Z" fill="white"/>
                                </svg>-->
                                <svg width="512" height="512" viewBox="0 0 512 512" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0footInst)">
                                        <path d="M31.9997 34.8373C-8.23497 76.6293 -0.000306403 121.024 -0.000306403 255.893C-0.000306403 367.893 -19.5416 480.171 82.7304 506.603C114.666 514.816 397.632 514.816 429.525 506.56C472.106 495.573 506.752 461.035 511.488 400.811C512.149 392.405 512.149 119.531 511.466 110.955C506.432 46.8053 466.944 9.83464 414.912 2.34664C402.986 0.618639 400.597 0.106639 339.413 -2.74732e-05C122.389 0.106639 74.8157 -9.55736 31.9997 34.8373V34.8373Z" fill="url(#paint0_linearfootInst)"/>
                                        <path d="M255.957 66.9653C178.496 66.9653 104.938 60.0747 76.8423 132.181C65.237 161.963 66.9223 200.64 66.9223 256.021C66.9223 304.619 65.365 350.293 76.8423 379.84C104.874 451.989 179.029 445.077 255.914 445.077C330.09 445.077 406.57 452.8 435.008 379.84C446.634 349.76 444.928 311.659 444.928 256.021C444.928 182.165 449.002 134.485 413.184 98.688C376.917 62.4213 327.872 66.9653 255.872 66.9653H255.957ZM239.018 101.035C400.597 100.779 421.162 82.816 409.813 332.352C405.781 420.608 338.581 410.923 255.978 410.923C105.365 410.923 101.034 406.613 101.034 255.936C101.034 103.509 112.981 101.12 239.018 100.992V101.035ZM356.864 132.416C344.341 132.416 334.186 142.571 334.186 155.093C334.186 167.616 344.341 177.771 356.864 177.771C369.386 177.771 379.541 167.616 379.541 155.093C379.541 142.571 369.386 132.416 356.864 132.416V132.416ZM255.957 158.933C202.346 158.933 158.89 202.411 158.89 256.021C158.89 309.632 202.346 353.088 255.957 353.088C309.568 353.088 353.002 309.632 353.002 256.021C353.002 202.411 309.568 158.933 255.957 158.933V158.933ZM255.957 193.003C339.264 193.003 339.37 319.04 255.957 319.04C172.672 319.04 172.544 193.003 255.957 193.003Z" fill="white"/>
                                    </g>
                                    <defs>
                                        <linearGradient id="paint0_linearfootInst" x1="32.9815" y1="479.298" x2="508.831" y2="67.4554" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#FFDD55"/>
                                            <stop offset="0.5" stop-color="#FF543E"/>
                                            <stop offset="1" stop-color="#C837AB"/>
                                        </linearGradient>
                                        <clipPath id="clip0footInst">
                                            <rect width="512" height="512" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>

                            </a>
                            <a href="<?php echo get_field('ssylka_na_fejsbuk', 'options')?>" target="_blank">
                                <!--<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M30 15C30 6.71484 23.2852 0 15 0C6.71484 0 0 6.71484 0 15C0 23.2852 6.71484 30 15 30C15.0879 30 15.1758 30 15.2637 29.9941V18.3223H12.041V14.5664H15.2637V11.8008C15.2637 8.5957 17.2207 6.84961 20.0801 6.84961C21.4512 6.84961 22.6289 6.94922 22.9688 6.99609V10.3477H21C19.4473 10.3477 19.1426 11.0859 19.1426 12.1699V14.5605H22.8633L22.377 18.3164H19.1426V29.4199C25.4121 27.6211 30 21.8496 30 15Z" fill="white"/>
                                </svg>-->
                                <svg width="512" height="512" viewBox="0 0 512 512" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0footFb)">
                                        <path d="M31.9997 34.8373C-8.23497 76.6293 -0.000306403 121.024 -0.000306403 255.893C-0.000306403 367.893 -19.5416 480.171 82.7304 506.603C114.666 514.816 397.632 514.816 429.525 506.56C472.106 495.573 506.752 461.035 511.488 400.811C512.149 392.405 512.149 119.531 511.466 110.955C506.432 46.8053 466.944 9.83464 414.912 2.34664C402.986 0.618639 400.597 0.106639 339.413 -2.74732e-05C122.389 0.106639 74.8157 -9.55736 31.9997 34.8373V34.8373Z" fill="#3B5999"/>
                                        <path d="M352 256V192C352 174.336 366.336 176 384 176H416V96H352C298.965 96 256 138.965 256 192V256H192V336H256V512H352V336H400L432 256H352Z" fill="white"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0footFb">
                                            <rect width="512" height="512" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>

                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="footer-info col-12">
                    <p class="copy">© 2013-<?php echo date('Y');?> SMMSTUDIO</p>
                    <a href="<?php echo $linkPravPol;?>" target="_blank"><?php echo $textPravPoly;?></a>
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->
    <?php include ('template-parts/popup.php');?>
</div><!-- #page -->

<?php wp_footer(); ?>
<!-- Для com -->
<!--<script src="https://www.google.com/recaptcha/api.js?render=6LcNmFoaAAAAAAOT9DlmDMIDncYiSDyPMvX_U3kT"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcNmFoaAAAAAAOT9DlmDMIDncYiSDyPMvX_U3kT', {action: 'homepage'}).then(function(token) {
            console.log(token);
            document.getElementsByClassName('g-recaptcha-response').value=token;
        });
    });
</script>-->

<!-- Для com.ua -->

<!--<script src="https://www.google.com/recaptcha/api.js?render=6LcSploaAAAAABxZqqBZvjGr3gyCYD7Hnng7p7Wu"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcSploaAAAAABxZqqBZvjGr3gyCYD7Hnng7p7Wu', {action: 'homepage'}).then(function(token) {
            console.log(token);
            document.getElementsByClassName('g-recaptcha-response').value=token;
        });
    });
</script>-->

</body>
</html>
