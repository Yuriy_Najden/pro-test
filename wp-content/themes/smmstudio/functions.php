<?php
/**
 * smmstudio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package smmstudio
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'smmstudio_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function smmstudio_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on smmstudio, use a find and replace
		 * to change 'smmstudio' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'smmstudio', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'smmstudio' ),
				'menu-2' => esc_html__( 'Footer', 'smmstudio' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'smmstudio_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'smmstudio_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function smmstudio_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'smmstudio_content_width', 640 );
}
add_action( 'after_setup_theme', 'smmstudio_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function smmstudio_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'smmstudio' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'smmstudio' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'smmstudio_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/scripts-styles.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Custom Post Types.
 */
require get_template_directory() . '/inc/custom-post-types.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	define( 'SITE_URL', get_site_url() );
	define( 'SITE_LOCALE', get_locale() );
	define( 'THEME_PATH', get_template_directory_uri() );

//Init Ajax

	add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
	function myajax_data(){

		wp_localize_script('smmstudio-main-js', 'myajax',
			array(
				'url' => admin_url('admin-ajax.php')
			)
		);

	}


/**
 * Ajax functions
 */

require get_template_directory() . '/inc/ajax-functions.php';


	//Site Send Mail To Amo

	use PHPMailer\PHPMailer\PHPMailer;

	function contact_form_send_email_to_admin() {

		require_once __DIR__ . '/amocrm.phar';
		require_once('vendor/autoload.php');


		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';*/

		$name = $_POST['name'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$formLang = $_POST['form-lang'];
		$homeUrl = $_POST['home-url'];
        $message = $_POST['mess'];
        $spamFild = $_POST['moreinfo'];

        $servicesList = $_POST['ch-service'];
		$serviceFormData = array();
		$target = '';
		$ved = '';
		$context = '';
		$dev = '';
		$automation = '';
		$diz = '';

        if ( $servicesList ){
        	foreach ( $servicesList as $key => $value ){

        		if ( $value == 'target' ){
			        $target = '4626965';
		        }
		        if ( $value == 'ved' ){
			        $ved = '4626967';
		        }
		        if ( $value == 'context' ){
			        $context = '4626969';
		        }
		        if ( $value == 'dev' ){
			        $dev = '4626971';
		        }
		        if ( $value == 'automation' ){
			        $automation = '4626973';
		        }
		        if ( $value == 'diz' ){
			        $diz = '4626975';
		        }
	        }

	        $serviceFormData = array($target, $ved, $context, $dev, $automation, $diz);
        }

		echo '<pre>';
		print_r($serviceFormData);
		echo '</pre>';
		echo $name;

		$utmSource = $_POST['utm_source'];
		$utmMedium = $_POST['utm_medium'];
		$utmCampaign = $_POST['utm_campaign'];
		$utmTerm = $_POST['utm_term'];
		$utmContent = $_POST['utm_content'];
		$gclid = $_POST['gclid'];

		if ( !empty($gclid)){
			$utmSource = 'adwords';
		}

		$phone = preg_replace( '/[^0-9]/', '', $phone);

		$thxName = '';

		if( $formLang == 'ua' ){
			$thxName = '/ua/dyakuєmo';
        }elseif ( $formLang == 'ru' ){
			$thxName = 'spasibo';
        }

		$voronka = 15516604;

        //Подключение проверки капчи для com
		/*function getCaptcha($SecretKey) {
			$Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcNmFoaAAAAAF70idJ6j76IJdvKOePcZFU7FO15&response={$SecretKey}");
			$Return = json_decode($Response);
			return $Return;
		}*/

		/*ПРОИЗВОДИМ ЗАПРОС НА GOOGLE СЕРВИС И ЗАПИСЫВАЕМ ОТВЕТ*/
		/*$Return = getCaptcha($_POST['g-recaptcha-response']);*/
		/*ВЫВОДИМ НА ЭКРАН ПОЛУЧЕННЫЙ ОТВЕТ*/

		/*ЕСЛИ ЗАПРОС УДАЧНО ОТПРАВЛЕН И ЗНАЧЕНИЕ score БОЛЬШЕ 0,5 ВЫПОЛНЯЕМ КОД*/
		/*if($Return->success == true && $Return->score > 0.5){*/
			/**
			 * Write data to log file.
			 *
			 * @param mixed $data
			 * @param string $title
			 *
			 * @return bool
			 */
		/*}else{
			$mail = new PHPMailer();

			try {

				//Server settings
				$mail->isSMTP();
				$mail->Host = 'mail.adm.tools';
				$mail->SMTPAuth = true;
				$mail->Username = 'spam@smmstudio.com.ua';
				$mail->Password = 'newSmmSpam1PostPass!';
				$mail->SMTPSecure = 'tls';
				$mail->Port = 25;
				$mail->CharSet = 'UTF-8';

				//Recipients
				$mail->setFrom('spam@smmstudio.com.ua', 'info');
				$mail->addAddress('spam@smmstudio.com.ua', 'info');

				//Content
				$mail->isHTML(true);
				$mail->Subject = 'smmstudio.com';
				$mail->Body = "<p>Имя: $name</p><p>Телефон: $phone</p><p>Email: $email</p><p>Задача: $message</p><p>Сфера: $sfera</p><p>utmSource: $utmSource</p><p>utmMedium: $utmMedium</p><p>utmCampaign: $utmCampaign</p><p>utmTerm: $utmTerm</p><p>utmContent: $utmContent</p>";

				$mail->send();

			} catch (Exception $e) {
				echo 'Message could not be sent.';

				echo 'Mailer Error: ' . $mail->ErrorInfo;
			}

			header('Location:'.$homeUrl.$thxName.'');
        }*/

		//Подключение проверки капчи для com.ua
/*
		function getCaptcha($SecretKey) {
			$Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcSploaAAAAAMXevM0m_MkplC4DsJvVMaJMDyMD&response={$SecretKey}");
			$Return = json_decode($Response);
			return $Return;
		}*/

		/*ПРОИЗВОДИМ ЗАПРОС НА GOOGLE СЕРВИС И ЗАПИСЫВАЕМ ОТВЕТ*/
		/*$Return = getCaptcha($_POST['g-recaptcha-response']);*/
		/*ВЫВОДИМ НА ЭКРАН ПОЛУЧЕННЫЙ ОТВЕТ*/

		/*ЕСЛИ ЗАПРОС УДАЧНО ОТПРАВЛЕН И ЗНАЧЕНИЕ score БОЛЬШЕ 0,5 ВЫПОЛНЯЕМ КОД*/
		/*if($Return->success == true && $Return->score > 20){*/
		/*if($spamFild == ''){*/
			/**
			 * Write data to log file.
			 *
			 * @param mixed $data
			 * @param string $title
			 *
			 * @return bool
			 */

			/*if( $name != 'smmstudio.com' ){
				if(!empty($name) && !empty($email) && !empty($phone)) {

					try {
						$subdomain = 'smmstudioagency';
						$login     = 'office@smmstudio.com.ua';
						$apikey    = 'f189bda0968beadab5450e23d7f1cced';

						$amo = new \AmoCRM\Client($subdomain, $login, $apikey);

						$lead = $amo->lead;
						$lead['name'] = 'Заявка на консультацию';
						$lead['status_id'] = $voronka;

						$currentLid = $lead->apiList([
							'query' => $email,
						]);

						$stId[]='';

						foreach ( $currentLid as $status ){
							$stId[] = $status['status_id'];
						}

						$newLid = '';

						foreach ( $stId as $lidStstusId ){

							if( $lidStstusId == '15516604' ){
								$newLid = 'not';
							}
						}

						if( $newLid == '' ){

							$lead->addCustomField(1958329, $utmSource );
							$lead->addCustomField(1958331, $utmMedium );
							$lead->addCustomField(1958333, $utmCampaign );
							$lead->addCustomField(1958335, $utmTerm );
							$lead->addCustomField(1958337, $utmContent );
							$lead->addCustomField(1958257, $gclid );



							$lead->addCustomMultiField(2100383, $serviceFormData);



							$lead->addCustomField(1975283, $_COOKIE['roistat_visit'] );

							$lidId = $lead->apiAdd();

							// Получение экземпляра модели для работы с контактами
							$contact = $amo->contact;

							$currentUserEmail = $contact->apiList([
								'query' => $email,
								'limit_rows' => 2,
							]);

							$currentUserPhone = $contact->apiList([
								'query' => $phone,
								'limit_rows' => 2,
							]);

							if ( !empty($currentUserEmail)){

								$currentUser = $currentUserEmail;

							}elseif ( !empty($currentUserPhone)){

								$currentUser = $currentUserPhone;

							}else{

							}

							$contPhone = $currentUser[0]['custom_fields'][0]['values'][0]['value'];
							$contEmail = $currentUser[0]['custom_fields'][1]['values'][0]['value'];


							$id = $currentUser[0]['id'];
							$linkId = $currentUser[0]['linked_leads_id'];

							$allLInks[] = $lidId;

							foreach ($linkId as $item) {
								$allLInks[] = (string)$item;
							}

							if(!empty($currentUser)){

								$contact['linked_leads_id'] = $allLInks;

								$contact->apiUpdate((int)$id, 'now');

								if( $contPhone != $phone ){
									$note = $amo->note;
									$note['element_id'] = $id;
									$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
									$note['note_type'] = \AmoCRM\Models\Note::COMMON;
									$note['text'] = "Оставлена повторная заявка с другим номером ' $phone '. Имя указанное в заявке '$name'";
									$note->apiAdd();
								}elseif ($contEmail != $email){
									$note = $amo->note;
									$note['element_id'] = $id;
									$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
									$note['note_type'] = \AmoCRM\Models\Note::COMMON;
									$note['text'] = "Оставлена повторная заявка с другой почтой ' $email '. Имя указанное в заявке '$name'";
									$note->apiAdd();
								}else{

								}

							} else{
								// Заполнение полей модели
								$contact['name'] = isset($name) ? $name : 'Не указано';
								$contact['linked_leads_id'] = [(int)$lidId];

								$contact->addCustomField(1851983, [
									[$phone, 'MOB'],
								]);

								$contact->addCustomField(1851985, [
									[$email, 'PRIV'],
								]);

								$id = $contact->apiAdd();

								$note = $amo->note;
								$note['element_id'] = $id;
								$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
								$note['note_type'] = \AmoCRM\Models\Note::COMMON;
								$note['text'] = "Дубль в СРМ не найден.";
								$note->apiAdd();
							}

						}

					} catch (\AmoCRM\Exception $e) {
						printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());
					}

					$mail = new PHPMailer();

					try {

						//Server settings
						$mail->isSMTP();
						$mail->Host = 'mail.adm.tools';
						$mail->SMTPAuth = true;
						$mail->Username = 'amo@smmstudio.com.ua';
						$mail->Password = 'D27rTBaF4p4o';
						$mail->SMTPSecure = 'tls';
						$mail->Port = 25;
						$mail->CharSet = 'UTF-8';

						//Recipients
						$mail->setFrom('amo@smmstudio.com.ua', 'info');
						$mail->addAddress('amo@smmstudio.com.ua', 'info');

						//Content
						$mail->isHTML(true);
						$mail->Subject = 'smmstudio.com.ua';
						$mail->Body = "<p>Имя: $name</p><p>Телефон: $phone</p><p>Email: $email</p><p>Задача: $message</p><p>Сфера: $sfera</p><p>utmSource: $utmSource</p><p>utmMedium: $utmMedium</p><p>utmCampaign: $utmCampaign</p><p>utmTerm: $utmTerm</p><p>utmContent: $utmContent</p>";

						$mail->send();

					} catch (Exception $e) {
						echo 'Message could not be sent.';

						echo 'Mailer Error: ' . $mail->ErrorInfo;
					}

					header('Location:'.$homeUrl.$thxName.'');
				}
			}else{
				header('Location:'.$homeUrl.$thxName.'');
			}*/

			/*header('Location:'.$homeUrl.$thxName.'');*/
		/*}else{
			$mail = new PHPMailer();

			try {

				//Server settings
				$mail->isSMTP();
				$mail->Host = 'mail.adm.tools';
				$mail->SMTPAuth = true;
				$mail->Username = 'spam@smmstudio.com.ua';
				$mail->Password = 'newSmmSpam1PostPass!';
				$mail->SMTPSecure = 'tls';
				$mail->Port = 25;
				$mail->CharSet = 'UTF-8';

				//Recipients
				$mail->setFrom('spam@smmstudio.com.ua', 'info');
				$mail->addAddress('spam@smmstudio.com.ua', 'info');

				//Content
				$mail->isHTML(true);
				$mail->Subject = 'smmstudio.com.ua';
				$mail->Body = "<p>Имя: $name</p><p>Телефон: $phone</p><p>Email: $email</p><p>Задача: $message</p><p>Сфера: $sfera</p><p>utmSource: $utmSource</p><p>utmMedium: $utmMedium</p><p>utmCampaign: $utmCampaign</p><p>utmTerm: $utmTerm</p><p>utmContent: $utmContent</p>";

				$mail->send();

			} catch (Exception $e) {
				echo 'Message could not be sent.';

				echo 'Mailer Error: ' . $mail->ErrorInfo;
			}

			header('Location:'.$homeUrl.$thxName.'');
        }*/

        /*$cookie_name = "smmsend";
		$cookie_value = "sendmail";

		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day*/

	}

	add_action( 'admin_post_nopriv_contact_form', 'contact_form_send_email_to_admin' );
	add_action( 'admin_post_contact_form', 'contact_form_send_email_to_admin' );

	//Site QuizTo Amo

	function quiz_form_callback() {

		require_once __DIR__ . '/amocrm.phar';
		require_once('vendor/autoload.php');

		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';*/

		$name1 = $_POST['name'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];

		$mess;

		$utmSource = $_POST['utm_source'];
		$utmMedium = $_POST['utm_medium'];
		$utmCampaign = $_POST['utm_campaign'];
		$utmTerm = $_POST['utm_term'];
		$utmContent = $_POST['utm_content'];

		$voronka = 15516604;

		foreach ( $_POST as $item => $name ){
			/*echo '<pre>';
			print_r($item);
			echo '</pre>';*/

			foreach ( $name as $key => $value ){

				/*echo '<pre>';
				print_r($itemCh)
				echo '</pre>';*/

				if( is_array($value)){
					$value = implode(", ", $value);

					$mess = $mess.$key .' : '.$value."\r\n";
				}else{
					$mess = $mess.$key .' : '.$value."\r\n";
				}

			}

		}

		if( $name1 != 'smmstudio.com' ){
			if(!empty($name1) && !empty($email) && !empty($phone)) {

				try {
					$subdomain = 'smmstudioagency';
					$login     = 'office@smmstudio.com.ua';
					$apikey    = 'f189bda0968beadab5450e23d7f1cced';

					$amo = new \AmoCRM\Client($subdomain, $login, $apikey);

					$lead = $amo->lead;
					$lead['name'] = 'Квиз по таргету';
					$lead['status_id'] = $voronka;

					$currentLid = $lead->apiList([
						'query' => $email,
					]);

					$stId[]='';

					foreach ( $currentLid as $status ){
						$stId[] = $status['status_id'];
					}

					$newLid = '';

					foreach ( $stId as $lidStstusId ){

						if( $lidStstusId == '15516604' ){
							$newLid = 'not';
						}
					}

					if( $newLid == '' ){

						$lead->addCustomField(1958329, $utmSource );
						$lead->addCustomField(1958331, $utmMedium );
						$lead->addCustomField(1958333, $utmCampaign );
						$lead->addCustomField(1958335, $utmTerm );
						$lead->addCustomField(1958337, $utmContent );
						$lead->addCustomField(1974757, $mess );

						$lead->addCustomField(1975283, $_COOKIE['roistat_visit'] );

						$lidId = $lead->apiAdd();

						// Получение экземпляра модели для работы с контактами
						$contact = $amo->contact;

						$currentUserEmail = $contact->apiList([
							'query' => $email,
							'limit_rows' => 2,
						]);

						$currentUserPhone = $contact->apiList([
							'query' => $phone,
							'limit_rows' => 2,
						]);

						if ( !empty($currentUserEmail)){

							$currentUser = $currentUserEmail;

						}elseif ( !empty($currentUserPhone)){

							$currentUser = $currentUserPhone;

						}else{

						}

						$contPhone = $currentUser[0]['custom_fields'][0]['values'][0]['value'];
						$contEmail = $currentUser[0]['custom_fields'][1]['values'][0]['value'];


						$id = $currentUser[0]['id'];
						$linkId = $currentUser[0]['linked_leads_id'];

						$allLInks[] = $lidId;

						foreach ($linkId as $item) {
							$allLInks[] = (string)$item;
						}

						if(!empty($currentUser)){

							$contact['linked_leads_id'] = $allLInks;

							$contact->apiUpdate((int)$id, 'now');

							if( $contPhone != $phone ){
								$note = $amo->note;
								$note['element_id'] = $id;
								$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
								$note['note_type'] = \AmoCRM\Models\Note::COMMON;
								$note['text'] = "Оставлена повторная заявка с другим номером ' $phone '. Имя указанное в заявке '$name'";
								$note->apiAdd();
							}elseif ($contEmail != $email){
								$note = $amo->note;
								$note['element_id'] = $id;
								$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
								$note['note_type'] = \AmoCRM\Models\Note::COMMON;
								$note['text'] = "Оставлена повторная заявка с другой почтой ' $email '. Имя указанное в заявке '$name'";
								$note->apiAdd();
							}else{

							}

						} else{
							// Заполнение полей модели
							$contact['name'] = isset($name) ? $name1 : 'Не указано';
							$contact['linked_leads_id'] = [(int)$lidId];

							$contact->addCustomField(1851983, [
								[$phone, 'MOB'],
							]);

							$contact->addCustomField(1851985, [
								[$email, 'PRIV'],
							]);

							$id = $contact->apiAdd();

							$note = $amo->note;
							$note['element_id'] = $id;
							$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
							$note['note_type'] = \AmoCRM\Models\Note::COMMON;
							$note['text'] = "Дубль в СРМ не найден.";
							$note->apiAdd();
						}

					}

				} catch (\AmoCRM\Exception $e) {
					/*printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());*/
				}

			}
		}else{

		}

	}

	add_action('wp_ajax_quiz_form', 'quiz_form_callback');
	add_action('wp_ajax_nopriv_quiz_form', 'quiz_form_callback');

//

	function add_rel_preload($html, $handle, $href, $media) {
		if (is_admin())
			return $html;

		$html = <<<EOT
			<link rel='preload' as='style' onload="this.onload=null;this.rel='stylesheet'" 
			id='$handle' href='$href' type='text/css' media='all' />
EOT;
		return $html;
	}

	add_filter( 'style_loader_tag', 'add_rel_preload', 10, 4 );


