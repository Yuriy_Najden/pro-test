<?php

	require_once __DIR__ . '/amocrm.phar';
	require_once('vendor/autoload.php');

	/*echo '<pre>';
	print_r($_POST);
	echo '</pre>';*/

	$name1 = $_POST['name'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];

	$mess = array();

	$utmSource = $_POST['utm_source'];
	$utmMedium = $_POST['utm_medium'];
	$utmCampaign = $_POST['utm_campaign'];
	$utmTerm = $_POST['utm_term'];
	$utmContent = $_POST['utm_content'];

	$voronka = 15516604;

	foreach ( $_POST as $item => $name ){
		/*echo '<pre>';
		print_r($item);
		echo '</pre>';*/

		foreach ( $name as $key => $value ){

			/*echo '<pre>';
			print_r($itemCh)
			echo '</pre>';*/

			if( is_array($value)){
				$value = implode(", ", $value);

				$mess = $mess.$key .' : '.$value."\r\n";
			}else{
				$mess = $mess.$key .' : '.$value."\r\n";
			}

		}

	}

	if( $name1 != 'smmstudio.com' ){
		if(!empty($name1) && !empty($email) && !empty($phone)) {

			try {
				$subdomain = 'smmstudioagency';
				$login     = 'office@smmstudio.com.ua';
				$apikey    = 'f189bda0968beadab5450e23d7f1cced';

				$amo = new \AmoCRM\Client($subdomain, $login, $apikey);

				$lead = $amo->lead;
				$lead['name'] = 'Квиз по таргету';
				$lead['status_id'] = $voronka;

				$currentLid = $lead->apiList([
					'query' => $email,
				]);

				$stId[]='';

				if($currentLid){
					foreach ( $currentLid as $status ){
						$stId[] = $status['status_id'];
					}
				}

				$newLid = '';

				if($stId){
					foreach ( $stId as $lidStstusId ){

						if( $lidStstusId == '15516604' ){
							$newLid = 'not';
						}
					}
				}


				if( $newLid == '' ){

					$lead->addCustomField(1958329, $utmSource );
					$lead->addCustomField(1958331, $utmMedium );
					$lead->addCustomField(1958333, $utmCampaign );
					$lead->addCustomField(1958335, $utmTerm );
					$lead->addCustomField(1958337, $utmContent );
					$lead->addCustomField(1974757, $mess );

					$lead->addCustomField(1975283, $_COOKIE['roistat_visit'] );

					$lidId = $lead->apiAdd();

					// Получение экземпляра модели для работы с контактами
					$contact = $amo->contact;

					$currentUserEmail = $contact->apiList([
						'query' => $email,
						'limit_rows' => 2,
					]);

					$currentUserPhone = $contact->apiList([
						'query' => $phone,
						'limit_rows' => 2,
					]);

					if ( !empty($currentUserEmail)){

						$currentUser = $currentUserEmail;

					}elseif ( !empty($currentUserPhone)){

						$currentUser = $currentUserPhone;

					}

					$contPhone = $currentUser[0]['custom_fields'][0]['values'][0]['value'];
					$contEmail = $currentUser[0]['custom_fields'][1]['values'][0]['value'];


					$id = $currentUser[0]['id'];
					$linkId = $currentUser[0]['linked_leads_id'];

					$allLInks[] = $lidId;

					if($linkId){
						foreach ($linkId as $item) {
							$allLInks[] = (string)$item;
						}
					}

					if(!empty($currentUser)){

						$contact['linked_leads_id'] = $allLInks;

						$contact->apiUpdate((int)$id, 'now');

						if( $contPhone != $phone ){
							$note = $amo->note;
							$note['element_id'] = $id;
							$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
							$note['note_type'] = \AmoCRM\Models\Note::COMMON;
							$note['text'] = "Оставлена повторная заявка с другим номером ' $phone '. Имя указанное в заявке '$name'";
							$note->apiAdd();
						}elseif ($contEmail != $email){
							$note = $amo->note;
							$note['element_id'] = $id;
							$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
							$note['note_type'] = \AmoCRM\Models\Note::COMMON;
							$note['text'] = "Оставлена повторная заявка с другой почтой ' $email '. Имя указанное в заявке '$name'";
							$note->apiAdd();
						}

					} else{
						// Заполнение полей модели
						$contact['name'] = isset($name) ? $name1 : 'Не указано';
						$contact['linked_leads_id'] = [(int)$lidId];

						$contact->addCustomField(1851983, [
							[$phone, 'MOB'],
						]);

						$contact->addCustomField(1851985, [
							[$email, 'PRIV'],
						]);

						$id = $contact->apiAdd();

						$note = $amo->note;
						$note['element_id'] = $id;
						$note['element_type'] = \AmoCRM\Models\Note::TYPE_CONTACT;
						$note['note_type'] = \AmoCRM\Models\Note::COMMON;
						$note['text'] = "Дубль в СРМ не найден.";
						$note->apiAdd();
					}

				}

			} catch (\AmoCRM\Exception $e) {
				/*printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());*/
			}

		}
	}