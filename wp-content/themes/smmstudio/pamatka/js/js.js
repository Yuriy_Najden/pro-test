$(function() {

    // SHOW SCROLL TOP BUTTON.

    $(document).scroll(function() {
        var y = $(this).scrollTop();

        if (y > 1) {
            $('header').addClass('scroll');
        } else {
            $('header').removeClass('scroll');
        }
    });

});

